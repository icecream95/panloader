#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <assert.h>
#include <unistd.h>

#include <mali-ioctl.h>

#include <sys/user.h>

unsigned MARK = 0;

struct ptr {
	void *cpu;
	u64 gpu;
};

unsigned char vs_bin[] = {
  0x7b, 0x0d, 0x00, 0x00, 0x04, 0x84, 0x5e, 0x00,
  0xc4, 0xc0, 0x3c, 0x10, 0x71, 0xc0, 0xb4, 0x00,
  0xc0, 0x00, 0x00, 0x80, 0xbf, 0xc2, 0x10, 0x01,
  0x40, 0xc0, 0x02, 0xd0, 0x00, 0xc1, 0x50, 0x01,
  0xc8, 0xc0, 0x7c, 0x10, 0x61, 0xc0, 0xb4, 0x00,
  0x40, 0xc0, 0x42, 0xd0, 0x00, 0xc0, 0x50, 0x09,
  0xc0, 0xd3, 0x35, 0xe6, 0x32, 0xc3, 0x10, 0x01,
  0x01, 0x03, 0xc0, 0x00, 0x84, 0xc6, 0xb2, 0x00,
  0x00, 0x43, 0x46, 0x00, 0x80, 0xc3, 0xb2, 0x00,
  0xd0, 0x43, 0x03, 0x00, 0x00, 0xc3, 0xa4, 0x00,
  0x43, 0x00, 0x00, 0x00, 0x00, 0xcb, 0x9c, 0x00,
  0x00, 0x87, 0xc0, 0x00, 0x04, 0xc2, 0xb2, 0x00,
  0x01, 0x81, 0x42, 0x00, 0x00, 0xc2, 0xb2, 0x00,
  0x42, 0x0b, 0x85, 0x00, 0x00, 0xc9, 0xb2, 0x00,
  0x41, 0x83, 0xc0, 0x00, 0x04, 0xc1, 0xb2, 0x00,
  0x40, 0x80, 0x41, 0x00, 0x00, 0xc0, 0xb2, 0x00,
  0x40, 0x0b, 0x84, 0x00, 0x00, 0xc8, 0xb2, 0x00,
  0x86, 0x00, 0x00, 0x00, 0x00, 0xca, 0x91, 0x00,
  0x44, 0x00, 0x00, 0x39, 0x08, 0x48, 0x61, 0x78
};

unsigned char fs_bin[] = {
  0xc0, 0x00, 0x3c, 0x00, 0x3c, 0xc0, 0x10, 0x41, 0x3c, 0xd0, 0xea, 0x00,
  0x02, 0xbc, 0x7d, 0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc1, 0x91, 0x48,
  0xf0, 0x00, 0x3c, 0x33, 0x04, 0x40, 0x7f, 0x78, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0xf6, 0x10, 0x01, 0xc0, 0xf1, 0x00, 0x00, 0x10, 0xc1, 0x2f, 0x08
};
unsigned int fs_bin_len = 48;

/* Valhall data structures demo */

struct ptr galloc(struct ptr *pool, size_t sz)
{
	sz = (sz + 4096) & ~4095; // ALIGN

	struct ptr ret = {
		.cpu = pool->cpu + MARK,
		.gpu = pool->gpu + MARK
	};

	MARK += sz;

	return ret;
}


#include "build/pack.h"

static u64 demo_compute(struct ptr heap, struct ptr shaders)
{
	struct ptr output = galloc(&heap, 1024);

	uint8_t prog[] = {
		0xc0, 0xbe, 0xba, 0xfe, 0xca, 0xc0, 0x10, 0x01,
		0x80, 0x00, 0x00, 0x18, 0x02, 0x40, 0x61, 0x78
	};

	memcpy(shaders.cpu, prog, sizeof(prog));

	struct ptr fau = galloc(&heap, 1024);
	u64 *fau_cpu = fau.cpu;
	fau_cpu[0] = output.gpu;

	struct ptr tsd = galloc(&heap, 1024);
	pan_pack(tsd.cpu, LOCAL_STORAGE, cfg) {
		cfg.tls_base_pointer = (1ull << 60);
	}

	struct ptr shader = galloc(&heap, 1024);
	pan_pack(shader.cpu, SHADER_PROGRAM, cfg) {
		cfg.stage = MALI_SHADER_STAGE_COMPUTE;
		cfg.is_not_coordinate_shader = 1;
		cfg.register_allocation = MALI_SHADER_REGISTER_ALLOCATION_64_PER_THREAD;
		cfg.binary = shaders.gpu;
	}

	struct ptr jc = galloc(&heap, 1024);
	pan_pack(jc.cpu, JOB_HEADER, cfg) {
		cfg.type = MALI_JOB_TYPE_COMPUTE;
		cfg.index = 1;
	}
	pan_pack(jc.cpu + MALI_JOB_HEADER_LENGTH, COMPUTE_PAYLOAD, cfg) {
		cfg.workgroup_size_x = 1;
		cfg.workgroup_size_y = 1;
		cfg.workgroup_size_z = 1;
		cfg.workgroup_count_x = 1;
		cfg.workgroup_count_y = 1;
		cfg.workgroup_count_z = 1;

		cfg.unk_0 = 0x2;
		cfg.unk_1 = 0x8010;

		cfg.compute.fau_count = 1;
		cfg.compute.shader = shader.gpu;
		cfg.compute.thread_storage = tsd.gpu;
		cfg.compute.fau = fau.gpu;
	}

	return jc.gpu;
}

#define HEAP_SIZE (0x200000)
#define HEAP_PAGES (HEAP_SIZE / 4096)

static u64 demo_tiler(struct ptr heap, struct ptr tiler_heap)
{
	struct ptr tiler_heap_descriptor = galloc(&heap, 1024);
	pan_pack(tiler_heap_descriptor.cpu, TILER_HEAP, cfg) {
		cfg.size = HEAP_SIZE;
		cfg.base = tiler_heap.gpu;
		cfg.bottom = tiler_heap.gpu;
		cfg.top = tiler_heap.gpu + cfg.size;
	}

	struct ptr tiler_context = galloc(&heap, 1024);
	pan_pack(tiler_context.cpu, TILER_CONTEXT, cfg) {
		cfg.hierarchy_mask = 0xaa;//value on G57 vs value on G78? 202;
		cfg.fb_width = 256;
		cfg.fb_height = 256;
		cfg.sample_pattern = MALI_SAMPLE_PATTERN_SINGLE_SAMPLED;
		cfg.heap = tiler_heap_descriptor.gpu;
	}

	pan_pack(tiler_context.cpu + pan_size(TILER_CONTEXT), TILER_CONTEXT, cfg) {
		cfg.fb_width = 1;
		cfg.fb_height = 1;
		cfg.heap = tiler_heap_descriptor.gpu;
	}

	return tiler_context.gpu;
}

static u64 demo_idvs(struct ptr heap, struct ptr shaders, struct ptr tiler_heap, u64 tiler)
{
	memcpy(shaders.cpu, vs_bin, sizeof(vs_bin));
	memcpy(shaders.cpu + 1024, fs_bin, sizeof(fs_bin));

	struct ptr tsd = galloc(&heap, 1024);
	pan_pack(tsd.cpu, LOCAL_STORAGE, cfg) {
		cfg.tls_base_pointer = (1ull << 60);
	}

	struct ptr shader_vs = galloc(&heap, 1024);
	pan_pack(shader_vs.cpu, SHADER_PROGRAM, cfg) {
		cfg.stage = MALI_SHADER_STAGE_VERTEX;
		cfg.is_not_coordinate_shader = true; // XXX: because there is no varying shader?
		cfg.register_allocation = MALI_SHADER_REGISTER_ALLOCATION_32_PER_THREAD;
		cfg.binary = shaders.gpu;
		cfg.preload.vertex.linear_id = true;
		cfg.preload.vertex.vertex_id = true;
	}

	struct ptr shader_fs = galloc(&heap, 1024);
	pan_pack(shader_fs.cpu, SHADER_PROGRAM, cfg) {
		cfg.stage = MALI_SHADER_STAGE_FRAGMENT;
		cfg.is_not_coordinate_shader = true;
		cfg.register_allocation = MALI_SHADER_REGISTER_ALLOCATION_32_PER_THREAD;
		cfg.binary = shaders.gpu + 1024;
		cfg.preload.fragment.coverage = true;
		cfg.preload.fragment.sample_mask_id = true;
	}

	struct ptr zs = galloc(&heap, 1024);
	pan_pack(zs.cpu, DEPTH_STENCIL, cfg) {
		cfg.front_compare_function = MALI_FUNC_ALWAYS;
		cfg.front_stencil_fail = MALI_STENCIL_OP_KEEP;
		cfg.front_depth_fail = MALI_STENCIL_OP_KEEP;
		cfg.front_depth_pass = MALI_STENCIL_OP_KEEP;
		cfg.back_compare_function = MALI_FUNC_ALWAYS;
		cfg.back_stencil_fail = MALI_STENCIL_OP_KEEP;
		cfg.back_depth_fail = MALI_STENCIL_OP_KEEP;
		cfg.back_depth_pass = MALI_STENCIL_OP_KEEP;
		cfg.stencil_test_enable = false;
		cfg.front_write_mask = 0xFF;
		cfg.front_value_mask = 0xFF;
		cfg.back_write_mask = 0xFF;
		cfg.back_value_mask = 0xFF;
		cfg.unk_1 = true;
		cfg.unk_2 = true;
		cfg.depth_function = MALI_FUNC_ALWAYS;
	}

	struct ptr fau = galloc(&heap, 1024);
	float blob_fau[] = {
		128.0, 128.0, 0.5, 0.0,
		128.0, 128.0, 0.5, 0.0,
		0.0, 0.0
	};
	memcpy(fau.cpu, blob_fau, sizeof(blob_fau));

	struct ptr blend = galloc(&heap, 1024);
	pan_pack(blend.cpu, BLEND, cfg) {
		cfg.enable = true;

		cfg.equation.rgb.a = MALI_BLEND_OPERAND_A_SRC;
		cfg.equation.rgb.b = MALI_BLEND_OPERAND_B_SRC;
		cfg.equation.rgb.c = MALI_BLEND_OPERAND_C_ZERO;
		cfg.equation.alpha.a = MALI_BLEND_OPERAND_A_SRC;
		cfg.equation.alpha.b = MALI_BLEND_OPERAND_B_SRC;
		cfg.equation.alpha.c = MALI_BLEND_OPERAND_C_ZERO;
		cfg.equation.color_mask = 0xF;

		cfg.internal.mode = MALI_BLEND_MODE_OPAQUE;
		cfg.internal.fixed_function.num_comps = 4;
		cfg.internal.fixed_function.conversion.memory_format = (MALI_RGBA8_TB << 12);
		cfg.internal.fixed_function.conversion.register_format = MALI_REGISTER_FILE_FORMAT_F16;
	}

	struct ptr jc = galloc(&heap, 1024);
	pan_pack(jc.cpu, JOB_HEADER, cfg) {
		cfg.type = MALI_JOB_TYPE_IDVS_HELPER;
		cfg.index = 2;
		cfg.dependency_1 = 1;
		cfg.dependency_2 = 2;
	}
	pan_pack(jc.cpu + MALI_JOB_HEADER_LENGTH, IDVS_HELPER_PAYLOAD, cfg) {
		cfg.primitive.draw_mode = MALI_DRAW_MODE_TRIANGLES;
		cfg.primitive.first_provoking_vertex = false;
		cfg.primitive.index_count = 3;
		cfg.instance_count = 1;
		cfg.vertex_output_bytes = 16;
		cfg.tiler = tiler;
		cfg.scissor.scissor_maximum_x = 255;
		cfg.scissor.scissor_maximum_y = 255;
		cfg.line_width = 1.0;

		cfg.draw.pixel_kill_operation = MALI_PIXEL_KILL_WEAK_EARLY;
		cfg.draw.zs_update_operation = MALI_PIXEL_KILL_WEAK_EARLY;
		cfg.draw.allow_forward_pixel_to_kill = true;
		cfg.draw.allow_forward_pixel_to_be_killed = true;
		cfg.draw.alpha_one_store = true;
		cfg.draw.clear_colour_depth = true;
		cfg.draw.front_face_ccw = true;
		cfg.draw.minimum_z = 0.0;
		cfg.draw.maximum_z = 1.0;
		cfg.draw.unk_25 = 0x1FFFF;

		// Story of an interesting bit
		// DATA_INVALID_FAULT if this isn't set ...
		// ... and this is set by the GPU, maybe one of the earlier
		// compute jobs Needs to be investigated!
		// Gates everything else, then the tiler is looked up.
		cfg.draw.unk_26 = 1;

		cfg.draw.blend_unk = 2;

		cfg.draw.position.thread_storage = tsd.gpu;
		cfg.draw.position.shader = shader_vs.gpu;
		cfg.draw.position.fau = fau.gpu;
		cfg.draw.position.fau_count = sizeof(blob_fau) / sizeof(u64);

#if 1
		cfg.draw.fragment.shader = shader_fs.gpu;
		cfg.draw.fragment.thread_storage = tsd.gpu;
		cfg.draw.blend = blend.gpu;
		cfg.draw.depth_stencil = zs.gpu;
#else
		cfg.draw.blend = 0xCABE0000;
		cfg.draw.fragment.shader = 0xAC000000;
		cfg.draw.fragment.thread_storage = 0xAF000000;
		cfg.draw.depth_stencil = 0xCADE0000;
#endif
	}

	return jc.gpu;
}


static u64 demo_frag(struct ptr heap, struct ptr shaders, uint64_t tiler, struct ptr rt0)
{
	struct ptr samples = galloc(&heap, 1024);
	uint16_t *sample = samples.cpu;
	for (int i = 0; i < 34*2; ++i)
		sample[i] = 128;

	struct ptr framebuffer = galloc(&heap, 1024);
	pan_pack(framebuffer.cpu, FRAMEBUFFER_PARAMETERS, cfg) {
		cfg.sample_locations = samples.gpu;
		cfg.frame_shader_dcds = 0xABCD0000; // TODO
		cfg.width = 256;
		cfg.height = 256;
		cfg.bound_max_x = 255;
		cfg.bound_max_y = 255;
		cfg.sample_count = 1;
		cfg.sample_pattern = MALI_SAMPLE_PATTERN_SINGLE_SAMPLED;
		cfg.effective_tile_size = 256;
		cfg.x_downsampling_scale = 4; // XXX
		cfg.y_downsampling_scale = 3; // XXX
		cfg.render_target_count = 1;
		cfg.color_buffer_allocation = 1024;
		cfg.z_internal_format = MALI_Z_INTERNAL_FORMAT_D24;
		cfg.has_zs_crc_extension = true;
		cfg.tiler = tiler;
	}

	pan_pack(framebuffer.cpu + pan_size(FRAMEBUFFER), ZS_CRC_EXTENSION, cfg) {
		cfg.word_0 = 0x40004;
		cfg.crc_row_stride = 128;
		cfg.crc_clear_color = 0x429847f1dc972c91ULL;
		cfg.crc_base = 0x5effe96040ULL;
	}

	pan_pack(framebuffer.cpu + pan_size(FRAMEBUFFER) + pan_size(ZS_CRC_EXTENSION), RENDER_TARGET, cfg) {
		cfg.internal_format = MALI_COLOR_BUFFER_INTERNAL_FORMAT_R8G8B8A8;
//		cfg.writeback_block_format = MALI_BLOCK_FORMAT_LINEAR;
		cfg.writeback_block_format = MALI_BLOCK_FORMAT_TILED_U_INTERLEAVED;
		cfg.write_enable = true;
		cfg.writeback_format = MALI_COLOR_FORMAT_R8G8B8;
//		cfg.writeback_format = MALI_COLOR_FORMAT_R8G8B8A8;
		cfg.writeback_msaa = MALI_MSAA_SINGLE;
		//cfg.swizzle = 1672;
		cfg.swizzle = 2696;
		cfg.dithering_enable = true;
		cfg.clean_pixel_write_enable = true;
		cfg.rgb.base = rt0.gpu;
		cfg.rgb.row_stride = 12288; //256*4;
#if 0
		cfg.clear.color_0 = 0xff00ff;
		cfg.clear.color_1 = 0xff00ff;
		cfg.clear.color_2 = 0xff00ff;
		cfg.clear.color_3 = 0xff00ff;
#endif
	}

	struct ptr jc = galloc(&heap, 1024);
	pan_pack(jc.cpu, JOB_HEADER, cfg) {
		cfg.type = MALI_JOB_TYPE_FRAGMENT;
		cfg.index = 2;
		cfg.dependency_1 = 1;
	}
	pan_pack(jc.cpu + MALI_JOB_HEADER_LENGTH, FRAGMENT_JOB_PAYLOAD, cfg) {
		cfg.bound_max_x = 15;
		cfg.bound_max_y = 15;
		cfg.framebuffer = framebuffer.gpu | 3;
	}

	return jc.gpu;
}


/* From the kernel module */

#define MALI_MEM_MAP_TRACKING_HANDLE (3ull << 12)
#define MALI_CONTEXT_CREATE_FLAG_NONE 0

static int
pandev_get_driver_version(int fd, unsigned *major, unsigned *minor)
{
	struct mali_ioctl_get_version args = { };
	int rc = ioctl(fd, MALI_IOCTL_GET_VERSION, &args);

	if (rc)
		return rc;

	*major = args.major;
	*minor = args.minor;

	return 0;
}

static int
pandev_set_flags(int fd)
{
	struct mali_ioctl_set_flags args = {
		.create_flags = MALI_CONTEXT_CREATE_FLAG_NONE
	};

	return ioctl(fd, MALI_IOCTL_SET_FLAGS, &args);
}

struct ptr
pandev_allocate(int fd, int va_pages, int flags)
{
	union mali_ioctl_mem_alloc args = {
		.in.va_pages = va_pages,
		.in.commit_pages = va_pages,
		.in.flags = flags
	};

	int rc = ioctl(fd, MALI_IOCTL_MEM_ALLOC, &args);
	assert (rc == 0);

	void *map = mmap(NULL, va_pages * 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, args.out.gpu_va);
	assert (map != MAP_FAILED);

	memset((void *) (uintptr_t) map, 0, va_pages * 4096);

	return (struct ptr) {
		.cpu = (void *) (uintptr_t) map,
		.gpu = (uintptr_t) map
	};
}

u8*
pandev_map_mtp(int fd)
{
	return mmap(NULL, 4096, PROT_NONE, MAP_SHARED, fd, MALI_MEM_MAP_TRACKING_HANDLE);
}

int main(int argc, char **argv) {
	int fd = open("/dev/mali0", O_RDWR | O_NONBLOCK | O_CLOEXEC);
	int rc;
	unsigned major, minor;

	if (argc != 2) {
		fprintf(stderr, "usage: mali0 [output.bmp]\n");
		return 1;
	}

	if (fd < 0)
		return fd;

	rc = pandev_get_driver_version(fd, &major, &minor);
	if (rc) {
		printf("Failed to get driver version: %d\n", rc);
		return rc;
	}

	printf("Found kernel driver version v%d.%d at /dev/mali0\n",
	       major, minor);

	rc = pandev_set_flags(fd);
	if (rc) {
		printf("Failed to set flags %d\n", rc);
		return rc;
	}

	printf("Set flags\n");

	if (pandev_map_mtp(fd) == MAP_FAILED)
		return -1;

	printf("Mapped mtp\n");

	struct ptr main = pandev_allocate(fd, 4096, 
		       MALI_MEM_PROT_CPU_RD |
		       MALI_MEM_PROT_CPU_WR |
		       MALI_MEM_PROT_GPU_RD |
		       MALI_MEM_PROT_GPU_WR |
		       MALI_MEM_SAME_VA);

	struct ptr rt0 = pandev_allocate(fd, (256*256*4)/4096, 
		       MALI_MEM_PROT_CPU_RD |
		       MALI_MEM_PROT_CPU_WR |
		       MALI_MEM_PROT_GPU_RD |
		       MALI_MEM_PROT_GPU_WR |
		       MALI_MEM_SAME_VA);

	struct ptr shader = pandev_allocate(fd, 4096, 
		       MALI_MEM_PROT_CPU_RD |
		       MALI_MEM_PROT_CPU_WR |
		       MALI_MEM_PROT_GPU_RD |
		       MALI_MEM_PROT_GPU_EX |
		       MALI_MEM_SAME_VA);

	struct ptr tiler_heap = pandev_allocate(fd, HEAP_PAGES, 
		       MALI_MEM_PROT_CPU_RD |
		       MALI_MEM_PROT_CPU_WR |
		       MALI_MEM_PROT_GPU_RD |
		       MALI_MEM_PROT_GPU_WR |
		       MALI_MEM_SAME_VA);

	u64 tiler = demo_tiler(main, tiler_heap);

	struct mali_jd_atom_v2 atoms[] = {
		{
			.jc = demo_idvs(main, shader, tiler_heap, tiler),
			.atom_number = 1,
			.core_req = MALI_JD_REQ_CS | MALI_JD_REQ_T | MALI_JD_REQ_CF | MALI_JD_REQ_COHERENT_GROUP,
		},
#if 1
		{
			.jc = demo_frag(main, shader, tiler, rt0),
			.atom_number = 2,
			.core_req = MALI_JD_REQ_FS,
			.pre_dep[0].atom_id = 1,
			.pre_dep[0].dependency_type = MALI_JD_DEP_TYPE_DATA,
			.pre_dep[1].atom_id = 1,
			.pre_dep[1].dependency_type = MALI_JD_DEP_TYPE_ORDER,
		}
#endif

	};

	struct mali_ioctl_job_submit js = {
		.addr = atoms,
		.nr_atoms = sizeof(atoms) / sizeof(atoms[0]),
		.stride = sizeof(atoms[0])
	};

	rc = ioctl(fd, MALI_IOCTL_JOB_SUBMIT, &js);

	if (rc) {
		printf("submit failed %d\n", rc);
		return rc;
	}

	usleep(1000);
	printf("submitted\n");
	printf("result %" PRIx64 "\n", *((u64 *) rt0.cpu));

	return 0;
}
