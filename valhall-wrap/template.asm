IADD_IMM.i32 r0, 0x0, #0xDEADBEEF

# Load a value
LOAD.i128.unsigned.slot0.wait0 @r0:r1:r2:r3, u0, offset:4

FMA.f32 r0, r0, r0, r0
FMA.f32 r0, r0, u5, 0x0.neg
FMA.f32 r0, u4, 0x0, r0

#MOV.i32 r0, 0x0
FMA.f32 r0, r2, r0, u5

# Store the snarfed value
SHADDX.u64 r2, u0, r60.w0, shift:2
STORE.i32.slot0.return @r0, r2, offset:0
