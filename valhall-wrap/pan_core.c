/*
 * Copyright (c) 2021 Icecream95
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <fcntl.h>
#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <libelf.h>
#include <elfutils/libdwelf.h>

#include "pan_core.h"

struct pan_section_info {
        uint64_t va;
        size_t size;
        size_t file_size;
        void *ptr;
        const char *label;
        uint32_t flags;

        Elf64_Shdr *shdr;
        Dwelf_Strent *str;

        struct pan_section_info *next;
};

struct pan_core {
        Elf *elf;
        unsigned num_phdr;

        struct pan_section_info *sections;
        struct pan_section_info *last_section;

        uint64_t *trampoline_instrs;
        struct job_shader_info *si;
};

struct pan_core *
panfrost_core_create(int fd)
{
        elf_version(EV_CURRENT);

        Elf *elf = elf_begin(fd, ELF_C_WRITE, NULL);
        if (!elf) {
                printf("error creating ELF descriptor: %s\n", elf_errmsg(-1));
                return NULL;
        }

        Elf64_Ehdr *ehdr = elf64_newehdr(elf);
        if (!ehdr) {
                printf("error creating ELF header: %s\n", elf_errmsg(-1));
                return NULL;
        }
        *ehdr = (Elf64_Ehdr) {
                .e_ident[EI_CLASS] = ELFCLASS64,
                .e_ident[EI_DATA] = ELFDATA2LSB,
                .e_ident[EI_OSABI] = ELFOSABI_STANDALONE,
                .e_type = ET_CORE,
                .e_machine = 24884, /* Randomly chosen unofficial value */
                .e_version = EV_CURRENT,
        };
        elf_flagehdr(elf, ELF_C_SET, ELF_F_DIRTY);

        struct pan_core *core = calloc(sizeof(*core), 1);
        core->elf = elf;

        return core;
}

void
panfrost_core_add(struct pan_core *core, uint64_t va, size_t size,
                  uint8_t *ptr, const char *label, uint32_t flags)
{
        struct pan_section_info *info = calloc(sizeof(*info), 1);

        size_t file_size = size;
        while (file_size && ptr[file_size - 1] == 0xac)
            --file_size;

        *info = (struct pan_section_info) {
                .va = va,
                .size = size,
                .file_size = file_size,
                .ptr = ptr,
                .label = label,
                .flags = flags,
        };

        if (core->last_section)
                core->last_section->next = info;
        else
                core->sections = info;

        core->last_section = info;

        if (ptr)
                ++core->num_phdr;
}

void
panfrost_core_add_si(struct pan_core *core, struct job_shader_info *si)
{
        unsigned size = sizeof(uint64_t) * si->num_instr;
        uint64_t *instrs = malloc(size);

        struct pan_section_info *info = calloc(sizeof(*info), 1);

        *info = (struct pan_section_info) {
                .va = 0,
                .size = size,
                .ptr = instrs,
                .label = ".text",
                .flags = 0,
        };

        if (core->last_section)
                core->last_section->next = info;
        else
                core->sections = info;

        core->last_section = info;

        ++core->num_phdr;

        uint64_t *ins = instrs;
        for (struct job_shader_instr *instr = si->next; instr; instr = instr->next) {
                if (instr->type != SI_INSTR)
                        continue;
                *(ins++) = instr->instr;
        }
        assert(ins == instrs + si->num_instr);

        core->trampoline_instrs = instrs;
        core->si = si;
}

void
panfrost_core_finish(struct pan_core *core)
{
        Elf *elf = core->elf;

        unsigned num_phdr = core->num_phdr;

        Elf64_Phdr *phdr = elf64_newphdr(elf, num_phdr);
        if (num_phdr && !phdr) {
                printf("error creating program headers: %s\n", elf_errmsg(-1));
                return;
        }

        for (unsigned i = 0; i < num_phdr; ++i)
                phdr[i].p_type = PT_LOAD;
        elf_flagphdr(elf, ELF_C_SET, ELF_F_DIRTY);

        unsigned trampoline_index = 0;

        Dwelf_Strtab *shst = dwelf_strtab_init(true);

        for (struct pan_section_info *info = core->sections; info; info = info->next) {
                Elf_Scn *section = elf_newscn(elf);
                if (!section) {
                        printf("error creating PROGBITS section: %s\n", elf_errmsg(-1));
                        return;
                }
                Elf64_Shdr *shdr = elf64_getshdr(section);
                if (!shdr) {
                        printf("error getting header for PROGBITS section: %s\n", elf_errmsg(-1));
                        return;
                }

                Dwelf_Strent *str = dwelf_strtab_add(shst, info->label ?: "Unknown BO");

                if (!info->va)
                        trampoline_index = elf_ndxscn(section);

                info->str = str;

                *shdr = (Elf64_Shdr) {
                        .sh_type = info->ptr ? SHT_PROGBITS : SHT_NOBITS,
                        .sh_flags = SHF_ALLOC | (info->flags & 1 ? SHF_EXECINSTR : SHF_WRITE),
                        .sh_addr = info->va,
                };

                Elf_Data *data = elf_newdata(section);
                if (!data) {
                        printf("error creating data for PROGBITS section: %s\n", elf_errmsg(-1));
                        return;
                }
                *data = (Elf_Data) {
                        .d_buf = info->ptr,
                        .d_type = ELF_T_BYTE,
                        .d_version = EV_CURRENT,
                        .d_size = info->file_size,
                        .d_align = 1,
                };

                /* Compression is disabled because zlib is too
                 * slow. Instead, write into tmpfs, then use zstd on
                 * the output file. */
#if 0
                shdr->sh_flags &= ~((uint64_t)SHF_ALLOC);
                if (elf_compress(section, ELFCOMPRESS_ZLIB, 0) < 0)
                    printf("failure in elf_compress(): %s\n", elf_errmsg(-1));
                elf_flagscn(section, ELF_C_SET, ELF_F_DIRTY);
#endif

                info->shdr = elf64_getshdr(section);
        }

        struct job_shader_info si = {0};
        if (core->si)
                si = *(core->si);

        Elf64_Sym *sym_tab = calloc(sizeof(*sym_tab), si.num_sym);
        if (si.num_sym) {
                unsigned sym_tab_idx = 0;
                unsigned num_instr = 0;
                for (struct job_shader_instr *instr = si.next; instr; instr = instr->next) {
                        if (instr->type == SI_INSTR)
                                ++num_instr;

                        if (instr->type != SI_SYM)
                                continue;

                        uint64_t value = num_instr * 8;

                        if (sym_tab_idx)
                                sym_tab[sym_tab_idx - 1].st_size = value - sym_tab[sym_tab_idx - 1].st_value;


                        sym_tab[sym_tab_idx++] = (Elf64_Sym) {
                                .st_info = ELF64_ST_INFO(STB_GLOBAL, STT_FUNC),
                                .st_other = ELF64_ST_VISIBILITY(STV_DEFAULT),
                                .st_shndx = trampoline_index,
                                .st_value = value,
                        };

                        instr->str = dwelf_strtab_add(shst, instr->sym);
                }

                sym_tab[sym_tab_idx - 1].st_size = num_instr * 8 - sym_tab[sym_tab_idx - 1].st_value;
        }

        Elf_Scn *sym_scn = elf_newscn(elf);
        if (!sym_scn) {
                printf("error creating SYMTAB section: %s\n", elf_errmsg(-1));
                return;
        }
        Elf64_Shdr *sym_shdr = elf64_getshdr(sym_scn);
        if (!sym_shdr) {
                printf("error getting SYMTAB section header: %s\n", elf_errmsg(-1));
                return;
        }
        *sym_shdr = (Elf64_Shdr) {
                .sh_type = SHT_SYMTAB,
                .sh_entsize = sizeof(Elf64_Sym),
        };

        Elf_Scn *str_scn = elf_newscn(elf);
        if (!str_scn) {
                printf("error creating STRTAB section: %s\n", elf_errmsg(-1));
                return;
        }
        Elf64_Shdr *str_shdr = elf64_getshdr(str_scn);
        if (!str_shdr) {
                printf("error getting STRTAB section header: %s\n", elf_errmsg(-1));
                return;
        }
        Dwelf_Strent *systrtabse = dwelf_strtab_add(shst, ".symtab");
        Dwelf_Strent *shstrtabse = dwelf_strtab_add(shst, ".shstrtab");

        *str_shdr = (Elf64_Shdr) {
                .sh_type = SHT_STRTAB,
                .sh_entsize = 1,
        };
        elf64_getehdr(elf)->e_shstrndx = elf_ndxscn(str_scn);

        dwelf_strtab_finalize(shst, elf_newdata(str_scn));

        for (struct pan_section_info *info = core->sections; info; info = info->next) {
                info->shdr->sh_name = dwelf_strent_off(info->str);
        }
        if (si.num_sym) {
                unsigned sym_tab_idx = 0;
                for (struct job_shader_instr *instr = si.next; instr; instr = instr->next) {
                        if (instr->type != SI_SYM)
                                continue;
                        sym_tab[sym_tab_idx++].st_name = dwelf_strent_off(instr->str);
                }
        }
        str_shdr->sh_name = dwelf_strent_off(shstrtabse);
        sym_shdr->sh_name = dwelf_strent_off(systrtabse);
        sym_shdr->sh_link = elf_ndxscn(str_scn);

        Elf_Data *data = elf_newdata(sym_scn);
        if (!data) {
                printf("error creating data for SYMTAB section: %s\n", elf_errmsg(-1));
                return;
        }
        *data = (Elf_Data) {
                .d_buf = sym_tab,
                .d_type = ELF_T_SYM,
                .d_version = EV_CURRENT,
                .d_size = sizeof(*sym_tab) * si.num_sym,
                .d_align = 8,
        };

        if (elf_update(elf, ELF_C_NULL) < 0)
        {
                printf("failure in elf_update(NULL): %s\n", elf_errmsg(-1));
                return;
        }

        for (struct pan_section_info *info = core->sections; info; info = info->next) {
                if (!info->ptr)
                        continue;

                *(phdr++) = (Elf64_Phdr) {
                        .p_type = PT_LOAD,
                        .p_offset = info->shdr->sh_offset,
                        .p_vaddr = info->va,
                        .p_paddr = 0,
                        .p_filesz = info->file_size,
                        .p_memsz = info->size,
                        .p_flags = PF_R | (info->flags & 1 ? PF_X : PF_W),
                        .p_align = 1,
                };
        }

        if (elf_update(elf, ELF_C_WRITE) < 0)
        {
                printf("failure in elf_update(WRITE): %s\n", elf_errmsg(-1));
                return;
        }
        elf_end(elf);

        struct pan_section_info *info = core->sections;
        while (info) {
                struct pan_section_info *ptr = info;
                info = info->next;
                free(ptr);
        }
        free(core);
}
