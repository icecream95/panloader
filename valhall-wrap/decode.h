/*
 * Copyright (C) 2017-2019 Lyude Paul
 * Copyright (C) 2017-2019 Alyssa Rosenzweig
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#ifndef __PAN_DECODE_H__
#define __PAN_DECODE_H__

#include "panfrost-job.h"

#include "wrap.h"
#include "pan_core.h"

extern FILE *pandecode_dump_stream;

void pandecode_dump_file_open(void);

struct pandecode_xref_entry {
        struct pandecode_xref_entry *next;
        uint64_t from;
        uint16_t mapping;
        uint8_t byte_offset;
};

struct pandecode_xref_list {
        struct pandecode_xref_entry *head;
};

struct pandecode_mapped_memory {
        size_t length;
        void *addr;
        uint64_t gpu_va;
	int flags;
        bool ro;
        char name[32];
        struct pandecode_xref_list *xrefs;
};

char *pointer_as_memory_reference(uint64_t ptr);

struct pandecode_mapped_memory *pandecode_find_mapped_gpu_mem_containing(uint64_t addr);

void pandecode_map_read_write(void);

static inline void
pandecode_set_label(struct pandecode_mapped_memory *mem, const char *str, unsigned *count)
{
    // TODO: what if a label is already set?
    ++*count;
    snprintf(mem->name, sizeof(mem->name), "%s %d", str, *count);
}

static inline void
pandecode_set_va_label(uint64_t gpu_va, const char *str, unsigned *count)
{
    struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
    if (!mem)
        return;
    pandecode_set_label(mem, str, count);
}

#define SET_LABEL(mem, str) do { static unsigned count = 0; pandecode_set_label(mem, str, &count); } while (0)
#define SET_LABEL_VA(va, str) do { static unsigned count = 0; pandecode_set_va_label(va, str, &count); } while (0)

static inline void *
__pandecode_fetch_gpu_mem(const struct pandecode_mapped_memory *mem,
                          uint64_t gpu_va, size_t size,
                          int line, const char *filename)
{
        if (!mem)
                mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);

        if (!mem) {
                fprintf(stderr, "Access to unknown memory %" PRIx64 " in %s:%d\n",
                        gpu_va, filename, line);
//                assert(0);
        }

//        assert(mem);
        assert(size + (gpu_va - mem->gpu_va) <= mem->length);

        return mem->addr + gpu_va - mem->gpu_va;
}

#define pandecode_fetch_gpu_mem(mem, gpu_va, size) \
	__pandecode_fetch_gpu_mem(mem, gpu_va, size, __LINE__, __FILE__)

/* Returns a validated pointer to mapped GPU memory with the given pointer type,
 * size automatically determined from the pointer type
 */
#define PANDECODE_PTR(mem, gpu_va, type) \
	((type*)(__pandecode_fetch_gpu_mem(mem, gpu_va, sizeof(type), \
					 __LINE__, __FILE__)))

/* Usage: <variable type> PANDECODE_PTR_VAR(name, mem, gpu_va) */
#define PANDECODE_PTR_VAR(name, mem, gpu_va) \
	name = __pandecode_fetch_gpu_mem(mem, gpu_va, sizeof(*name), \
				       __LINE__, __FILE__)

__attribute__((optimize("-O3")))
static inline void
hexdump(FILE *fp, const uint8_t *hex, size_t cnt,
        struct pandecode_xref_list *xrefs,
        mali_ptr min_va, mali_ptr max_va)
{
   for (unsigned i = 0; i < cnt; ++i) {
      if ((i & 0xF) == 0)
         fprintf(fp, "%06X ", i);

      uint8_t v = hex[i];

      if ((v == 0 || v == 0xAC || v == 0xFA) && (i & 0xF) == 0) {
         /* Check if we're starting an aligned run of zeroes */
         unsigned zero_count = 0;

         uint64_t wide = (v << 8) | v;
         wide = (wide << 16) | wide;
         wide = (wide << 32) | wide;

         // TODO: What if cnt % 16 != 0?
         for (unsigned j = i; j < cnt; j += 16) {
            uint64_t *val = (uint64_t *)&hex[j];
            void *xref = xrefs ? xrefs[j / 16].head : NULL;
            uint64_t local[2];
            // GCC *still* still uses two ldr and not ldp..
            memcpy(local, val, 16);

            if (local[0] == wide && local[1] == wide && !xref)
               zero_count += 16;
            else
               break;
         }

         if (zero_count >= 32) {
            switch (v) {
            case 0: fprintf(fp, " *\n"); break;
            case 0xAC: fprintf(fp, " **\n"); break;
            case 0xFA: fprintf(fp, " *?\n"); break;
            }
            i += (zero_count & ~0xF) - 1;
            continue;
         }
      }

      fprintf(fp, " %02X", hex[i]);

      if ((i & 0xF) == 0xF && xrefs && xrefs[i / 16].head) {
          fprintf(fp, "  XREFS:");

          struct pandecode_xref_entry *e = xrefs[i / 16].head;
          unsigned count = 0;
          while (e && count < 16) {
              fprintf(fp, " %"PRIx64, e->from);
              if (e->byte_offset)
                  fprintf(fp, "->%i", e->byte_offset);
              e = e->next;
              ++count;
          }
      }

      if ((i & 0xF) == 0xF && max_va) {
          for (unsigned j = 0; j < 16; j += 8) {
              uint64_t value = ((uint64_t *)hex)[(i + j - 15)  / 8];

              value &= 0xfffffffffff0ULL; // 48 bits
              if (value < min_va || value >= max_va)
                  continue;

              struct pandecode_mapped_memory *mem =
                  pandecode_find_mapped_gpu_mem_containing(value);
              if (!mem)
                  continue;

              unsigned offset = value - mem->gpu_va;
              const uint8_t *ref_hex = (const uint8_t *)mem->addr + offset;

              fprintf(fp, "  [%i] -> %06X", j, offset);
              for (unsigned i = 0; i < 16; ++i)
                  fprintf(fp, " %02X", ref_hex[i]);
          }
      }

/*      if ((i & 0xF) == 0xF && with_strings) {
         fprintf(fp, " | ");
         for (unsigned j = i & ~0xF; j <= i; ++j) {
            uint8_t c = hex[j];
            fputc((c < 32 || c > 128) ? '.' : c, fp);
         }
      }*/

      if ((i & 0xF) == 0xF)
         fprintf(fp, "\n");
   }

   fprintf(fp, "\n");
}

struct midgard_disasm_stats {
        /* Counts gleaned from disassembly, or negative if the field cannot be
         * inferred, for instance due to indirect access. If negative, the abs
         * is the upper limit for the count. */

        signed texture_count;
        signed sampler_count;
        signed attribute_count;
        signed varying_count;
        signed uniform_count;
        signed uniform_buffer_count;
        signed work_count;

        /* These are pseudometrics for shader-db */
        unsigned instruction_count;
        unsigned bundle_count;
        unsigned quadword_count;

        /* Should we enable helper invocations? */
        bool helper_invocations;
};

#if PAN_ARCH == 9
#define GENX(X) X##_v9
#else
#define GENX(X) X##_v10
#endif
#define PRINTFLIKE(x,y)

static inline struct job_shader_instr *
si_add(struct job_shader_info *si, struct job_shader_instr i)
{
    struct job_shader_instr *ri = calloc(sizeof(*ri), 1);
    *ri = i;

    if (si->end)
        si->end->next = ri;
    else
        si->next = ri;

    si->end = ri;

    if (ri->type == SI_INSTR)
        ++si->num_instr;
    else
        ++si->num_sym;

    return ri;
}

/* Only six bits may be used for flags */
#define SI_WRITE_HIGH 0x1
#define SI_CLEAR_HIGH 0x2

static inline struct job_shader_instr *
si_add_instr_32(struct job_shader_info *si, uint8_t op, uint32_t imm, uint8_t flags)
{
    /* Little-endian ordering:
     * op imm1 imm2 imm3 imm4 flags 00000000 00000000
     */
    return si_add(si, (struct job_shader_instr) {
            .type = SI_INSTR,
            .instr = op | ((uint64_t)imm << 8) | ((uint64_t)flags << 40),
        });
}

static inline void
si_add_instr_64(struct job_shader_info *si, uint8_t op, uint64_t imm, uint8_t flags)
{
    if (imm < (1ULL << 32)) {
        si_add_instr_32(si, op, imm, flags | SI_CLEAR_HIGH);
    } else {
        si_add_instr_32(si, op, imm, flags);
        si_add_instr_32(si, op, imm >> 32, flags | SI_WRITE_HIGH);
    }
}

static inline void
si_set_shader(struct job_shader_info *si, const char *name)
{
    si_add(si, (struct job_shader_instr) {
            .type = SI_SYM,
            .sym = name,
        });
}

#define SI_OP_BRANCH 1
#define SI_OP_SET_REG 2
#define SI_OP_SET_FAU 3
#define SI_OP_SET_RESOURCE 4

static inline void
si_set_fau(struct job_shader_info *si, mali_ptr va)
{
    si_add_instr_64(si, SI_OP_SET_FAU, va, 0x0);
}

static inline void
si_set_resource(struct job_shader_info *si, mali_ptr addr)
{
    si_add_instr_64(si, SI_OP_SET_RESOURCE, addr, 0x0);
}

static inline void
si_set_branch(struct job_shader_info *si, mali_ptr addr)
{
    si_add_instr_64(si, SI_OP_BRANCH, addr, 0x0);
}

#endif /* __MMAP_TRACE_H__ */
