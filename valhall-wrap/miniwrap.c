/*
 * Copyright (C) 2017-2019 Alyssa Rosenzweig
 * Copyright (C) 2017-2019 Connor Abbott
 * Copyright (C) 2019 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <fcntl.h>
#include <pack.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>
#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/mman.h>
#include <mali-ioctl.h>

#include "decode.h"
#include "panfrost-job.h"
#include "pan_core.h"

#include "disassemble.h"

FILE *pandecode_dump_stream;

/* Memory handling, this can't pull in proper data structures so hardcode some
 * things, it should be "good enough" for most use cases */

#define MAX_MAPPINGS 4096

struct pandecode_mapped_memory mmap_array[MAX_MAPPINGS];
unsigned mmap_count = 0;

struct pandecode_mapped_memory *ro_mappings[MAX_MAPPINGS];
unsigned ro_mapping_count = 0;

static struct pandecode_mapped_memory *
pandecode_find_mapped_gpu_mem_containing_rw(uint64_t addr)
{
        for (unsigned i = 0; i < mmap_count; ++i) {
                if (addr >= mmap_array[i].gpu_va && (addr - mmap_array[i].gpu_va) < mmap_array[i].length)
                        return mmap_array + i;
        }

        return NULL;
}

struct pandecode_mapped_memory *
pandecode_find_mapped_gpu_mem_containing(uint64_t addr)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing_rw(addr);

        if (mem && mem->addr && !mem->ro) {
//                mprotect(mem->addr, mem->length, PROT_READ);
                mem->ro = true;
                ro_mappings[ro_mapping_count++] = mem;
                assert(ro_mapping_count < MAX_MAPPINGS);
        }

        return mem;
}

void
pandecode_map_read_write(void)
{
        for (unsigned i = 0; i < ro_mapping_count; ++i) {
                ro_mappings[i]->ro = false;
//                mprotect(ro_mappings[i]->addr, ro_mappings[i]->length,
 //                               PROT_READ | PROT_WRITE);
        }

        ro_mapping_count = 0;
}

static void
pandecode_add_name(struct pandecode_mapped_memory *mem, uint64_t gpu_va, const char *name)
{
        if (!name) {
                /* If we don't have a name, assign one */

                snprintf(mem->name, sizeof(mem->name) - 1,
                         "memory_%" PRIx64, gpu_va);
        } else {
                assert((strlen(name) + 1) < sizeof(mem->name));
                memcpy(mem->name, name, strlen(name) + 1);
        }
}

void
pandecode_inject_mmap(uint64_t gpu_va, void *cpu, unsigned sz, const char *name, int flags)
{
        /* First, search if we already mapped this and are just updating an address */

        struct pandecode_mapped_memory *existing =
                pandecode_find_mapped_gpu_mem_containing_rw(gpu_va);

        if (existing && existing->gpu_va == gpu_va) {
        printf("Inject_mmap %"PRIx64" %p %i %s 0x%x\n",
               gpu_va, cpu, sz, name, flags);

                existing->length = sz;
                existing->addr = cpu;
                pandecode_add_name(existing, gpu_va, name);
                return;
        }
        printf("inject_mmap %"PRIx64" %p %i %s 0x%x\n",
               gpu_va, cpu, sz, name, flags);

        assert((mmap_count + 1) < MAX_MAPPINGS);
        mmap_array[mmap_count++] = (struct pandecode_mapped_memory) {
                .addr = cpu,
                .gpu_va = gpu_va,
                .length = sz,
		.flags = flags
        };

	pandecode_add_name(&mmap_array[mmap_count - 1], gpu_va, name);
}

void
pandecode_cs_queue_bind(uint64_t gpu_va, unsigned group_handle, unsigned csi_index)
{
        struct pandecode_mapped_memory *existing =
                pandecode_find_mapped_gpu_mem_containing_rw(gpu_va);

        // TODO
        if (!existing)
                return;

        char buffer[32];
        /* Assign to a variable to silence format-truncation warning  */
        UNUSED int ret = snprintf(buffer, 32, "cs_%i_%i_%s",
                                  group_handle, csi_index, existing->name);
        memcpy(existing->name, buffer, 32);
}

char *
pointer_as_memory_reference(uint64_t ptr)
{
        struct pandecode_mapped_memory *mapped;
        char *out = malloc(128);

        /* Try to find the corresponding mapped zone */

        mapped = pandecode_find_mapped_gpu_mem_containing_rw(ptr);

        if (mapped) {
                snprintf(out, 128, "%s + %d", mapped->name, (int) (ptr - mapped->gpu_va));
                return out;
        }

        /* Just use the raw address if other options are exhausted */

        snprintf(out, 128, "0x%" PRIx64, ptr);
        return out;

}

static int pandecode_dump_frame_count = 0;

static bool force_stderr = false;

void
pandecode_dump_file_open(void)
{
        if (pandecode_dump_stream)
                return;

        /* This does a getenv every frame, so it is possible to use
         * setenv to change the base at runtime.
         */
        const char *dump_file_base = getenv("PANDECODE_DUMP_FILE") ?: "pandecode.dump";
        if (force_stderr || !strcmp(dump_file_base, "stderr"))
                pandecode_dump_stream = stderr;
        else {
                char buffer[1024];
                snprintf(buffer, sizeof(buffer), "%s.%04d", dump_file_base, pandecode_dump_frame_count);
                fprintf(stderr, "pandecode: dump command stream to file %s\n", buffer);
                pandecode_dump_stream = fopen(buffer, "w");
                if (!pandecode_dump_stream)
                        fprintf(stderr,
                                "pandecode: failed to open command stream log file %s\n",
                                buffer);

                setlinebuf(pandecode_dump_stream);
        }
}

static void
pandecode_dump_file_close(void)
{
        if (pandecode_dump_stream && pandecode_dump_stream != stderr) {
                fclose(pandecode_dump_stream);
                pandecode_dump_stream = NULL;
        }
}

void
pandecode_initialize(bool to_stderr)
{
    force_stderr = false;//to_stderr;
}

void
pandecode_next_frame(void)
{
        pandecode_dump_file_close();
        pandecode_dump_frame_count++;
}

void
pandecode_reset_frame(void)
{
        pandecode_dump_frame_count = -1;
}

void
pandecode_close(void)
{
        pandecode_dump_file_close();
}

static bool
can_dump(struct pandecode_mapped_memory *mem)
{
    if (mem->gpu_va < 0x50000)
        return false;

    if (mem->flags & MALI_MEM_GROW_ON_GPF)
        return false;

    /* mincore doesn't prevent SIGBUS.. Maybe we can try a read
     * syscall from the memory? (Or do we just need to check the end,
     * not the start?) */
    return true;

    unsigned char vec;
    int ret = mincore(mem->addr, 1, &vec);
    if (ret)
        perror("mincore");

    return ret == 0;
}

//#define CORE_DUMP

void
pandecode_dump_mappings(struct job_shader_info *si)
{
   pandecode_next_frame();
   pandecode_dump_file_open();

   const char *dump_file_base = getenv("PANDECODE_DUMP_FILE") ?: "pandecode.dump";

   FILE *dis_stream = NULL;
   {
       char buffer[1024];
       snprintf(buffer, sizeof(buffer), "%s.shaders", dump_file_base);
       dis_stream = fopen(buffer, "w");
   }

#ifdef CORE_DUMP
   static int core_dump_count = 0;
   ++core_dump_count;

   char buffer[1024];
   snprintf(buffer, sizeof(buffer), "%s.%d.%04d.core", dump_file_base, core_dump_count, pandecode_dump_frame_count);
   int core_file = creat(buffer, 0666);
   struct pan_core *core = panfrost_core_create(core_file);

   if (si)
       panfrost_core_add_si(core, si);
#endif

   mali_ptr min_va = ~0ULL;
   mali_ptr max_va = 0;

   for (unsigned i = 0; i < mmap_count; ++i) {
       if (!can_dump(&mmap_array[i]))
           continue;

       if (mmap_array[i].gpu_va < min_va)
           min_va = mmap_array[i].gpu_va;
       if (mmap_array[i].gpu_va + mmap_array[i].length > max_va)
           max_va = mmap_array[i].gpu_va + mmap_array[i].length;

       mmap_array[i].xrefs = calloc(sizeof(struct pandecode_xref_list),
                                    mmap_array[i].length / 16);
   }

   for (signed i = mmap_count - 1; i >= 0; --i) {
       if (!can_dump(&mmap_array[i]))
           continue;

       uint64_t *start = mmap_array[i].addr;
       uint64_t *end = (uint64_t *)((uint8_t *)mmap_array[i].addr + mmap_array[i].length);

       for (uint64_t *data = end - 1; data >= start; --data) {
           uint64_t a = *data & 0xffffffffffffULL; // 48 bits
           if (a < min_va || a >= max_va)
               continue;

           struct pandecode_mapped_memory *mem =
               pandecode_find_mapped_gpu_mem_containing(a);
           if (!mem || !mem->xrefs)
               continue;

           unsigned position = (a - mem->gpu_va) / 16;

           struct pandecode_xref_entry *e = calloc(sizeof(struct pandecode_xref_entry), 1);
           *e = (struct pandecode_xref_entry) {
               .from = mmap_array[i].gpu_va + (data - start) * 8,
               .next = mem->xrefs[position].head,
               .mapping = i,
               .byte_offset = a % 16,
           };
           mem->xrefs[position].head = e;
       }
   }

   for (unsigned i = 0; i < mmap_count; ++i) {
       // todo: is gpu_va too restrictive for real hw?
       if (mmap_array[i].gpu_va < 0x50000)
           continue;

#ifdef CORE_DUMP
       if (mmap_array[i].length && !(mmap_array[i].flags & MALI_MEM_GROW_ON_GPF)) {

           // TODO: Translate flags
           panfrost_core_add(core, mmap_array[i].gpu_va, mmap_array[i].length, mmap_array[i].addr, mmap_array[i].name, 0);
       }
#endif

      if (mmap_array[i].flags & MALI_MEM_GPU_VA_SAME_4GB_PAGE)
	      fprintf(pandecode_dump_stream, "(TLS)\n");

      if (mmap_array[i].flags & MALI_MEM_GROW_ON_GPF) {
	      fprintf(pandecode_dump_stream, "Buffer: %s gpu %" PRIx64 " flags %X (growable)\n\n",
		      mmap_array[i].name, mmap_array[i].gpu_va, mmap_array[i].flags);

	      continue;
      } else if (!(mmap_array[i].flags & MALI_MEM_PROT_CPU_RD)) {
	      fprintf(pandecode_dump_stream, "Buffer: %s gpu %" PRIx64 " flags %X (invisible)\n\n",
		      mmap_array[i].name, mmap_array[i].gpu_va, mmap_array[i].flags);

	      continue;
      } else if (mmap_array[i].flags & MALI_MEM_PROT_GPU_EX) {
	      fprintf(pandecode_dump_stream, "Buffer: %s gpu %" PRIx64 " flags %X (shader)\n\n",
		      mmap_array[i].name, mmap_array[i].gpu_va, mmap_array[i].flags);

	      fprintf(dis_stream, "Buffer: %s gpu %" PRIx64 " flags %X (shader)\n\n",
		      mmap_array[i].name, mmap_array[i].gpu_va, mmap_array[i].flags);

              disassemble_valhall(dis_stream, mmap_array[i].addr, mmap_array[i].length, mmap_array[i].xrefs);

	      continue;
      }

      fprintf(pandecode_dump_stream, "Buffer: %s gpu %" PRIx64 " flags %X len %zX\n\n",
              mmap_array[i].name, mmap_array[i].gpu_va, mmap_array[i].flags, mmap_array[i].length);

      if (!mmap_array[i].addr || !mmap_array[i].length || !can_dump(&mmap_array[i]))
           continue;

      uint64_t *dump = mmap_array[i].addr;
      if (pandecode_dump_frame_count == 0 &&
          dump[0] == mmap_array[i].gpu_va &&
          (dump[1] & 0xaaaa00000000) == 0xaaaa00000000)
          dump[1] = 0xaaaa00000000;
      if (pandecode_dump_frame_count == 0 &&
          dump[6] == mmap_array[i].gpu_va &&
          (dump[7] & 0xaaaa00000000) == 0xaaaa00000000)
          dump[7] = 0xaaaa00000000;

      hexdump(pandecode_dump_stream, mmap_array[i].addr, mmap_array[i].length, mmap_array[i].xrefs, min_va, max_va);
      fprintf(pandecode_dump_stream, "\n");

#if PAN_ARCH >= 10
      if (strncmp(mmap_array[i].name, "cs_", 3) == 0) {
          GENX(pandecode_cs_only)(mmap_array[i].gpu_va, 0);
          // TODO: Actually do something with the shader info
          struct job_shader_info *si = job_shader_info_create();
          GENX(pandecode_cs)(mmap_array[i].gpu_va, 0, si);
          job_shader_info_free(si);
      }
#endif
   }

   for (unsigned i = 0; i < mmap_count; ++i) {
       if (mmap_array[i].xrefs) {
           for (unsigned j = 0; j < mmap_array[i].length / 16; ++j) {
               while (mmap_array[i].xrefs[j].head) {
                   struct pandecode_xref_entry *e = mmap_array[i].xrefs[j].head;
                   mmap_array[i].xrefs[j].head = e->next;
                   free(e);
               }
           }
       }

       free(mmap_array[i].xrefs);
       mmap_array[i].xrefs = NULL;
   }

   fclose(dis_stream);
   fflush(pandecode_dump_stream);

#ifdef CORE_DUMP
   panfrost_core_finish(core);
   close(core_file);
#endif
}

struct mali_jit_alloc {
	u64 gpu_addr; /* allocated address */
	u64 va_pages, commit_pages, extension;
	u8 id, bin_id, max_allocs, flags;
	u8 padding[2];
	u16 usage_id;
	u64 heap_info_gpu_addr;
} __attribute__((packed));

void
pandecode_jit_alloc(u64 jc, unsigned count)
{
	struct mali_jit_alloc *cl = (void *) (uintptr_t) jc;

	for (unsigned i = 0; i < count; ++i) {
		printf("\n");
		printf("JIT alloc\n");
		printf("GPU address: 0x%" PRIx64"\n", cl[i].gpu_addr);
		printf("VA pages: 0x%" PRIx64"\n", cl[i].va_pages);
		printf("Commit pages: 0x%" PRIx64"\n", cl[i].commit_pages);
		printf("Extension: 0x%" PRIx64"\n", cl[i].extension);
		printf("ID: 0x%" PRIx8"\n", cl[i].id);
		printf("BinID: 0x%" PRIx8"\n", cl[i].bin_id);
		printf("Max allocs: 0x%" PRIx8"\n", cl[i].max_allocs);
		printf("Flags: 0x%" PRIx8"\n", cl[i].flags);
		printf("Usage ID: 0x%" PRIx16"\n", cl[i].usage_id);
		printf("Heap info GPU address: 0x%" PRIx64"\n", cl[i].heap_info_gpu_addr);
		printf("\n");
	}
}
