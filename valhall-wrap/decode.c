/*
 * Copyright (C) 2017-2019 Alyssa Rosenzweig
 * Copyright (C) 2017-2019 Connor Abbott
 * Copyright (C) 2019 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <pack.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include "decode.h"

#include "disassemble.h"

#define DUMP_UNPACKED(T, var, ...) { \
        pandecode_log(__VA_ARGS__); \
        pan_print(pandecode_dump_stream, T, var, (pandecode_indent + 1) * 2); \
}

#define DUMP_CL(T, cl, ...) {\
        pan_unpack(cl, T, temp); \
        DUMP_UNPACKED(T, temp, __VA_ARGS__); \
}

#define DUMP_SECTION(A, S, cl, ...) { \
        pan_section_unpack(cl, A, S, temp); \
        pandecode_log(__VA_ARGS__); \
        pan_section_print(pandecode_dump_stream, A, S, temp, (pandecode_indent + 1) * 2); \
}

#define MAP_ADDR(T, addr, cl) \
        const uint8_t *cl = 0; \
        { \
                struct pandecode_mapped_memory *mapped_mem = pandecode_find_mapped_gpu_mem_containing(addr); \
                cl = pandecode_fetch_gpu_mem(mapped_mem, addr, pan_size(T)); \
        }

#define DUMP_ADDR(T, addr, ...) {\
        MAP_ADDR(T, addr, cl) \
        DUMP_CL(T, cl, __VA_ARGS__); \
}

/* Semantic logging type.
 *
 * Raw: for raw messages to be printed as is.
 * Message: for helpful information to be commented out in replays.
 *
 * Use one of pandecode_log or pandecode_msg as syntax sugar.
 */

enum pandecode_log_type {
        PANDECODE_RAW,
        PANDECODE_MESSAGE,
};

#define pandecode_log(...)  pandecode_log_typed(PANDECODE_RAW,      __VA_ARGS__)
#define pandecode_msg(...)  pandecode_log_typed(PANDECODE_MESSAGE,  __VA_ARGS__)

static unsigned pandecode_indent = 0;

static void
pandecode_make_indent(void)
{
        for (unsigned i = 0; i < pandecode_indent; ++i)
                fprintf(pandecode_dump_stream, "  ");
}

static void PRINTFLIKE(2, 3)
pandecode_log_typed(enum pandecode_log_type type, const char *format, ...)
{
        va_list ap;

        pandecode_make_indent();

        if (type == PANDECODE_MESSAGE)
                fprintf(pandecode_dump_stream, "// ");

        va_start(ap, format);
        vfprintf(pandecode_dump_stream, format, ap);
        va_end(ap);
}

static void
pandecode_log_cont(const char *format, ...)
{
        va_list ap;

        va_start(ap, format);
        vfprintf(pandecode_dump_stream, format, ap);
        va_end(ap);
}

/* To check for memory safety issues, validates that the given pointer in GPU
 * memory is valid, containing at least sz bytes. The goal is to eliminate
 * GPU-side memory bugs (NULL pointer dereferences, buffer overflows, or buffer
 * overruns) by statically validating pointers.
 */

static void
pandecode_validate_buffer(mali_ptr addr, size_t sz)
{
        if (!addr) {
                pandecode_msg("XXX: null pointer deref");
                return;
        }

        /* Find a BO */

        struct pandecode_mapped_memory *bo =
                pandecode_find_mapped_gpu_mem_containing(addr);

        if (!bo) {
                pandecode_msg("XXX: invalid memory dereference\n");
                return;
        }

        /* Bounds check */

        unsigned offset = addr - bo->gpu_va;
        unsigned total = offset + sz;

        if (total > bo->length) {
                pandecode_msg("XXX: buffer overrun. "
                                "Chunk of size %zu at offset %d in buffer of size %zu. "
                                "Overrun by %zu bytes. \n",
                                sz, offset, bo->length, total - bo->length);
                return;
        }
}

#if PAN_ARCH <= 5
/* Midgard's tiler descriptor is embedded within the
 * larger FBD */

static void
pandecode_midgard_tiler_descriptor(
                const struct mali_tiler_context_packed *tp,
                const struct mali_tiler_weights_packed *wp)
{
        pan_unpack(tp, TILER_CONTEXT, t);
        DUMP_UNPACKED(TILER_CONTEXT, t, "Tiler:\n");

        /* We've never seen weights used in practice, but they exist */
        pan_unpack(wp, TILER_WEIGHTS, w);
        bool nonzero_weights = false;

        nonzero_weights |= w.weight0 != 0x0;
        nonzero_weights |= w.weight1 != 0x0;
        nonzero_weights |= w.weight2 != 0x0;
        nonzero_weights |= w.weight3 != 0x0;
        nonzero_weights |= w.weight4 != 0x0;
        nonzero_weights |= w.weight5 != 0x0;
        nonzero_weights |= w.weight6 != 0x0;
        nonzero_weights |= w.weight7 != 0x0;

        if (nonzero_weights)
                DUMP_UNPACKED(TILER_WEIGHTS, w, "Tiler Weights:\n");
}
#endif

/* Information about the framebuffer passed back for
 * additional analysis */

struct pandecode_fbd {
        unsigned width;
        unsigned height;
        unsigned rt_count;
        bool has_extra;
};

#if PAN_ARCH == 4
static struct pandecode_fbd
pandecode_sfbd(uint64_t gpu_va, int job_no, bool is_fragment, unsigned gpu_id)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
        const void *PANDECODE_PTR_VAR(s, mem, (mali_ptr) gpu_va);

        struct pandecode_fbd info = {
                .has_extra = false,
                .rt_count = 1
        };

        pandecode_log("Framebuffer:\n");
        pandecode_indent++;

        DUMP_SECTION(FRAMEBUFFER, LOCAL_STORAGE, s, "Local Storage:\n");
        pan_section_unpack(s, FRAMEBUFFER, PARAMETERS, p);
        DUMP_UNPACKED(FRAMEBUFFER_PARAMETERS, p, "Parameters:\n");

        const void *t = pan_section_ptr(s, FRAMEBUFFER, TILER);
        const void *w = pan_section_ptr(s, FRAMEBUFFER, TILER_WEIGHTS);

        pandecode_midgard_tiler_descriptor(t, w);

        pandecode_indent--;

        /* Dummy unpack of the padding section to make sure all words are 0.
         * No need to call print here since the section is supposed to be empty.
         */
        pan_section_unpack(s, FRAMEBUFFER, PADDING_1, padding1);
        pan_section_unpack(s, FRAMEBUFFER, PADDING_2, padding2);
        pandecode_log("\n");

        return info;
}
#endif

#if PAN_ARCH >= 5
static void
pandecode_local_storage(uint64_t gpu_va, int job_no)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
        const struct mali_local_storage_packed *PANDECODE_PTR_VAR(s, mem, (mali_ptr) gpu_va);
        DUMP_CL(LOCAL_STORAGE, s, "Local Storage:\n");
}

static void
pandecode_render_target(uint64_t gpu_va, unsigned job_no, unsigned gpu_id,
                        const struct MALI_FRAMEBUFFER_PARAMETERS *fb)
{
        pandecode_log("Color Render Targets:\n");
        pandecode_indent++;

        for (int i = 0; i < (fb->render_target_count); i++) {
                mali_ptr rt_va = gpu_va + i * pan_size(RENDER_TARGET);
                struct pandecode_mapped_memory *mem =
                        pandecode_find_mapped_gpu_mem_containing(rt_va);
                const struct mali_render_target_packed *PANDECODE_PTR_VAR(rtp, mem, (mali_ptr) rt_va);
                pan_unpack(rtp, RENDER_TARGET, temp);
                DUMP_UNPACKED(RENDER_TARGET, temp, "Color Render Target %d:\n", i);

                static unsigned rt_count = 0;
                pandecode_set_va_label(temp.afbc.body, "Render Target", &rt_count);
        }

        pandecode_indent--;
        pandecode_log("\n");
}
#endif

#if PAN_ARCH >= 6
static void
pandecode_sample_locations(const void *fb, int job_no)
{
        pan_section_unpack(fb, FRAMEBUFFER, PARAMETERS, params);

        struct pandecode_mapped_memory *smem =
                pandecode_find_mapped_gpu_mem_containing(params.sample_locations);

        const u16 *PANDECODE_PTR_VAR(samples, smem, params.sample_locations);

        pandecode_log("Sample locations:\n");
        for (int i = 0; i < 33; i++) {
                pandecode_log("  (%d, %d),\n",
                                samples[2 * i] - 128,
                                samples[2 * i + 1] - 128);
        }
}
#endif

static void
pandecode_dcd(const struct MALI_DRAW *p,
              int job_no, enum mali_job_type job_type,
              char *suffix, unsigned gpu_id);

static void
dump_buffer(mali_ptr addr, size_t sz);

#if PAN_ARCH >= 5
UNUSED static struct pandecode_fbd
pandecode_mfbd_bfr(uint64_t gpu_va, int job_no, bool is_fragment, unsigned gpu_id)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
        const void *PANDECODE_PTR_VAR(fb, mem, (mali_ptr) gpu_va);
        pan_section_unpack(fb, FRAMEBUFFER, PARAMETERS, params);

        struct pandecode_fbd info;

#if PAN_ARCH >= 6
        pandecode_sample_locations(fb, job_no);

        pan_section_unpack(fb, FRAMEBUFFER, PARAMETERS, bparams);
#if 0
        unsigned dcd_size = pan_size(DRAW);
        struct pandecode_mapped_memory *dcdmem =
                pandecode_find_mapped_gpu_mem_containing(bparams.frame_shader_dcds);

        if (bparams.pre_frame_0 != MALI_PRE_POST_FRAME_SHADER_MODE_NEVER) {
                const void *PANDECODE_PTR_VAR(dcd, dcdmem, bparams.frame_shader_dcds + (0 * dcd_size));
                pan_unpack(bparams.frame_shader_dcds, DRAW, draw);
		dump_buffer(bparams.frame_shader_dcds, 512);
                pandecode_log("Pre frame 0:\n");
                pandecode_dcd(&draw, job_no, MALI_JOB_TYPE_FRAGMENT, "", gpu_id);
        }

        if (bparams.pre_frame_1 != MALI_PRE_POST_FRAME_SHADER_MODE_NEVER) {
                const void *PANDECODE_PTR_VAR(dcd, dcdmem, bparams.frame_shader_dcds + (1 * dcd_size));
                pan_unpack(dcd, DRAW, draw);
                pandecode_log("Pre frame 1:\n");
                pandecode_dcd(&draw, job_no, MALI_JOB_TYPE_FRAGMENT, "", gpu_id);
        }

        if (bparams.post_frame != MALI_PRE_POST_FRAME_SHADER_MODE_NEVER) {
                const void *PANDECODE_PTR_VAR(dcd, dcdmem, bparams.frame_shader_dcds + (2 * dcd_size));
                pan_unpack(dcd, DRAW, draw);
                pandecode_log("Post frame:\n");
                pandecode_dcd(&draw, job_no, MALI_JOB_TYPE_FRAGMENT, "", gpu_id);
        }
#endif
#endif
 
        pandecode_log("Multi-Target Framebuffer:\n");
        pandecode_indent++;

#if PAN_ARCH <= 5
        DUMP_SECTION(FRAMEBUFFER, LOCAL_STORAGE, fb, "Local Storage:\n");
#endif

        info.width = params.width;
        info.height = params.height;
        info.rt_count = params.render_target_count;
        DUMP_UNPACKED(FRAMEBUFFER_PARAMETERS, params, "Parameters:\n");

#if PAN_ARCH <= 5
        const void *t = pan_section_ptr(fb, FRAMEBUFFER, TILER);
        const void *w = pan_section_ptr(fb, FRAMEBUFFER, TILER_WEIGHTS);
        pandecode_midgard_tiler_descriptor(t, w);
#endif

        pandecode_indent--;
        pandecode_log("\n");

        gpu_va += pan_size(FRAMEBUFFER);

        info.has_extra = params.has_zs_crc_extension;

        if (info.has_extra) {
                struct pandecode_mapped_memory *mem =
                        pandecode_find_mapped_gpu_mem_containing(gpu_va);
                const struct mali_zs_crc_extension_packed *PANDECODE_PTR_VAR(zs_crc, mem, (mali_ptr)gpu_va);
                DUMP_CL(ZS_CRC_EXTENSION, zs_crc, "ZS CRC Extension:\n");
                pandecode_log("\n");

                gpu_va += pan_size(ZS_CRC_EXTENSION);
        }

        if (is_fragment)
                pandecode_render_target(gpu_va, job_no, gpu_id, &params);

        return info;
}
#endif

#if PAN_ARCH <= 7
static void
pandecode_attributes(const struct pandecode_mapped_memory *mem,
                            mali_ptr addr, int job_no, char *suffix,
                            int count, bool varying, enum mali_job_type job_type)
{
        char *prefix = varying ? "Varying" : "Attribute";
        assert(addr);

        if (!count) {
                pandecode_msg("warn: No %s records\n", prefix);
                return;
        }

        MAP_ADDR(ATTRIBUTE_BUFFER, addr, cl);

        for (int i = 0; i < count; ++i) {
                pan_unpack(cl + i * pan_size(ATTRIBUTE_BUFFER), ATTRIBUTE_BUFFER, temp);
                DUMP_UNPACKED(ATTRIBUTE_BUFFER, temp, "%s:\n", prefix);

                switch (temp.type) {
                case MALI_ATTRIBUTE_TYPE_1D_NPOT_DIVISOR_WRITE_REDUCTION:
                case MALI_ATTRIBUTE_TYPE_1D_NPOT_DIVISOR: {
                        pan_unpack(cl + (i + 1) * pan_size(ATTRIBUTE_BUFFER),
                                   ATTRIBUTE_BUFFER_CONTINUATION_NPOT, temp2);
                        pan_print(pandecode_dump_stream, ATTRIBUTE_BUFFER_CONTINUATION_NPOT,
                                  temp2, (pandecode_indent + 1) * 2);
                        i++;
                        break;
                }
                case MALI_ATTRIBUTE_TYPE_3D_LINEAR:
                case MALI_ATTRIBUTE_TYPE_3D_INTERLEAVED: {
                        pan_unpack(cl + (i + 1) * pan_size(ATTRIBUTE_BUFFER_CONTINUATION_3D),
                                   ATTRIBUTE_BUFFER_CONTINUATION_3D, temp2);
                        pan_print(pandecode_dump_stream, ATTRIBUTE_BUFFER_CONTINUATION_3D,
                                  temp2, (pandecode_indent + 1) * 2);
                        i++;
                        break;
                }
                default:
                        break;
                }
        }
        pandecode_log("\n");
}
#endif

#if PAN_ARCH >= 6
/* Decodes a Bifrost blend constant. See the notes in bifrost_blend_rt */

static mali_ptr
pandecode_bifrost_blend(void *descs, int job_no, int rt_no, mali_ptr frag_shader)
{
        pan_unpack(descs + (rt_no * pan_size(BLEND)), BLEND, b);
        DUMP_UNPACKED(BLEND, b, "Blend RT %d:\n", rt_no);
        if (b.internal.mode != MALI_BLEND_MODE_SHADER)
                return 0;

        return (frag_shader & 0xFFFFFFFF00000000ULL) | b.internal.shader.pc;
}
#elif PAN_ARCH == 5
static mali_ptr
pandecode_midgard_blend_mrt(void *descs, int job_no, int rt_no)
{
        pan_unpack(descs + (rt_no * pan_size(BLEND)), BLEND, b);
        DUMP_UNPACKED(BLEND, b, "Blend RT %d:\n", rt_no);
        return b.blend_shader ? (b.shader_pc & ~0xf) : 0;
}
#endif

#if PAN_ARCH <= 7
static unsigned
pandecode_attribute_meta(int count, mali_ptr attribute, bool varying)
{
        unsigned max = 0;

        for (int i = 0; i < count; ++i, attribute += pan_size(ATTRIBUTE)) {
                MAP_ADDR(ATTRIBUTE, attribute, cl);
                pan_unpack(cl, ATTRIBUTE, a);
                DUMP_UNPACKED(ATTRIBUTE, a, "%s:\n", varying ? "Varying" : "Attribute");
                max = MAX2(max, a.buffer_index);
        }

        pandecode_log("\n");
        return MIN2(max + 1, 256);
}

/* return bits [lo, hi) of word */
static u32
bits(u32 word, u32 lo, u32 hi)
{
        if (hi - lo >= 32)
                return word; // avoid undefined behavior with the shift

        if (lo >= 32)
                return 0;

        return (word >> lo) & ((1 << (hi - lo)) - 1);
}

static void
pandecode_invocation(const void *i)
{
        /* Decode invocation_count. See the comment before the definition of
         * invocation_count for an explanation.
         */
        pan_unpack(i, INVOCATION, invocation);

        unsigned size_x = bits(invocation.invocations, 0, invocation.size_y_shift) + 1;
        unsigned size_y = bits(invocation.invocations, invocation.size_y_shift, invocation.size_z_shift) + 1;
        unsigned size_z = bits(invocation.invocations, invocation.size_z_shift, invocation.workgroups_x_shift) + 1;

        unsigned groups_x = bits(invocation.invocations, invocation.workgroups_x_shift, invocation.workgroups_y_shift) + 1;
        unsigned groups_y = bits(invocation.invocations, invocation.workgroups_y_shift, invocation.workgroups_z_shift) + 1;
        unsigned groups_z = bits(invocation.invocations, invocation.workgroups_z_shift, 32) + 1;

        pandecode_log("Invocation (%d, %d, %d) x (%d, %d, %d)\n",
                      size_x, size_y, size_z,
                      groups_x, groups_y, groups_z);

        DUMP_UNPACKED(INVOCATION, invocation, "Invocation:\n")
}
#endif

#if PAN_ARCH <= 7
static void
pandecode_primitive(const void *p)
{
        pan_unpack(p, PRIMITIVE, primitive);
        DUMP_UNPACKED(PRIMITIVE, primitive, "Primitive:\n");

        /* Validate an index buffer is present if we need one. TODO: verify
         * relationship between invocation_count and index_count */

        if (primitive.indices) {
                /* Grab the size */
                unsigned size = (primitive.index_type == MALI_INDEX_TYPE_UINT32) ?
                        sizeof(uint32_t) : primitive.index_type;

                /* Ensure we got a size, and if so, validate the index buffer
                 * is large enough to hold a full set of indices of the given
                 * size */

                if (!size)
                        pandecode_msg("XXX: index size missing\n");
                else
                        pandecode_validate_buffer(primitive.indices, primitive.index_count * size);
        } else if (primitive.index_type)
                pandecode_msg("XXX: unexpected index size\n");
}

static void
pandecode_uniform_buffers(mali_ptr pubufs, int ubufs_count, int job_no)
{
        struct pandecode_mapped_memory *umem = pandecode_find_mapped_gpu_mem_containing(pubufs);
        uint64_t *PANDECODE_PTR_VAR(ubufs, umem, pubufs);

        for (int i = 0; i < ubufs_count; i++) {
                mali_ptr addr = (ubufs[i] >> 10) << 2;
                unsigned size = addr ? (((ubufs[i] & ((1 << 10) - 1)) + 1) * 16) : 0;

                pandecode_validate_buffer(addr, size);

                char *ptr = pointer_as_memory_reference(addr);
                pandecode_log("ubuf_%d[%u] = %s;\n", i, size, ptr);
                free(ptr);
        }

        pandecode_log("\n");
}

static void
pandecode_uniforms(mali_ptr uniforms, unsigned uniform_count)
{
        pandecode_validate_buffer(uniforms, uniform_count * 16);

        char *ptr = pointer_as_memory_reference(uniforms);
        pandecode_log("vec4 uniforms[%u] = %s;\n", uniform_count, ptr);
        free(ptr);
        pandecode_log("\n");
}
#endif

static const char *
shader_type_for_job(unsigned type)
{
        switch (type) {
#if PAN_ARCH <= 7
        case MALI_JOB_TYPE_VERTEX:  return "VERTEX";
#endif
        case MALI_JOB_TYPE_TILER:   return "FRAGMENT";
        case MALI_JOB_TYPE_FRAGMENT: return "FRAGMENT";
        case MALI_JOB_TYPE_COMPUTE: return "COMPUTE";
        default: return "UNKNOWN";
        }
}

static unsigned shader_id = ~0;

static struct midgard_disasm_stats
pandecode_shader_disassemble(mali_ptr shader_ptr, int shader_no, int type,
                             unsigned gpu_id)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(shader_ptr);
        uint8_t *PANDECODE_PTR_VAR(code, mem, shader_ptr);

        SET_LABEL(mem, "Shader");

        /* Compute maximum possible size */
        size_t sz = mem->length - (shader_ptr - mem->gpu_va);

        /* Print some boilerplate to clearly denote the assembly (which doesn't
         * obey indentation rules), and actually do the disassembly! */

        pandecode_log_cont("\n\n");

#if 0
	/* Shader replacement */
        const char *replace = getenv("PANWRAP_SHADER_REPLACE");

	if (replace) {
		FILE *fp = fopen(replace, "rb");
		fseek(fp, 0L, SEEK_END);
		unsigned size = ftell(fp);
		rewind(fp);
		fread(code, 1, size, fp);
		fclose(fp);
		memset(code + (size/sizeof(code[0])), 0, 64);
	}
#endif

        struct midgard_disasm_stats stats = {0};

#if PAN_ARCH >= 9
                fprintf(pandecode_dump_stream, "va  %p %zu\n", code, sz);
		const uint64_t *c = (void *) code;
                fprintf(pandecode_dump_stream, "%"PRIx64"X\n", c[0]);
                disassemble_valhall(pandecode_dump_stream, (const uint64_t *) code, sz, NULL);
#elif PAN_ARCH >= 6 && PAN_ARCH <= 7
        disassemble_bifrost(pandecode_dump_stream, code, sz, true);

        /* TODO: Extend stats to Bifrost */
        stats.texture_count = -128;
        stats.sampler_count = -128;
        stats.attribute_count = -128;
        stats.varying_count = -128;
        stats.uniform_count = -128;
        stats.uniform_buffer_count = -128;
        stats.work_count = -128;

        stats.instruction_count = 0;
        stats.bundle_count = 0;
        stats.quadword_count = 0;
        stats.helper_invocations = false;
#else
	stats = disassemble_midgard(pandecode_dump_stream,
                                    code, sz, gpu_id, true);
#endif

        unsigned nr_threads =
                (stats.work_count <= 4) ? 4 :
                (stats.work_count <= 8) ? 2 :
                1;

        pandecode_log_cont("shader%d - MESA_SHADER_%s shader: "
                "%u inst, %u bundles, %u quadwords, "
                "%u registers, %u threads, 0 loops, 0:0 spills:fills\n\n\n",
                shader_id++,
                shader_type_for_job(type),
                stats.instruction_count, stats.bundle_count, stats.quadword_count,
                stats.work_count, nr_threads);

        return stats;
}

static void
pandecode_texture_payload(mali_ptr payload,
                          enum mali_texture_dimension dim,
                          enum mali_texture_layout layout,
                          bool manual_stride,
                          uint8_t levels,
                          uint16_t nr_samples,
                          uint16_t array_size,
                          struct pandecode_mapped_memory *tmem)
{
        pandecode_log(".payload = {\n");
        pandecode_indent++;

        /* A bunch of bitmap pointers follow.
         * We work out the correct number,
         * based on the mipmap/cubemap
         * properties, but dump extra
         * possibilities to futureproof */

        int bitmap_count = levels;

        /* Miptree for each face */
        if (dim == MALI_TEXTURE_DIMENSION_CUBE)
                bitmap_count *= 6;

        /* Array of layers */
        bitmap_count *= nr_samples;

        /* Array of textures */
        bitmap_count *= array_size;

#if 0
        /* Stride for each element */
        if (manual_stride)
                bitmap_count *= 2;

        mali_ptr *pointers_and_strides = pandecode_fetch_gpu_mem(tmem,
                payload, sizeof(mali_ptr) * bitmap_count);
#endif

        for (int i = 0; i < bitmap_count; ++i) {
		DUMP_ADDR(SURFACE_WITH_STRIDE, payload + pan_size(SURFACE_WITH_STRIDE) * i, "Payload %d:\n", i);
#if 0
                /* How we dump depends if this is a stride or a pointer */

                if (manual_stride && (i & 1)) {
                        /* signed 32-bit snuck in as a 64-bit pointer */
                        uint64_t stride_set = pointers_and_strides[i];
                        int32_t line_stride = stride_set;
                        int32_t surface_stride = stride_set >> 32;
                        pandecode_log("(mali_ptr) %d /* surface stride */ %d /* line stride */, \n",
                                      surface_stride, line_stride);
                } else {
                        char *a = pointer_as_memory_reference(pointers_and_strides[i]);
                        pandecode_log("%s, \n", a);
                        free(a);
                }
#endif
        }

        pandecode_indent--;
        pandecode_log("},\n");
}

#if PAN_ARCH <= 5
static void
pandecode_texture(mali_ptr u,
                struct pandecode_mapped_memory *tmem,
                unsigned job_no, unsigned tex)
{
        struct pandecode_mapped_memory *mapped_mem = pandecode_find_mapped_gpu_mem_containing(u);
        const uint8_t *cl = pandecode_fetch_gpu_mem(mapped_mem, u, pan_size(TEXTURE));

        pan_unpack(cl, TEXTURE, temp);
        DUMP_UNPACKED(TEXTURE, temp, "Texture:\n")

        for (int i = 0; i < 4; ++i)
                DUMP_ADDR(SURFACE_WITH_STRIDE, u + pan_size(TEXTURE) + pan_size(SURFACE_WITH_STRIDE) * i, "Payload %d:\n", i);
}
#else
static void
pandecode_bifrost_texture(
                const void *cl,
                unsigned job_no,
                unsigned tex)
{
        pan_unpack(cl, TEXTURE, temp);
        DUMP_UNPACKED(TEXTURE, temp, "Texture:\n")

	struct pandecode_mapped_memory *tmem = pandecode_find_mapped_gpu_mem_containing(temp.surfaces);
        unsigned nr_samples = temp.dimension == MALI_TEXTURE_DIMENSION_3D ?
                              1 : temp.sample_count;
        pandecode_indent++;
        pandecode_texture_payload(temp.surfaces, temp.dimension, temp.texel_ordering,
                                  true, temp.levels, nr_samples, temp.array_size, tmem);
        pandecode_indent--;

        SET_LABEL(tmem, "Texture");
}
#endif

#if PAN_ARCH <= 7
static void
pandecode_blend_shader_disassemble(mali_ptr shader, int job_no, int job_type,
                                   unsigned gpu_id)
{
        struct midgard_disasm_stats stats =
                pandecode_shader_disassemble(shader, job_no, job_type, gpu_id);

        bool has_texture = (stats.texture_count > 0);
        bool has_sampler = (stats.sampler_count > 0);
        bool has_attribute = (stats.attribute_count > 0);
        bool has_varying = (stats.varying_count > 0);
        bool has_uniform = (stats.uniform_count > 0);
        bool has_ubo = (stats.uniform_buffer_count > 0);

        if (has_texture || has_sampler)
                pandecode_msg("XXX: blend shader accessing textures\n");

        if (has_attribute || has_varying)
                pandecode_msg("XXX: blend shader accessing interstage\n");

        if (has_uniform || has_ubo)
                pandecode_msg("XXX: blend shader accessing uniforms\n");
}

static void
pandecode_textures(mali_ptr textures, unsigned texture_count, int job_no)
{
        struct pandecode_mapped_memory *mmem = pandecode_find_mapped_gpu_mem_containing(textures);

        if (!mmem)
                return;

        pandecode_log("Textures %"PRIx64"_%d:\n", textures, job_no);
        pandecode_indent++;

#if PAN_ARCH >= 6
        const void *cl =
                pandecode_fetch_gpu_mem(mmem,
                                        textures,
                                        pan_size(TEXTURE) *
                                        texture_count);

        for (unsigned tex = 0; tex < texture_count; ++tex) {
                pandecode_bifrost_texture(cl + pan_size(TEXTURE) * tex,
                                          job_no, tex);
        }
#else
        mali_ptr *PANDECODE_PTR_VAR(u, mmem, textures);

        for (int tex = 0; tex < texture_count; ++tex) {
                mali_ptr *PANDECODE_PTR_VAR(u, mmem, textures + tex * sizeof(mali_ptr));
                char *a = pointer_as_memory_reference(*u);
                pandecode_log("%s,\n", a);
                free(a);
        }

        /* Now, finally, descend down into the texture descriptor */
        for (unsigned tex = 0; tex < texture_count; ++tex) {
                mali_ptr *PANDECODE_PTR_VAR(u, mmem, textures + tex * sizeof(mali_ptr));
                struct pandecode_mapped_memory *tmem = pandecode_find_mapped_gpu_mem_containing(*u);
                if (tmem)
                        pandecode_texture(*u, tmem, job_no, tex);
        }
#endif
        pandecode_indent--;
        pandecode_log("\n");
}

static void
pandecode_samplers(mali_ptr samplers, unsigned sampler_count, int job_no)
{
        pandecode_log("Samplers %"PRIx64"_%d:\n", samplers, job_no);
        pandecode_indent++;

        for (int i = 0; i < sampler_count; ++i)
                DUMP_ADDR(SAMPLER, samplers + (pan_size(SAMPLER) * i), "Sampler %d:\n", i);

        pandecode_indent--;
        pandecode_log("\n");
}

static void
pandecode_dcd(const struct MALI_DRAW *p,
              int job_no, enum mali_job_type job_type,
              char *suffix, unsigned gpu_id)
{
        struct pandecode_mapped_memory *attr_mem;

#if PAN_ARCH >= 5
        struct pandecode_fbd fbd_info = {
                /* Default for Bifrost */
                .rt_count = 1
        };
#endif

#if PAN_ARCH >= 6
        pandecode_local_storage(p->thread_storage & ~1, job_no);
#elif PAN_ARCH == 5
        if (job_type != MALI_JOB_TYPE_TILER) {
                pandecode_local_storage(p->thread_storage & ~1, job_no);
	} else {
                assert(p->fbd & MALI_FBD_TAG_IS_MFBD);
                fbd_info = pandecode_mfbd_bfr((u64) ((uintptr_t) p->fbd) & ~MALI_FBD_TAG_MASK,
                                              job_no, false, gpu_id);
        }
#else
        pandecode_sfbd((u64) (uintptr_t) p->fbd, job_no, false, gpu_id);
#endif

        int varying_count = 0, attribute_count = 0, uniform_count = 0, uniform_buffer_count = 0;
        int texture_count = 0, sampler_count = 0;

        if (p->state) {
                struct pandecode_mapped_memory *smem = pandecode_find_mapped_gpu_mem_containing(p->state);
                uint32_t *cl = pandecode_fetch_gpu_mem(smem, p->state, pan_size(RENDERER_STATE));

                pan_unpack(cl, RENDERER_STATE, state);

                if (state.shader.shader & ~0xF)
                        pandecode_shader_disassemble(state.shader.shader & ~0xF, job_no, job_type, gpu_id);

#if PAN_ARCH >= 6
                bool idvs = (job_type == MALI_JOB_TYPE_INDEXED_VERTEX);

                if (idvs && state.secondary_shader)
                        pandecode_shader_disassemble(state.secondary_shader, job_no, job_type, gpu_id);
#endif
                DUMP_UNPACKED(RENDERER_STATE, state, "State:\n");
                pandecode_indent++;

                /* Save for dumps */
                attribute_count = state.shader.attribute_count;
                varying_count = state.shader.varying_count;
                texture_count = state.shader.texture_count;
                sampler_count = state.shader.sampler_count;
                uniform_buffer_count = state.properties.uniform_buffer_count;

#if PAN_ARCH >= 6
                uniform_count = state.preload.uniform_count;
#else
                uniform_count = state.properties.uniform_count;
#endif

#if PAN_ARCH >= 6
                DUMP_UNPACKED(PRELOAD, state.preload, "Preload:\n");
#elif PAN_ARCH == 4
                mali_ptr shader = state.blend_shader & ~0xF;
                if (state.multisample_misc.blend_shader && shader)
                        pandecode_blend_shader_disassemble(shader, job_no, job_type, gpu_id);
#endif
                pandecode_indent--;
                pandecode_log("\n");

                /* MRT blend fields are used whenever MFBD is used, with
                 * per-RT descriptors */

#if PAN_ARCH >= 5
                if ((job_type == MALI_JOB_TYPE_TILER || job_type == MALI_JOB_TYPE_FRAGMENT) &&
                    (PAN_ARCH >= 6 || p->thread_storage & MALI_FBD_TAG_IS_MFBD)) {
                        void* blend_base = ((void *) cl) + pan_size(RENDERER_STATE);

                        for (unsigned i = 0; i < fbd_info.rt_count; i++) {
                                mali_ptr shader = 0;

#if PAN_ARCH >= 6
                                shader = pandecode_bifrost_blend(blend_base, job_no, i,
                                                                 state.shader.shader);
#else
                                shader = pandecode_midgard_blend_mrt(blend_base, job_no, i);
#endif
                                if (shader & ~0xF)
                                        pandecode_blend_shader_disassemble(shader, job_no, job_type,
                                                                           gpu_id);
                        }
                }
#endif
        } else
                pandecode_msg("XXX: missing shader descriptor\n");

        if (p->viewport) {
                DUMP_ADDR(VIEWPORT, p->viewport, "Viewport:\n");
                pandecode_log("\n");
        }

        unsigned max_attr_index = 0;

        if (p->attributes)
                max_attr_index = pandecode_attribute_meta(attribute_count, p->attributes, false);

        if (p->attribute_buffers) {
                attr_mem = pandecode_find_mapped_gpu_mem_containing(p->attribute_buffers);
                pandecode_attributes(attr_mem, p->attribute_buffers, job_no, suffix, max_attr_index, false, job_type);
        }

        if (p->varyings) {
                varying_count = pandecode_attribute_meta(varying_count, p->varyings, true);
        }

        if (p->varying_buffers) {
                attr_mem = pandecode_find_mapped_gpu_mem_containing(p->varying_buffers);
                pandecode_attributes(attr_mem, p->varying_buffers, job_no, suffix, varying_count, true, job_type);
        }

        if (p->uniform_buffers) {
                if (uniform_buffer_count)
                        pandecode_uniform_buffers(p->uniform_buffers, uniform_buffer_count, job_no);
                else
                        pandecode_msg("warn: UBOs specified but not referenced\n");
        } else if (uniform_buffer_count)
                pandecode_msg("XXX: UBOs referenced but not specified\n");

        /* We don't want to actually dump uniforms, but we do need to validate
         * that the counts we were given are sane */

        if (p->push_uniforms) {
                if (uniform_count)
                        pandecode_uniforms(p->push_uniforms, uniform_count);
                else
                        pandecode_msg("warn: Uniforms specified but not referenced\n");
        } else if (uniform_count)
                pandecode_msg("XXX: Uniforms referenced but not specified\n");

        if (p->textures)
                pandecode_textures(p->textures, texture_count, job_no);

        if (p->samplers)
                pandecode_samplers(p->samplers, sampler_count, job_no);
}

static void
pandecode_primitive_size(const void *s, bool constant)
{
        pan_unpack(s, PRIMITIVE_SIZE, ps);
        if (ps.size_array == 0x0)
                return;

        DUMP_UNPACKED(PRIMITIVE_SIZE, ps, "Primitive Size:\n")
}

static void
pandecode_vertex_compute_geometry_job(const struct MALI_JOB_HEADER *h,
                                      const struct pandecode_mapped_memory *mem,
                                      mali_ptr job, int job_no, unsigned gpu_id)
{
        struct mali_compute_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, COMPUTE_JOB, DRAW, draw);
        pandecode_dcd(&draw, job_no, h->type, "", gpu_id, si);

        pandecode_log("Vertex Job Payload:\n");
        pandecode_indent++;
        pandecode_invocation(pan_section_ptr(p, COMPUTE_JOB, INVOCATION));
        DUMP_SECTION(COMPUTE_JOB, PARAMETERS, p, "Vertex Job Parameters:\n");
        DUMP_UNPACKED(DRAW, draw, "Draw:\n");
        pandecode_indent--;
        pandecode_log("\n");
}
#endif

#if PAN_ARCH >= 6
static void
pandecode_bifrost_tiler_heap(mali_ptr gpu_va, int job_no)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
        pan_unpack(PANDECODE_PTR(mem, gpu_va, void), TILER_HEAP, h);
        DUMP_UNPACKED(TILER_HEAP, h, "Bifrost Tiler Heap:\n");
}

UNUSED static void
pandecode_bifrost_tiler(mali_ptr gpu_va, int job_no)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(gpu_va);
        pan_unpack(PANDECODE_PTR(mem, gpu_va, void), TILER_CONTEXT, t);

        if (t.heap)
                pandecode_bifrost_tiler_heap(t.heap, job_no);

        DUMP_UNPACKED(TILER_CONTEXT, t, "Bifrost Tiler:\n");
        pandecode_indent++;
        if (t.hierarchy_mask != 0xa &&
            t.hierarchy_mask != 0x14 &&
            t.hierarchy_mask != 0x28 &&
            t.hierarchy_mask != 0x50 &&
            t.hierarchy_mask != 0xa0)
                pandecode_msg("XXX: Unexpected hierarchy_mask (not 0xa, 0x14, 0x28, 0x50 or 0xa0)!");

        pandecode_indent--;
}

#if PAN_ARCH <= 7
static void
pandecode_indexed_vertex_job(const struct MALI_JOB_HEADER *h,
                             const struct pandecode_mapped_memory *mem,
                             mali_ptr job, int job_no, unsigned gpu_id)
{
        struct mali_indexed_vertex_job_packed *PANDECODE_PTR_VAR(p, mem, job);

        pandecode_log("Vertex:\n");
        pan_section_unpack(p, INDEXED_VERTEX_JOB, VERTEX_DRAW, vert_draw);
        pandecode_dcd(&vert_draw, job_no, h->type, "", gpu_id);
        DUMP_UNPACKED(DRAW, vert_draw, "Vertex Draw:\n");

        pandecode_log("Fragment:\n");
        pan_section_unpack(p, INDEXED_VERTEX_JOB, FRAGMENT_DRAW, frag_draw);
        pandecode_dcd(&frag_draw, job_no, MALI_JOB_TYPE_FRAGMENT, "", gpu_id);
        DUMP_UNPACKED(DRAW, frag_draw, "Fragment Draw:\n");

        pan_section_unpack(p, INDEXED_VERTEX_JOB, TILER, tiler_ptr);
        pandecode_log("Tiler Job Payload:\n");
        pandecode_indent++;
        pandecode_bifrost_tiler(tiler_ptr.address, job_no);
        pandecode_indent--;

        pandecode_invocation(pan_section_ptr(p, INDEXED_VERTEX_JOB, INVOCATION));
        pandecode_primitive(pan_section_ptr(p, INDEXED_VERTEX_JOB, PRIMITIVE));

        /* TODO: gl_PointSize on Bifrost */
        pandecode_primitive_size(pan_section_ptr(p, INDEXED_VERTEX_JOB, PRIMITIVE_SIZE), true);

        pan_section_unpack(p, INDEXED_VERTEX_JOB, PADDING, padding);
}

static void
pandecode_tiler_job_bfr(const struct MALI_JOB_HEADER *h,
                        const struct pandecode_mapped_memory *mem,
                        mali_ptr job, int job_no, unsigned gpu_id)
{
        struct mali_tiler_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, TILER_JOB, DRAW, draw);
        pan_section_unpack(p, TILER_JOB, TILER, tiler_ptr);
        pandecode_dcd(&draw, job_no, h->type, "", gpu_id);

        pandecode_log("Tiler Job Payload:\n");
        pandecode_indent++;
        pandecode_bifrost_tiler(tiler_ptr.address, job_no);

        pandecode_invocation(pan_section_ptr(p, TILER_JOB, INVOCATION));
        pandecode_primitive(pan_section_ptr(p, TILER_JOB, PRIMITIVE));

        /* TODO: gl_PointSize on Bifrost */
        pandecode_primitive_size(pan_section_ptr(p, TILER_JOB, PRIMITIVE_SIZE), true);
        pan_section_unpack(p, TILER_JOB, PADDING, padding);
        DUMP_UNPACKED(DRAW, draw, "Draw:\n");
        pandecode_indent--;
        pandecode_log("\n");
}
#endif
#else
static void
pandecode_tiler_job_mdg(const struct MALI_JOB_HEADER *h,
                        const struct pandecode_mapped_memory *mem,
                        mali_ptr job, int job_no, unsigned gpu_id)
{
        struct mali_tiler_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, TILER_JOB, DRAW, draw);
        pandecode_dcd(&draw, job_no, h->type, "", gpu_id);

        pandecode_log("Tiler Job Payload:\n");
        pandecode_indent++;
        pandecode_invocation(pan_section_ptr(p, TILER_JOB, INVOCATION));
        pandecode_primitive(pan_section_ptr(p, TILER_JOB, PRIMITIVE));
        DUMP_UNPACKED(DRAW, draw, "Draw:\n");

        pan_section_unpack(p, TILER_JOB, PRIMITIVE, primitive);
        pandecode_primitive_size(pan_section_ptr(p, TILER_JOB, PRIMITIVE_SIZE),
                                 primitive.point_size_array_format == MALI_POINT_SIZE_ARRAY_FORMAT_NONE);
        pandecode_indent--;
        pandecode_log("\n");
}
#endif

#if PAN_ARCH <= 9
static void
pandecode_fragment_job(const struct pandecode_mapped_memory *mem,
                       mali_ptr job, int job_no, unsigned gpu_id,
                       struct job_shader_info *si)
{
        struct mali_fragment_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, FRAGMENT_JOB, PAYLOAD, s);


#if PAN_ARCH == 4
        pandecode_sfbd(s.framebuffer, job_no, true, gpu_id);
#else
        assert(s.framebuffer & MALI_FBD_TAG_IS_MFBD);

        struct pandecode_fbd info;

        info = pandecode_mfbd_bfr(s.framebuffer & ~MALI_FBD_TAG_MASK, job_no,
                                  true, gpu_id);
#endif

#if PAN_ARCH >= 5
        unsigned expected_tag = 0;

        /* Compute the tag for the tagged pointer. This contains the type of
         * FBD (MFBD/SFBD), and in the case of an MFBD, information about which
         * additional structures follow the MFBD header (an extra payload or
         * not, as well as a count of render targets) */

        expected_tag = MALI_FBD_TAG_IS_MFBD;
        if (info.has_extra)
                expected_tag |= MALI_FBD_TAG_HAS_ZS_RT;

        expected_tag |= MALI_FBD_TAG_IS_MFBD | (MALI_POSITIVE(info.rt_count) << 2);
#endif

        DUMP_UNPACKED(FRAGMENT_JOB_PAYLOAD, s, "Fragment Job Payload:\n");

#if PAN_ARCH >= 5
        /* The FBD is a tagged pointer */

        unsigned tag = (s.framebuffer & MALI_FBD_TAG_MASK);

        if (tag != expected_tag)
                pandecode_msg("XXX: expected FBD tag %X but got %X\n", expected_tag, tag);
#endif

        pandecode_log("\n");
}

static void
pandecode_write_value_job(const struct pandecode_mapped_memory *mem,
                          mali_ptr job, int job_no)
{
        struct mali_write_value_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, WRITE_VALUE_JOB, PAYLOAD, u);
        DUMP_SECTION(WRITE_VALUE_JOB, PAYLOAD, p, "Write Value Payload:\n");
        pandecode_log("\n");
}

static void
pandecode_cache_flush_job(const struct pandecode_mapped_memory *mem,
                          mali_ptr job, int job_no)
{
        struct mali_cache_flush_job_packed *PANDECODE_PTR_VAR(p, mem, job);
        pan_section_unpack(p, CACHE_FLUSH_JOB, PAYLOAD, u);
        DUMP_SECTION(CACHE_FLUSH_JOB, PAYLOAD, p, "Cache Flush Payload:\n");
        pandecode_log("\n");
}
#endif

#if PAN_ARCH >= 10
static void
dump_state(uint32_t *state)
{
        uint64_t *st_64 = (uint64_t *)state;
        for (unsigned i = 0; i < 16; ++i) {
                uint64_t v1 = st_64[i * 2];
                uint64_t v2 = st_64[i * 2 + 1];

                if (!v1 && !v2)
                        continue;

                fprintf(pandecode_dump_stream,
                        "0x%2x: 0x%16"PRIx64" 0x%16"PRIx64"\n",
                        i * 4, v1, v2);
        }
}
#endif

#if PAN_ARCH >= 9
static void
dump_buffer(mali_ptr addr, size_t sz)
{
        struct pandecode_mapped_memory *mem =
                pandecode_find_mapped_gpu_mem_containing(addr);
        const uint8_t *PANDECODE_PTR_VAR(raw, mem, addr);
        fprintf(pandecode_dump_stream, "Addr %" PRIx64 "\n", addr);
        hexdump(pandecode_dump_stream, raw, sz, NULL, 0, 0);
}

static void
dump_named_buffer(mali_ptr addr, size_t sz, const char *name)
{
        fprintf(pandecode_dump_stream, "%s:\n", name);
        dump_buffer(addr, sz);
        fprintf(pandecode_dump_stream, "\n");
}

UNUSED static void
dump_fau(mali_ptr addr, unsigned count, const char *name)
{
        struct pandecode_mapped_memory *mem =
                pandecode_find_mapped_gpu_mem_containing(addr);
        const uint32_t *PANDECODE_PTR_VAR(raw, mem, addr);

	pandecode_validate_buffer(addr, count * 8);

        fprintf(pandecode_dump_stream, "%s:\n", name);
        for (unsigned i = 0; i < count; ++i) {
                fprintf(pandecode_dump_stream, "  %08X %08X\n",
                                raw[2*i], raw[2*i + 1]);
        }
        fprintf(pandecode_dump_stream, "\n");
}

static mali_ptr
pandecode_shader(mali_ptr addr, const char *label, unsigned gpu_id)
{
        MAP_ADDR(SHADER_PROGRAM, addr, cl);
        pan_unpack(cl, SHADER_PROGRAM, desc);

        SET_LABEL_VA(addr, "Shader descriptors");

        if (desc.type == 0) {
            fprintf(pandecode_dump_stream, "(no shader)\n");
            return 0;
        }
        assert(desc.type == 8);

        DUMP_UNPACKED(SHADER_PROGRAM, desc, "%s Shader:\n", label);
        pandecode_shader_disassemble(desc.binary, 0, 0, gpu_id);
        return desc.binary;
}

static void
pandecode_resources(mali_ptr addr, unsigned size, bool idvs)
{
        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(addr);
        if (!mem || size + (addr - mem->gpu_va) > mem->length)
                return;
        const uint8_t *cl = pandecode_fetch_gpu_mem(mem, addr, size);
        assert((size % 0x20) == 0);

        for (unsigned i = 0; i < size; i += 0x20) {
                unsigned type = (cl[i] & 0xF);

                switch (type) {
                case MALI_DESCRIPTOR_TYPE_SAMPLER:
                        DUMP_CL(SAMPLER, cl + i, "Sampler:\n");
                        break;
                case MALI_DESCRIPTOR_TYPE_TEXTURE:
                        pandecode_bifrost_texture(cl + i, 0, i);
                        break;
                case MALI_DESCRIPTOR_TYPE_ATTRIBUTE:
                        DUMP_CL(ATTRIBUTE, cl + i, "Attribute:\n");
                        break;
                case MALI_DESCRIPTOR_TYPE_BUFFER:
                        DUMP_CL(BUFFER, cl + i, "Buffer:\n");
                        break;
                default:
                        fprintf(pandecode_dump_stream, "Unknown descriptor type %X\n", type);
                        hexdump(pandecode_dump_stream, cl + i, 0x20, NULL, 0, 0);
                        break;
                }
        }
}

static void
pandecode_resource_tables(mali_ptr addr, const char *label, unsigned count,
                          bool idvs)
{
        fprintf(pandecode_dump_stream, "Tag %x\n", (int) (addr & 0xF));
        addr = addr & ~0xF;
        dump_named_buffer(addr, 128, label);

        struct pandecode_mapped_memory *mem = pandecode_find_mapped_gpu_mem_containing(addr);
        if (!count)
                count = 8; // TODO: what is the actual count? at least 9.
        const uint8_t *cl = pandecode_fetch_gpu_mem(mem, addr, MALI_RESOURCE_LENGTH * count);

        for (unsigned i = 0; i < count; ++i) {
                pan_unpack(cl + i * MALI_RESOURCE_LENGTH, RESOURCE, entry);
                DUMP_UNPACKED(RESOURCE, entry, "Entry %u:\n", i);
                pandecode_resources(entry.address, entry.size, idvs);
        }
}

static void
pandecode_depth_stencil(mali_ptr addr)
{
	MAP_ADDR(DEPTH_STENCIL, addr, cl);
	pan_unpack(cl, DEPTH_STENCIL, desc);
	DUMP_UNPACKED(DEPTH_STENCIL, desc, "Depth/stencil");
}

static void
pandecode_dcd(const struct MALI_DRAW *p,
              int job_no, enum mali_job_type job_type,
              char *suffix, unsigned gpu_id)
{
#if PAN_ARCH >= 10
	mali_ptr frag_shader = pandecode_shader(p->fragment, "Fragment", gpu_id);

        if (p->varying)
                pandecode_shader(p->varying, "Varying", gpu_id);

        if (p->position)
                pandecode_shader(p->position, "Position", gpu_id);
#else
	mali_ptr frag_shader = pandecode_shader(p->fragment.shader, "Fragment", gpu_id);

	if (p->varying.shader)
		pandecode_shader(p->varying.shader, "Varying", gpu_id);

	if (p->position.shader)
		pandecode_shader(p->position.shader, "Position", gpu_id);
#endif

	pandecode_depth_stencil(p->depth_stencil);

	if (p->blend) {
		struct pandecode_mapped_memory *blend_mem =
			pandecode_find_mapped_gpu_mem_containing(p->blend);

		struct mali_blend_packed *PANDECODE_PTR_VAR(blend_descs, blend_mem, p->blend);

		mali_ptr blend_shader = pandecode_bifrost_blend(blend_descs, 0, 0, frag_shader);
		if (blend_shader) {
			fprintf(pandecode_dump_stream, "Blend shader");
			pandecode_shader_disassemble(blend_shader, 0, 0, gpu_id);
		}
	}

#if PAN_ARCH == 9
	if (p->fragment.resources)
		pandecode_resource_tables(p->fragment.resources, "Fragment resources", 0, true);
	if (p->fragment.thread_storage)
		pandecode_local_storage(p->fragment.thread_storage, 0);
	if (p->fragment.fau)
		dump_fau(p->fragment.fau, p->fragment.fau_count, "Fragment FAU");

	if (p->position.resources)
		pandecode_resource_tables(p->position.resources, "Position resources", 0, true);
	if (p->position.thread_storage)
		pandecode_local_storage(p->position.thread_storage, 0);
	if (p->position.fau)
		dump_fau(p->position.fau, p->position.fau_count, "Position FAU");

	if (p->varying.resources)
		pandecode_resource_tables(p->varying.resources, "Varying resources", 0, true);
	if (p->varying.thread_storage)
		pandecode_local_storage(p->varying.thread_storage, 0);
	if (p->varying.fau)
		dump_fau(p->varying.fau, p->varying.fau_count, "Varying FAU");
#endif
}

/*
static void
pandecode_idvs_helper_job(const struct pandecode_mapped_memory *mem,
                          mali_ptr job, unsigned gpu_id)
{
	mali_ptr frag_shader = pandecode_shader(p->fragment.shader, "Fragment", gpu_id);

	if (payload.tiler)
		pandecode_bifrost_tiler(payload.tiler, 0);


	pandecode_dcd(&payload.draw, 0, 0, NULL, gpu_id);
	DUMP_UNPACKED(IDVS_HELPER_PAYLOAD, payload, "IDVS Helper:\n");
}
*/

#if PAN_ARCH <= 9
static void
pandecode_idvs_helper_job(const struct pandecode_mapped_memory *mem,
                          mali_ptr job, unsigned gpu_id,
                          struct job_shader_info *si)
{
	struct mali_idvs_helper_job_packed *PANDECODE_PTR_VAR(p, mem, job);
	pan_section_unpack(p, IDVS_HELPER_JOB, PRIMITIVE, primitive);
	pan_section_unpack(p, IDVS_HELPER_JOB, COUNTS, counts);
	pan_section_unpack(p, IDVS_HELPER_JOB, TILER, tiler);
	pan_section_unpack(p, IDVS_HELPER_JOB, SCISSOR, scissor);
	pan_section_unpack(p, IDVS_HELPER_JOB, PRIMITIVE_SIZE, prim_size);
	pan_section_unpack(p, IDVS_HELPER_JOB, INDICES, indices);
	pan_section_unpack(p, IDVS_HELPER_JOB, DRAW, draw);

	if (tiler.address)
		pandecode_bifrost_tiler(tiler.address, 0);

	DUMP_UNPACKED(PRIMITIVE, primitive, "IDVS Helper primitive:\n");
	DUMP_UNPACKED(IDVS_COUNTS, counts, "IDVS Helper counts:\n");
	DUMP_UNPACKED(SCISSOR, scissor, "IDVS Helper scissor:\n");
	DUMP_UNPACKED(PRIMITIVE_SIZE, prim_size, "IDVS Helper prim_size:\n");
	DUMP_UNPACKED(INDICES, indices, "IDVS Helper indices:\n");

	pandecode_dcd(&draw, 0, 0, NULL, gpu_id);
}
#endif

#if PAN_ARCH <= 9
static void
pandecode_compute_job(const struct pandecode_mapped_memory *mem, mali_ptr job, unsigned gpu_id, struct job_shader_info *si)
{
	struct mali_compute_job_packed *PANDECODE_PTR_VAR(p, mem, job);
	pan_section_unpack(p, COMPUTE_JOB, PAYLOAD, payload);

        mali_ptr bin = pandecode_shader(payload.compute.shader, "Shader", gpu_id);

        char buf[256];
        snprintf(buf, 256, "shader%u", shader_id);
        si_set_shader(si, strdup(buf));
        si_set_fau(si, payload.compute.fau);
        si_set_resource(si, payload.compute.resources & ~0xF);
        si_set_branch(si, bin);

	if (payload.compute.thread_storage)
		pandecode_local_storage(payload.compute.thread_storage, 0);
	if (payload.compute.fau)
		dump_fau(payload.compute.fau, payload.compute.fau_count, "FAU");
	if (payload.compute.resources)
		pandecode_resource_tables(payload.compute.resources, "Resources", 0, false);

	DUMP_UNPACKED(COMPUTE_PAYLOAD, payload, "Compute:\n");
}
#endif
#endif

/* Entrypoint to start tracing. jc_gpu_va is the GPU address for the first job
 * in the chain; later jobs are found by walking the chain. Bifrost is, well,
 * if it's bifrost or not. GPU ID is the more finegrained ID (at some point, we
 * might wish to combine this with the bifrost parameter) because some details
 * are model-specific even within a particular architecture. */

#if PAN_ARCH <= 9
void
GENX(pandecode_jc)(mali_ptr jc_gpu_va, unsigned gpu_id, struct job_shader_info *si)
{
        pandecode_dump_file_open();

        unsigned job_descriptor_number = 0;
        mali_ptr next_job = 0;

        do {
                struct pandecode_mapped_memory *mem =
                        pandecode_find_mapped_gpu_mem_containing(jc_gpu_va);

                pan_unpack(PANDECODE_PTR(mem, jc_gpu_va, struct mali_job_header_packed),
                           JOB_HEADER, h);
                next_job = h.next;

                int job_no = job_descriptor_number++;

                DUMP_UNPACKED(JOB_HEADER, h, "Job Header:\n");
                pandecode_log("\n");

                switch (h.type) {
                case MALI_JOB_TYPE_WRITE_VALUE:
                        pandecode_write_value_job(mem, jc_gpu_va, job_no);
                        break;

                case MALI_JOB_TYPE_CACHE_FLUSH:
                        pandecode_cache_flush_job(mem, jc_gpu_va, job_no);
                        break;

#if PAN_ARCH <= 7
                case MALI_JOB_TYPE_TILER:
#if PAN_ARCH >= 6
                        pandecode_tiler_job_bfr(&h, mem, jc_gpu_va, job_no, gpu_id);
#else
                        pandecode_tiler_job_mdg(&h, mem, jc_gpu_va, job_no, gpu_id);
#endif
                        break;

                case MALI_JOB_TYPE_VERTEX:
                case MALI_JOB_TYPE_COMPUTE:
                        pandecode_vertex_compute_geometry_job(&h, mem, jc_gpu_va, job_no, gpu_id);
                        break;

#if PAN_ARCH >= 6
                case MALI_JOB_TYPE_INDEXED_VERTEX:
                        pandecode_indexed_vertex_job(&h, mem, jc_gpu_va, job_no, gpu_id, si);
                        break;
#endif
#else
		case MALI_JOB_TYPE_COMPUTE:
			if (/*jc_gpu_va == 0x5effe36c00ULL*/0) {
				pan_pack(PANDECODE_PTR(mem, jc_gpu_va, struct mali_job_header_packed),
                           JOB_HEADER, cfg) {
					cfg.next = h.next;
					cfg.type = MALI_JOB_TYPE_NULL;
				}

				break;
			}
			dump_named_buffer(jc_gpu_va + sizeof(struct mali_job_header_packed), 384, "Compute payload");
			pandecode_compute_job(mem, jc_gpu_va, gpu_id, si);
			break;

		case MALI_JOB_TYPE_IDVS_HELPER:
			dump_named_buffer(jc_gpu_va + sizeof(struct mali_job_header_packed), 384, "IDVS payload");
			pandecode_idvs_helper_job(mem, jc_gpu_va, gpu_id, si);
			break;
#endif

                case MALI_JOB_TYPE_FRAGMENT:
			if (h.index == 1) {
				pan_pack(PANDECODE_PTR(mem, jc_gpu_va, struct mali_job_header_packed),
                           JOB_HEADER, cfg) {
					cfg.next = h.next;
					cfg.type = MALI_JOB_TYPE_NULL;
				}

				break;
			}

                        pandecode_fragment_job(mem, jc_gpu_va, job_no, gpu_id, si);
                        break;

                default:
                        break;
                }
        } while ((jc_gpu_va = next_job));

        fflush(pandecode_dump_stream);
        pandecode_map_read_write();
}

void
GENX(pandecode_abort_on_fault)(mali_ptr jc_gpu_va)
{
        mali_ptr next_job = 0;

        do {
                struct pandecode_mapped_memory *mem =
                        pandecode_find_mapped_gpu_mem_containing(jc_gpu_va);

                pan_unpack(PANDECODE_PTR(mem, jc_gpu_va, struct mali_job_header_packed),
                           JOB_HEADER, h);
                next_job = h.next;

                /* Ensure the job is marked COMPLETE */
                if (h.exception_status != 0x1) {
                        fprintf(stderr, "Incomplete job or timeout");
                        abort();
                }
        } while ((jc_gpu_va = next_job));

        pandecode_map_read_write();
}
#endif

#if PAN_ARCH >= 10

static void
pandecode_csf_compute(uint32_t *state, unsigned gpu_id, struct job_shader_info *si)
{
        dump_state(state);
        pan_unpack(state, COMPUTE_JOB, c);
        DUMP_UNPACKED(COMPUTE_JOB, c, "CSF Compute Job:\n");

        mali_ptr bin = pandecode_shader(c.shader, "Shader", gpu_id);

        char buf[256];
        snprintf(buf, 256, "shader%u", shader_id);
        si_set_shader(si, strdup(buf));
        si_set_resource(si, c.resources & ~0xF);
        si_set_branch(si, bin);

        if (c.thread_storage)
            pandecode_local_storage(c.thread_storage, 0);
        pandecode_resource_tables(c.resources, "Resources", 0, false);
}

static void
pandecode_csf_indexed_vertex(uint32_t *state, unsigned gpu_id, struct job_shader_info *si)
{
        dump_state(state);

        pan_unpack(state, PRIMITIVE_SIZE, r);
        pan_unpack(state, PRIMITIVE, p);
        pan_unpack(state, SCISSOR, s);
        pan_unpack(state, DRAW, d);
        pan_unpack(state, IDVS_COUNTS, c);

        DUMP_UNPACKED(PRIMITIVE, p, "CSF Indexed Vertex Job Primitive:\n");
        DUMP_UNPACKED(IDVS_COUNTS, c, "CSF IDVS Counts:\n");
        DUMP_UNPACKED(PRIMITIVE_SIZE, r, "CSF IDVS Primitive Size:\n");
        DUMP_UNPACKED(SCISSOR, s, "CSF Indexed Vertex Job Scissor:\n");

        // pandecode_dcd?
        DUMP_UNPACKED(DRAW, d, "CSF Indexed Vertex Job Draw:\n");

        pandecode_dcd(&d, 0, 0, NULL, gpu_id);

/*
        mali_ptr bin = pandecode_shader(c.shader, "Shader", gpu_id);

        char buf[256];
        snprintf(buf, 256, "shader%u", shader_id);
        si_set_shader(si, strdup(buf));
        si_set_resource(si, c.resources & ~0xF);
        si_set_branch(si, bin);
*/
//        pandecode_local_storage(c.thread_storage, 0);
//        pandecode_resource_tables(c.resources, "Resources", 0, false);
}

static void
pandecode_command_buffer(uint32_t *buffer, mali_ptr va, unsigned length, unsigned gpu_id, struct job_shader_info *si)
{
        uint32_t alt_buffer[256] = {0};

        pandecode_log("Decoding command buffer %"PRIx64"\n", va);
        pandecode_indent++;
        fflush(pandecode_dump_stream);
        SET_LABEL_VA(va, "Command Buffer");

        uint64_t *commands = pandecode_fetch_gpu_mem(NULL, va, 1);

        for (unsigned i = 0; i < length / 8; ++i) {
                uint64_t c = commands[i];
                uint8_t flags = c >> 56;
                uint8_t addr = (c >> 48) & 0xff;

                buffer[addr] = c & 0xffffffff;
                // TODO: be more precise with flags
                if (flags != 2) {
                    buffer[addr + 1] = (c >> 32) & 0xffff;
                } else if ((c << 16) >> 48) {
                    pandecode_log("Extra data in 02 command: 0x%"PRIx64"\n", (c << 16) >> 48);
                }

                if (flags & 4) {
                        pandecode_log("we haz a command %"PRIx64"\n", c);

                        memcpy(alt_buffer, buffer, 256 * 4);

                        if (flags == 0x17) {
                                /* I don't know what this means */
                        } else if (flags == 4) {
                                pandecode_csf_compute(buffer, gpu_id, si);
                                dump_state(buffer);
                        } else if (flags == 6) {
                                pandecode_csf_indexed_vertex(buffer, gpu_id, si);
                                dump_state(buffer);
                        }

                        memcpy(buffer, alt_buffer, 256 * 4);
                } else if (flags == 0x20) {
                        pandecode_command_buffer(buffer, buffer[0x48] + (((uint64_t)buffer[0x49]) << 32), buffer[0x4a], gpu_id, si);
                }
        }

        pandecode_indent--;
}

void
GENX(pandecode_cs)(mali_ptr cs_gpu_va, unsigned gpu_id, struct job_shader_info *si)
{
        pandecode_dump_file_open();

        uint32_t buffer[256] = {0};

        uint64_t *commands = pandecode_fetch_gpu_mem(NULL, cs_gpu_va + 64, 1);

        for (uint64_t c = *commands; c; c = *(++commands)) {
                uint8_t flags = c >> 56;
                uint8_t addr = (c >> 48) & 0xff;

                if (flags == 0x20) { /* Maybe it is actually some other command? */
                        /* TODO: Is this really a separate buffer? */
                        uint32_t inner[256] = {0};
                        pandecode_command_buffer(inner, buffer[0x48] + (((uint64_t)buffer[0x49]) << 32), buffer[0x4a], gpu_id, si);
                } else {
                        buffer[addr] = c & 0xffffffff;
                        buffer[addr + 1] = (c >> 32) & 0xffff;
                }
        }
}

#define cs_log pandecode_dump_stream

#define CS_OFFSET 64

void
GENX(pandecode_cs_only)(mali_ptr cs_gpu_va, unsigned indent)
{
    uint32_t buffer[256] = {0};

    uint64_t *commands = pandecode_fetch_gpu_mem(NULL, cs_gpu_va + CS_OFFSET, 1);

    for (uint64_t c = *commands; c != 0xfafafafafafafafaULL &&
             c != 0xacacacacacacacacULL; c = *(++commands)) {
        for (unsigned i = 0; i < indent; ++i)
            fprintf(cs_log, " ");

        uint8_t command = c >> 56;
        uint8_t addr = (c >> 48) & 0xff;
        uint64_t value = c & 0xffffffffffffULL;

        uint32_t h = value >> 32;
        uint32_t l = value;

        uint8_t arg1 = h & 0xff;
        uint8_t arg2 = h >> 8;

        switch (command) {
        case 0:
            if (addr || value)
                fprintf(cs_log, "nop %02x, #0x%"PRIx64"\n", addr, value);
            else
                fprintf(cs_log, "nop\n");
            break;
        case 1:
            buffer[addr] = l;
            buffer[addr + 1] = h;
            fprintf(cs_log, "mov x%02x, #0x%"PRIx64"\n", addr, value);
            break;
        case 2:
            buffer[addr] = l;
            fprintf(cs_log, "mov w%02x, #0x%"PRIx64"\n", addr, value);
            break;
        case 4: {
            uint32_t masked = l & 0xffff0000;
            unsigned job_div = l & 0x3fff;
            unsigned job_div_scale = (l >> 14) & 3;
            unsigned div = job_div << job_div_scale;
            if (h != 0xff00 || addr || masked)
                fprintf(cs_log, "compute (unk %02x), (unk %04x), "
                        "(unk %x), div %i\n", addr, h, masked, div);
            else
                fprintf(cs_log, "compute div %i\n", div);
            break;
        }
        case 6: {
            uint32_t masked = l & 0xfffff8f0;
            uint8_t mode = l & 0xf;
            uint8_t index = (l >> 8) & 7;
            if (addr || masked)
                fprintf(cs_log, "idvs (unk %02x), w%02x, w%02x, (unk %x), "
                        "mode %i index %i\n",
                        addr, arg1, arg2, masked, mode, index);
            else
                fprintf(cs_log, "idvs w%02x, w%02x, mode %i index %i\n",
                        arg1, arg2, mode, index);
            break;
        }
        case 7: {
            if (addr || value)
                fprintf(cs_log, "fragment (unk %02x), (unk %"PRIx64")\n",
                        addr, value);
            else
                fprintf(cs_log, "fragment\n");
            break;
        }
        case 0x20: {
            const char *name;
            /* This does not appear to be exact... */
            switch (buffer[arg1]) {
            case 0xc8:
                name = "vertex";
                break;
            case 0xf8:
                name = "fragment";
                break;
            case 0x108:
                name = "compute";
                break;
            default:
                name = "unk";
            }

            if (addr || l)
                fprintf(cs_log, "job (unk %02x), w%02x (%s), x%02x, (unk %x)\n",
                        addr, arg1, name, arg2, l);
            else
                fprintf(cs_log, "job w%02x (%s), x%02x\n", arg1, name, arg2);

            uint64_t target = (((uint64_t)buffer[arg2 + 1]) << 32) | buffer[arg2];
            GENX(pandecode_cs_only)(target - CS_OFFSET, indent + 1);
            break;
        }
        case 0x26: {
            fprintf(cs_log, "str (unk %02x), w%02x, [x%02x, unk %x]\n",
                    addr, arg1, arg2, l);
            break;
        }
        default:
            fprintf(cs_log, "UNK %02x %02x, #0x%"PRIx64"\n", addr, command, value);
            break;
        }
    }
    fprintf(cs_log, "\n");
}

#endif

struct job_shader_info *
job_shader_info_create(void) {
        return calloc(sizeof(struct job_shader_info), 1);
}

void
job_shader_info_free(struct job_shader_info *si) {
        free(si);
}
