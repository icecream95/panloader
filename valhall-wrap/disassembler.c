#include <stdio.h>
#include <stdlib.h>
#include "disassemble.h"

int main(int argc, const char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s infile\n", argv[0]);
		return 1;
	}

	FILE *fp = strcmp(argv[1], "-") == 0 ? stdin : fopen(argv[1], "rb");

	if (fp == NULL) {
		fprintf(stderr, "Couldn't open %s\n", argv[1]);
		return 1;
	}

	int count;
	uint64_t code;

	while ((count = fread(&code, 1, 8, fp))) {
		assert(count == 8);
		disassemble_valhall(stdout, &code, 8, NULL);
	}

	fclose(fp);
}
