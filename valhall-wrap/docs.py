#
# Copyright (C) 2020 Collabora, Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import textwrap
import xml.etree.ElementTree as ET
from valhall import MODIFIERS, instruction_dict

tree = ET.parse('ISA.xml')
root = tree.getroot()

SRCMODIFIERS = ["not", "lane", "lanes", "widen", "swizzle"]

def syntax_for_src(i, src, only_one = False):
    base = "src" if only_one else f"src{i}"

    if src.absneg:
        base = base + "{.abs}{.neg}"

    modifiers = {
        "not": src.notted,
        "swz": src.swizzle,
        "widen": src.widen,
        "lanes": src.lanes,
        "lane": src.lane,
    }

    modifier_syntax = ["{." + mod + "}" for mod in modifiers if modifiers[mod]]

    return base + ''.join(modifier_syntax)

def short_mod(mod):
    SHORT = {
        "condition": "cond",
        "lane_operation": "lane_op",
        "inactive_result": "inactive_res",
        "dimension": "dim",
        "lod_mode": "lod",
        "round_mode": "round",
        "swizzle": "swz",
    }

    return SHORT.get(mod.name, mod.name)

def make_syntax_sr(sr):
    return "@" + ("r" if sr.read else "") + ("w" if sr.write else "")

def make_syntax(ins):
    mods = ["{." + short_mod(mod) + "}" for mod in ins.modifiers if not mod.implied]
    dests = ["dest"] if len(ins.dests) == 1 else [f"dest{i}" for dest in ins.dests]
    srcs = [syntax_for_src(i, s, len(ins.srcs) == 1) for i, s in enumerate(ins.srcs)]
    imms = [f"#{imm.name}" for imm in ins.immediates]
    sr = [make_syntax_sr(sr) for sr in ins.staging]

    return "".join(mods) + " " + (", ".join(sr + dests + srcs + imms))

def print_description(el):
    descs = el.findall('desc')
    assert(len(descs) <= 1)
    for desc in descs:
        print(textwrap.dedent(desc.text).strip())
        print("")

class Pattern:
    def __init__(self, name, start, size):
        self.name = name
        self.start = start
        self.size = size

    def __str__(self):
        return f"{self.name} ({self.start}, {self.size})"

def ctz(x):
    return (-x & x).bit_length() - 1

def patterns_for_bitmask(name, start, mask):
    patterns = []

    while mask != 0:
        skip = ctz(mask)
        mask >>= skip
        start += skip

        count = ctz(~mask)
        mask >>= count

        patterns.append(Pattern(name, start, count))
        start += count

    return patterns

def operand_name(i, op, count, cls):
    if len(op.name) > 0:
        return f"{cls}: {op.name}"
    elif count == 1:
        return cls
    else:
        return f"{cls} {i}"

src_name = lambda i, src, count: operand_name(i, src, count, "Source")
dest_name = lambda dest: operand_name(0, dest, 1, "Destination")
sr_name = lambda i, sr, count: operand_name(i, sr, count, "Staging")

def instruction_bits(ins):
    ranges = [Pattern("Opcode", 48, 9), Pattern("Metadata", 57, 7)]
    ranges += patterns_for_bitmask("Secondary opcode", ins.secondary_shift,
                                   ins.secondary_mask)
    ranges += [Pattern(src_name(i, src, len(ins.srcs)), 8 * i, 8) for i, src in enumerate(ins.srcs)]
    ranges += [Pattern(dest_name(dest), 40, 8) for dest in ins.dests]
    ranges += [Pattern(sr_name(i, sr, len(ins.staging)), sr.start, 8 if sr.flags else 2) for i, sr in enumerate(ins.staging)]
    ranges += [Pattern(f"`{mod.name}`", mod.start, mod.size) for mod in ins.modifiers if not mod.implied]
    ranges += [Pattern(f"`{imm.name}`", imm.start, imm.size) for imm in ins.immediates]

    for i, src in enumerate(ins.srcs):
        for key in src.offset:
            ranges.append(Pattern(f"`{key}` {i}", src.offset[key], src.bits[key]))

    if len(ins.dests) == 0 and len(ins.staging) == 0:
        ranges.append(Pattern(f"Placeholder", 40, 8))

    return ranges

def print_range_row(name, start, size):
    if size != 1:
        print(f"{name} | {start}:{start + size - 1}")
    else:
        print(f"{name} | {start}")

def print_bitpattern(ins):
    ranges = sorted(instruction_bits(ins), key=lambda x: x.start)

    # Verify the bit pattern is well-defined 
    for R1, R2 in zip(ranges, ranges[1:]):
        assert(R2.start >= R1.start + R1.size)

    print("Name | Bits")
    print("---- | ----")

    # Insert initial reserved row
    if ranges[0].start > 0:
        print_range_row("_Reserved_", 0, ranges[0].start)

    # Insert rows
    for R, R2 in zip(ranges, ranges[1:] + [Pattern("End", 64, 0)]):
        print_range_row(R.name, R.start, R.size)

        # Insert reserved row after
        end = R.start + R.size
        gap = (R2.start - end) if R2 else 0
        if gap != 0:
            print_range_row("_Reserved_", end, gap)

    print("")

def print_markdown_for_instr(el):
    print(f"## {el.attrib['name']}")
    print("")
    print(f"{el.attrib['title']}.")
    print("")
    print_description(el)

    if el.tag == 'ins':
        instr = instruction_dict[el.attrib['name']]
        syn = make_syntax(instr)
        print(f"**`{instr.name}`**`{syn}`")
        print("")
        print(f"Opcode 0x{instr.opcode:X}")

        if instr.secondary_mask:
            print("")
            print(f"Secondary opcode 0x{instr.opcode2:X}")
    else:
        assert(el.tag == 'group')
        instr = instruction_dict[el.find('ins').attrib['name']]
        syn = make_syntax(instr)
        for ins in el.findall('ins'):
            print(f"**`{ins.attrib['name']}`**`{syn}`\\")

        shared_primary = "opcode" in el.attrib
        if shared_primary:
            print("")
            print(f"Primary opcode 0x{instr.opcode:X}")

        print("")
        if shared_primary:
            print("Mnemonic | Secondary opcode")
        else:
            print("Mnemonic | Opcode")
        print("--------|-------")
        for ins in el.findall('ins'):
            name = ins.attrib['name']
            opcode = ins.attrib['opcode2' if shared_primary else 'opcode']
            opcode = int(opcode, base=0)
            print(f"{name} | 0x{opcode:X}")

    print("")
    print_bitpattern(instr)

def print_lut(el):
    print("# Table of immediates in the Valhall ISA")
    print("")
    print_description(el)

    print("Index | Value | Description")
    print("------|-------|------------")
    for i, constant in enumerate(el.findall('constant')):
        print(f"{i} | `{constant.text}` | {constant.attrib['desc']}")
    print("")

def print_enum(el):
    print(f"## {el.attrib['name']}")
    print("")
    print_description(el)

    defaults = [x for x in el.findall('value') if x.attrib.get('default', '') == 'true']
    if len(defaults) > 0:
        assert(len(defaults) == 1)
        print(f"The default value is `{defaults[0].text}`.")
        print("")

    descriptions = any(['desc' in x.attrib for x in el])

    if descriptions:
        print("Index | Value | Description")
        print("------|-------|------------")
    else:
        print("Index | Value")
        print("------|------")

    i = 0
    for child in el:
        assert(child.tag in ["desc", "value", "reserved"])
        if child.tag == "desc":
            continue
        elif child.tag == "reserved":
            print(f"{i} | _Reserved_" + (" | " if descriptions else ""))
        elif child.tag == "value":
            if descriptions:
                print(f"{i} | `{child.text}` | {child.attrib.get('desc', '')}")
            else:
                print(f"{i} | `{child.text}`")

        i = i + 1

    print("")

current_section = None

def print_introduction(section, description):
    global current_section
    if section != current_section:
        current_section = section
        print(f"# {section}")
        print("")
        print(textwrap.dedent(description).strip())
        print("")
        print("\pagebreak")
        print("")

for child in root:
    if child.attrib.get('implied') == 'true':
        continue
    if child.attrib.get('name', '').startswith('TODO'):
        continue

    if child.tag == "enum":
        print_introduction("Enumerations", """
                This section describes each enumeration used in the Valhall
                ISA. Enumerations are found in the instruction metadata and as
                modifiers in individual instructions.""")
        print_enum(child)
    elif child.tag == "lut":
        print_lut(child)
    elif child.tag in ["ins", "group"]:
        print_introduction("Instruction reference", """
                The following section each known instruction in the Valhall ISA.
                It contains the instruction name, syntax, and bit pattern.

                Related instructions are grouped together if they share an
                encoding and semantics. In these cases, multiple syntax
                specifiers are shown but only one representative bit pattern is
                given.""")
        print_markdown_for_instr(child)

    print("\\pagebreak")
    print("")
