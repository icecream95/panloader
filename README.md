# panloader for rk3588, no hardware required

First, compile and insert the kernel module:

https://gitlab.com/icecream95/kbase-valhall

Grab blob drivers from here: (look for libmali-valhall-g610-g6p0-*)

https://github.com/JeffyCN/rockchip_mirrors/tree/libmali/lib/aarch64-linux-gnu

Don't forget to create symlinks for `libGLESv2.so.2`, `libEGL.so.1`,
`libgbm.so.1` and whatever other API you need (`libOpenCL.so.1`?).

Now make a build/ directory, cd in, `meson` then `ninja`. You'll get a
libpanwrap.so out in build/panwrap/libpanwrap.so, `LD_PRELOAD` it in
and set `LD_LIBRARY_PATH` to point to the blob to get a trace.

It should be safe enough to leave the library preloaded into your
shell, to make using debugging tools like gdb easier.

## dEQP

Using this with dEQP (configured for surfaceless EGL, for example the
Mesa CI builds) is simple:

```
./deqp-gles* \
        --deqp-surface-type=fbo --deqp-surface-width=256 \
        --deqp-surface-height=256 -n $case
```

## Piglit

For Piglit, `piglit.patch` in this repository patches `shader_runner`
and the OpenCL `program-tester` to integrate with panwrap, and adds a
couple of features.

Waffle must be configured with GLX disabled, for example:

```
meson -Dgbm=enabled -Dsurfaceless_egl=enabled -Dwayland=enabled -Dx11_egl=disabled -Dglx=disabled
```

Compile Piglit as normal.
