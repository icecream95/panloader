/*
 * © Copyright 2017-2018 The Panfrost Community
 * (C) COPYRIGHT 2020-2021 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU license.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef __MALI_IOCTL_DVALIN_H__
#define __MALI_IOCTL_DVALIN_H__

#include "mali-ioctl-kcpu.h"

union mali_ioctl_mem_alloc {
	union mali_ioctl_header header;
	/* [in] */
	struct {
		u64 va_pages;
		u64 commit_pages;
		u64 extent;
		u64 flags;
	} in;
	struct {
		u64 flags;
		u64 gpu_va;
	} out;
} __attribute__((packed));

struct mali_ioctl_get_gpuprops {
	u64 buffer;
	u32 size;
	u32 flags;
};

struct mali_ioctl_mem_exec_init {
    u64 va_pages;
};

#define MALI_IOCTL_TYPE_BASE  0x80
#define MALI_IOCTL_TYPE_MAX   0x81

#define MALI_IOCTL_TYPE_COUNT (MALI_IOCTL_TYPE_MAX - MALI_IOCTL_TYPE_BASE + 1)

#define MALI_IOCTL_GET_VERSION             (_IOWR(0x80,  0, struct mali_ioctl_get_version))
#define MALI_IOCTL_SET_FLAGS               ( _IOW(0x80,  1, struct mali_ioctl_set_flags))
#define MALI_IOCTL_JOB_SUBMIT              ( _IOW(0x80,  2, struct mali_ioctl_job_submit))
#define MALI_IOCTL_GET_GPUPROPS            ( _IOW(0x80,  3, struct mali_ioctl_get_gpuprops))
#define MALI_IOCTL_POST_TERM               (  _IO(0x80,  4))
#define MALI_IOCTL_MEM_ALLOC               (_IOWR(0x80,  5, union mali_ioctl_mem_alloc))
#define MALI_IOCTL_MEM_QUERY               (_IOWR(0x80,  6, struct mali_ioctl_mem_query))
#define MALI_IOCTL_MEM_FREE                ( _IOW(0x80,  7, struct mali_ioctl_mem_free))
#define MALI_IOCTL_HWCNT_SETUP             ( _IOW(0x80,  8, __ioctl_placeholder))
#define MALI_IOCTL_HWCNT_ENABLE            ( _IOW(0x80,  9, __ioctl_placeholder))
#define MALI_IOCTL_HWCNT_DUMP              (  _IO(0x80, 10))
#define MALI_IOCTL_HWCNT_CLEAR             (  _IO(0x80, 11))
#define MALI_IOCTL_DISJOINT_QUERY          ( _IOR(0x80, 12, __ioctl_placeholder))
// Get DDK version
// mem jit init
#define MALI_IOCTL_SYNC                    ( _IOW(0x80, 15, struct mali_ioctl_sync))
#define MALI_IOCTL_FIND_CPU_OFFSET         (_IOWR(0x80, 16, __ioctl_placeholder))
#define MALI_IOCTL_GET_CONTEXT_ID          ( _IOR(0x80, 17, struct mali_ioctl_get_context_id))
// TLStream acquire
// TLStream Flush
#define MALI_IOCTL_MEM_COMMIT              ( _IOW(0x80, 20, struct mali_ioctl_mem_commit))
#define MALI_IOCTL_MEM_ALIAS               (_IOWR(0x80, 21, struct mali_ioctl_mem_alias))
#define MALI_IOCTL_MEM_IMPORT              (_IOWR(0x80, 22, struct mali_ioctl_mem_import))
#define MALI_IOCTL_MEM_FLAGS_CHANGE        ( _IOW(0x80, 23, struct mali_ioctl_mem_flags_change))
#define MALI_IOCTL_STREAM_CREATE           ( _IOW(0x80, 24, struct mali_ioctl_stream_create))
#define MALI_IOCTL_FENCE_VALIDATE          ( _IOW(0x80, 25, __ioctl_placeholder))
#define MALI_IOCTL_GET_PROFILING_CONTROLS  ( _IOW(0x80, 26, __ioctl_placeholder))
#define MALI_IOCTL_DEBUGFS_MEM_PROFILE_ADD ( _IOW(0x80, 27, __ioctl_placeholder))
// Soft event update
// sticky resource map
// sticky resource unmap
// Find gpu start and offset
#define MALI_IOCTL_HWCNT_SET               ( _IOW(0x80, 32, __ioctl_placeholder))

struct mali_ioctl_mem_jit_init {
	u64 va_pages;
	u8 max_allocations;
	u8 trim_level;
	u8 group_id;
	u8 padding[5];
	u64 phys_pages;
};
#define MALI_IOCTL_MEM_JIT_INIT            ( _IOW(0x80, 14, struct mali_ioctl_mem_jit_init))

struct mali_ioctl_cs_queue_register {
	u64 buffer_gpu_addr;
	u32 buffer_size;
	u8 priority;
	u8 padding[3];
};

#define MALI_IOCTL_CS_QUEUE_REGISTER       ( _IOW(0x80, 36, struct mali_ioctl_cs_queue_register))

union kbase_ioctl_cs_queue_bind {
	struct {
		u64 buffer_gpu_addr;
		u8 group_handle;
		u8 csi_index;
		u8 padding[6];
	} in;
	struct {
		u64 mmap_handle;
	} out;
};

#define MALI_IOCTL_CS_QUEUE_BIND \
	_IOWR(0x80, 39, union kbase_ioctl_cs_queue_bind)

union kbase_ioctl_cs_queue_group_create_1_6 {
	struct {
		u64 tiler_mask;
		u64 fragment_mask;
		u64 compute_mask;
		u8 cs_min;
		u8 priority;
		u8 tiler_max;
		u8 fragment_max;
		u8 compute_max;
		u8 padding[3];

	} in;
	struct {
		u8 group_handle;
		u8 padding[3];
		u32 group_uid;
	} out;
};

#define MALI_IOCTL_CS_QUEUE_GROUP_CREATE_1_6                                  \
	_IOWR(0x80, 42, union kbase_ioctl_cs_queue_group_create_1_6)

union kbase_ioctl_cs_tiler_heap_init {
	struct {
		u32 chunk_size;
		u32 initial_chunks;
		u32 max_chunks;
		u16 target_in_flight;
		u8 group_id;
		u8 padding;
	} in;
	struct {
		u64 gpu_heap_va;
		u64 first_chunk_va;
	} out;
};

#define MALI_IOCTL_CS_TILER_HEAP_INIT \
	_IOWR(0x80, 48, union kbase_ioctl_cs_tiler_heap_init)

union kbase_ioctl_cs_get_glb_iface {
	struct {
		u32 max_group_num;
		u32 max_total_stream_num;
		u64 groups_ptr;
		u64 streams_ptr;
	} in;
	struct {
		u32 glb_version;
		u32 features;
		u32 group_num;
		u32 prfcnt_size;
		u32 total_stream_num;
		u32 instr_features;
	} out;
};

#define MALI_IOCTL_CS_GET_GLB_IFACE \
	_IOWR(0x80, 51, union kbase_ioctl_cs_get_glb_iface)

struct kbase_ioctl_cs_queue_kick {
        u64 buffer_gpu_addr;
};

#define MALI_IOCTL_CS_QUEUE_KICK \
	_IOW(0x80, 37, struct kbase_ioctl_cs_queue_kick)

struct kbase_ioctl_kcpu_queue_new {
	u8 id;
	u8 padding[7];
};

#define MALI_IOCTL_KCPU_QUEUE_CREATE \
	_IOR(0x80, 45, struct kbase_ioctl_kcpu_queue_new)

#define MALI_IOCTL_CS_EVENT_SIGNAL \
	_IO(0x80, 44)


struct kbase_ioctl_kcpu_queue_enqueue {
	u64 addr;
	u32 nr_commands;
	u8 id;
	u8 padding[3];
};

#define MALI_IOCTL_KCPU_QUEUE_ENQUEUE \
	_IOW(0x80, 47, struct kbase_ioctl_kcpu_queue_enqueue)

// https://github.com/ianmacd/d2s/blob/master/drivers/gpu/arm/b_r16p0/mali_kbase_core_linux.c
#define MALI_IOCTL_MEM_EXEC_INIT           ( _IOW(0x80, 38, struct mali_ioctl_mem_exec_init))
// gwt start
// gwt stop
// gwt dump
/// Begin TEST type region
/// End TEST type region
#endif /* __MALI_IOCTL_DVALIN_H__ */
