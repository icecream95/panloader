/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
/*
 *
 * (C) COPYRIGHT 2020-2021 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you can access it online at
 * http://www.gnu.org/licenses/gpl-2.0.html.
 *
 */

#ifndef _UAPI_BASE_CSF_KERNEL_H_
#define _UAPI_BASE_CSF_KERNEL_H_


/* The upper limit for number of objects that could be waited/set per command.
 * This limit is now enforced as internally the error inherit inputs are
 * converted to 32-bit flags in a u32 variable occupying a previously padding
 * field.
 */
#define BASEP_KCPU_CQS_MAX_NUM_OBJS ((size_t)32)

/**
 * enum base_kcpu_command_type - Kernel CPU queue command type.
 * @BASE_KCPU_COMMAND_TYPE_FENCE_SIGNAL:       fence_signal,
 * @BASE_KCPU_COMMAND_TYPE_FENCE_WAIT:         fence_wait,
 * @BASE_KCPU_COMMAND_TYPE_CQS_WAIT:           cqs_wait,
 * @BASE_KCPU_COMMAND_TYPE_CQS_SET:            cqs_set,
 * @BASE_KCPU_COMMAND_TYPE_CQS_WAIT_OPERATION: cqs_wait_operation,
 * @BASE_KCPU_COMMAND_TYPE_CQS_SET_OPERATION:  cqs_set_operation,
 * @BASE_KCPU_COMMAND_TYPE_MAP_IMPORT:         map_import,
 * @BASE_KCPU_COMMAND_TYPE_UNMAP_IMPORT:       unmap_import,
 * @BASE_KCPU_COMMAND_TYPE_UNMAP_IMPORT_FORCE: unmap_import_force,
 * @BASE_KCPU_COMMAND_TYPE_JIT_ALLOC:          jit_alloc,
 * @BASE_KCPU_COMMAND_TYPE_JIT_FREE:           jit_free,
 * @BASE_KCPU_COMMAND_TYPE_GROUP_SUSPEND:      group_suspend,
 * @BASE_KCPU_COMMAND_TYPE_ERROR_BARRIER:      error_barrier,
 */
enum base_kcpu_command_type {
	BASE_KCPU_COMMAND_TYPE_FENCE_SIGNAL,
	BASE_KCPU_COMMAND_TYPE_FENCE_WAIT,
	BASE_KCPU_COMMAND_TYPE_CQS_WAIT,
	BASE_KCPU_COMMAND_TYPE_CQS_SET,
	BASE_KCPU_COMMAND_TYPE_CQS_WAIT_OPERATION,
	BASE_KCPU_COMMAND_TYPE_CQS_SET_OPERATION,
	BASE_KCPU_COMMAND_TYPE_MAP_IMPORT,
	BASE_KCPU_COMMAND_TYPE_UNMAP_IMPORT,
	BASE_KCPU_COMMAND_TYPE_UNMAP_IMPORT_FORCE,
	BASE_KCPU_COMMAND_TYPE_JIT_ALLOC,
	BASE_KCPU_COMMAND_TYPE_JIT_FREE,
	BASE_KCPU_COMMAND_TYPE_GROUP_SUSPEND,
	BASE_KCPU_COMMAND_TYPE_ERROR_BARRIER
};

/**
 * enum base_queue_group_priority - Priority of a GPU Command Queue Group.
 * @BASE_QUEUE_GROUP_PRIORITY_HIGH:     GPU Command Queue Group is of high
 *                                      priority.
 * @BASE_QUEUE_GROUP_PRIORITY_MEDIUM:   GPU Command Queue Group is of medium
 *                                      priority.
 * @BASE_QUEUE_GROUP_PRIORITY_LOW:      GPU Command Queue Group is of low
 *                                      priority.
 * @BASE_QUEUE_GROUP_PRIORITY_REALTIME: GPU Command Queue Group is of real-time
 *                                      priority.
 * @BASE_QUEUE_GROUP_PRIORITY_COUNT:    Number of GPU Command Queue Group
 *                                      priority levels.
 *
 * Currently this is in order of highest to lowest, but if new levels are added
 * then those new levels may be out of order to preserve the ABI compatibility
 * with previous releases. At that point, ensure assignment to
 * the 'priority' member in &kbase_queue_group is updated to ensure it remains
 * a linear ordering.
 *
 * There should be no gaps in the enum, otherwise use of
 * BASE_QUEUE_GROUP_PRIORITY_COUNT in kbase must be updated.
 */
enum base_queue_group_priority {
	BASE_QUEUE_GROUP_PRIORITY_HIGH = 0,
	BASE_QUEUE_GROUP_PRIORITY_MEDIUM,
	BASE_QUEUE_GROUP_PRIORITY_LOW,
	BASE_QUEUE_GROUP_PRIORITY_REALTIME,
	BASE_QUEUE_GROUP_PRIORITY_COUNT
};

struct base_kcpu_command_fence_info {
	u64 fence;
};

struct base_cqs_wait_info {
	u64 addr;
	u32 val;
	u32 padding;
};

struct base_kcpu_command_cqs_wait_info {
	u64 objs;
	u32 nr_objs;
	u32 inherit_err_flags;
};

struct base_cqs_set {
	u64 addr;
};

struct base_kcpu_command_cqs_set_info {
	u64 objs;
	u32 nr_objs;
	u32 padding;
};

/**
 * typedef basep_cqs_data_type - Enumeration of CQS Data Types
 *
 * @BASEP_CQS_DATA_TYPE_U32: The Data Type of a CQS Object's value
 *                           is an unsigned 32-bit integer
 * @BASEP_CQS_DATA_TYPE_U64: The Data Type of a CQS Object's value
 *                           is an unsigned 64-bit integer
 */
typedef enum PACKED {
	BASEP_CQS_DATA_TYPE_U32 = 0,
	BASEP_CQS_DATA_TYPE_U64 = 1,
} basep_cqs_data_type;

/**
 * typedef basep_cqs_wait_operation_op - Enumeration of CQS Object Wait
 *                                Operation conditions
 *
 * @BASEP_CQS_WAIT_OPERATION_LE: CQS Wait Operation indicating that a
 *                                wait will be satisfied when a CQS Object's
 *                                value is Less than or Equal to
 *                                the Wait Operation value
 * @BASEP_CQS_WAIT_OPERATION_GT: CQS Wait Operation indicating that a
 *                                wait will be satisfied when a CQS Object's
 *                                value is Greater than the Wait Operation value
 */
typedef enum {
	BASEP_CQS_WAIT_OPERATION_LE = 0,
	BASEP_CQS_WAIT_OPERATION_GT = 1,
} basep_cqs_wait_operation_op;

struct base_cqs_wait_operation_info {
	u64 addr;
	u64 val;
	u8 operation;
	u8 data_type;
	u8 padding[6];
};

/**
 * struct base_kcpu_command_cqs_wait_operation_info - structure which contains information
 *		about the Timeline CQS wait objects
 *
 * @objs:              An array of Timeline CQS waits.
 * @nr_objs:           Number of Timeline CQS waits in the array.
 * @inherit_err_flags: Bit-pattern for the CQSs in the array who's error field
 *                     to be served as the source for importing into the
 *                     queue's error-state.
 */
struct base_kcpu_command_cqs_wait_operation_info {
	u64 objs;
	u32 nr_objs;
	u32 inherit_err_flags;
};

/**
 * typedef basep_cqs_set_operation_op - Enumeration of CQS Set Operations
 *
 * @BASEP_CQS_SET_OPERATION_ADD: CQS Set operation for adding a value
 *                                to a synchronization object
 * @BASEP_CQS_SET_OPERATION_SET: CQS Set operation for setting the value
 *                                of a synchronization object
 */
typedef enum {
	BASEP_CQS_SET_OPERATION_ADD = 0,
	BASEP_CQS_SET_OPERATION_SET = 1,
} basep_cqs_set_operation_op;

struct base_cqs_set_operation_info {
	u64 addr;
	u64 val;
	u8 operation;
	u8 data_type;
	u8 padding[6];
};

/**
 * struct base_kcpu_command_cqs_set_operation_info - structure which contains information
 *		about the Timeline CQS set objects
 *
 * @objs:    An array of Timeline CQS sets.
 * @nr_objs: Number of Timeline CQS sets in the array.
 * @padding: Structure padding, unused bytes.
 */
struct base_kcpu_command_cqs_set_operation_info {
	u64 objs;
	u32 nr_objs;
	u32 padding;
};

/**
 * struct base_kcpu_command_import_info - structure which contains information
 *		about the imported buffer.
 *
 * @handle:	Address of imported user buffer.
 */
struct base_kcpu_command_import_info {
	u64 handle;
};

/**
 * struct base_kcpu_command_jit_alloc_info - structure which contains
 *		information about jit memory allocation.
 *
 * @info:	An array of elements of the
 *		struct base_jit_alloc_info type.
 * @count:	The number of elements in the info array.
 * @padding:	Padding to a multiple of 64 bits.
 */
struct base_kcpu_command_jit_alloc_info {
	u64 info;
	u8 count;
	u8 padding[7];
};

/**
 * struct base_kcpu_command_jit_free_info - structure which contains
 *		information about jit memory which is to be freed.
 *
 * @ids:	An array containing the JIT IDs to free.
 * @count:	The number of elements in the ids array.
 * @padding:	Padding to a multiple of 64 bits.
 */
struct base_kcpu_command_jit_free_info {
	u64 ids;
	u8 count;
	u8 padding[7];
};

/**
 * struct base_kcpu_command_group_suspend_info - structure which contains
 *		suspend buffer data captured for a suspended queue group.
 *
 * @buffer:		Pointer to an array of elements of the type char.
 * @size:		Number of elements in the @buffer array.
 * @group_handle:	Handle to the mapping of CSG.
 * @padding:		padding to a multiple of 64 bits.
 */
struct base_kcpu_command_group_suspend_info {
	u64 buffer;
	u32 size;
	u8 group_handle;
	u8 padding[3];
};


/**
 * struct base_kcpu_command - kcpu command.
 * @type:	type of the kcpu command, one enum base_kcpu_command_type
 * @padding:	padding to a multiple of 64 bits
 * @info:	structure which contains information about the kcpu command;
 *		actual type is determined by @p type
 * @info.fence:            Fence
 * @info.cqs_wait:         CQS wait
 * @info.cqs_set:          CQS set
 * @info.import:           import
 * @info.jit_alloc:        jit allocation
 * @info.jit_free:         jit deallocation
 * @info.suspend_buf_copy: suspend buffer copy
 * @info.sample_time:      sample time
 * @info.padding:          padding
 */
struct base_kcpu_command {
	u8 type;
	u8 padding[sizeof(u64) - sizeof(u8)];
	union {
		struct base_kcpu_command_fence_info fence;
		struct base_kcpu_command_cqs_wait_info cqs_wait;
		struct base_kcpu_command_cqs_set_info cqs_set;
		struct base_kcpu_command_cqs_wait_operation_info cqs_wait_operation;
		struct base_kcpu_command_cqs_set_operation_info cqs_set_operation;
		struct base_kcpu_command_import_info import;
		struct base_kcpu_command_jit_alloc_info jit_alloc;
		struct base_kcpu_command_jit_free_info jit_free;
		struct base_kcpu_command_group_suspend_info suspend_buf_copy;
		u64 padding[2]; /* No sub-struct should be larger */
	} info;
};


#endif

