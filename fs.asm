LD_VAR_IMM_F32.sample.store.v4.f32.slot0.wait0 @r0:r1:r2:r3, r61, r0, index:0x0

# XXX: can we fold these comparisons in?
V2F32_TO_V2F16 r0, r0, r1
V2F32_TO_V2F16 r1, r2, r3

NOP.wait0126
ATEST.td @r60, r60, 0x3F800000, atest_datum
NOP.barrier
BLEND.slot0.v4.f16.return @r0:r1, @r60, blend_descriptor_0_x, target:0x0
IADD_IMM.i32 r54, 0x0, #0x0
BRANCHZI.eq.wait0 0x0, blend_descriptor_0_y
