#define EGL_EGL_PROTOTYPES 1
#define EGL_EGLEXT_PROTOTYPES 1

#define _GNU_SOURCE

/* TODO: Translate from 64-bit attribs to 32-bit.. */

#include <dlfcn.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#define dlsym_suffix(sym, suf) (dlsym(RTLD_NEXT, (sym)) ?: dlsym(RTLD_NEXT, (sym suf)))

#define KHRdlsym(sym) dlsym_suffix(sym, "KHR")
#define EXTdlsym(sym) dlsym_suffix(sym, "EXT")

EGLint eglClientWaitSync (EGLDisplay dpy, EGLSync sync, EGLint flags, EGLTime timeout)
{
        PFNEGLCLIENTWAITSYNCKHRPROC fn = KHRdlsym("eglClientWaitSync");
        return fn(dpy, sync, flags, timeout);
}

EGLBoolean eglWaitSync (EGLDisplay dpy, EGLSync sync, EGLint flags)
{
        PFNEGLWAITSYNCKHRPROC fn = KHRdlsym("eglWaitSync");
        return fn(dpy, sync, flags);
}

EGLImage eglCreateImage (EGLDisplay dpy, EGLContext ctx, EGLenum target, EGLClientBuffer buffer, const EGLAttrib *attrib_list)
{
        PFNEGLCREATEIMAGEKHRPROC fn = KHRdlsym("eglCreateImage");
        return fn(dpy, ctx, target, buffer, attrib_list);
}

EGLBoolean eglDestroyImage (EGLDisplay dpy, EGLImage image)
{
        PFNEGLDESTROYIMAGEKHRPROC fn = KHRdlsym("eglDestroyImage");
        return fn(dpy, image);
}

EGLBoolean eglDestroySync (EGLDisplay dpy, EGLSync sync)
{
        PFNEGLDESTROYSYNCKHRPROC fn = KHRdlsym("eglDestroySync");
        return fn(dpy, sync);
}

EGLImage eglCreateSync (EGLDisplay dpy, EGLenum type, const EGLAttrib *attrib_list)
{
        PFNEGLCREATESYNCKHRPROC fn = KHRdlsym("eglCreateSync");
        return fn(dpy, type, attrib_list);
}

EGLSurface eglCreatePlatformPixmapSurface (EGLDisplay dpy, EGLConfig config, void *native_pixmap, const EGLAttrib *attrib_list)
{
        PFNEGLCREATEPLATFORMPIXMAPSURFACEEXTPROC fn =
                EXTdlsym("eglCreatePlatformPixmapSurface");

        return fn(dpy, config, native_pixmap, attrib_list);
}

EGLSurface eglCreatePlatformWindowSurface (EGLDisplay dpy, EGLConfig config, void *native_pixmap, const EGLAttrib *attrib_list)
{
        PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC fn =
                EXTdlsym("eglCreatePlatformWindowSurface");

        return fn(dpy, config, native_pixmap, attrib_list);
}

EGLDisplay eglGetPlatformDisplay (EGLenum platform, void *native_display, const EGLAttrib *attrib_list)
{
        PFNEGLGETPLATFORMDISPLAYEXTPROC fn =
                EXTdlsym("eglGetPlatformDisplay");
        return fn(platform, native_display, attrib_list);
}

EGLBoolean eglGetSyncAttrib (EGLDisplay dpy, EGLSync sync, EGLint attribute, EGLAttrib *value)
{
        PFNEGLGETSYNCATTRIBKHRPROC fn = KHRdlsym("eglGetSyncAttrib");
        return fn(dpy, sync, attribute, value);
}
