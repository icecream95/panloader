/*
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <linux/ioctl.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
#include <linux/limits.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <dlfcn.h>
#include <assert.h>
#include <poll.h>

#include <mali-ioctl.h>
#include <mali-props.h>
#include <list.h>
#include "panwrap.h"
#include "wrap.h"

#include "props-6221.h"

#ifdef __aarch64__
#define PANWRAP_CSF
#define IS_CSF 1
#else
#define PANWRAP_JM
#define IS_CSF 0
#endif

// 0: don't fake anything
// 1: fake things, still call kernel
// 2: fake everything, don't call kernel
// 3: use panfrost for BOs, don't submit GPU jobs
// 4: use panfrost kernel driver
#ifdef PANWRAP_CSF
static int fake = 0;
#else
static int fake = 2;
#endif

struct panfrost_device *pan_dev;


static pthread_mutex_t l;
static void __attribute__((constructor)) panloader_constructor(void) {
	pthread_mutexattr_t mattr;

	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&l, &mattr);
	pthread_mutexattr_destroy(&mattr);
}

#define IOCTL_CASE(request) (_IO(_IOC_TYPE(request), _IOC_NR(request)))

struct ioctl_info {
	const char *name;
};

struct device_info {
	const char *name;
	const struct ioctl_info info[MALI_IOCTL_TYPE_COUNT][_IOC_NR(0xffffffff)];
};

typedef void* (mmap_func)(void *, size_t, int, int, int, loff_t);
typedef int (open_func)(const char *, int flags, ...);

#define IOCTL_TYPE(type) [type - MALI_IOCTL_TYPE_BASE] =
#define IOCTL_INFO(n) [_IOC_NR(MALI_IOCTL_##n)] = { .name = #n }
static struct device_info mali_info = {
	.name = "mali",
	.info = {
		IOCTL_TYPE(0x80) {
			IOCTL_INFO(GET_VERSION),
#ifndef dvalin
		},
		IOCTL_TYPE(0x82) {
#endif
                        IOCTL_INFO(SET_FLAGS),
                        IOCTL_INFO(GET_GPUPROPS),
                        IOCTL_INFO(GET_CONTEXT_ID),
                        IOCTL_INFO(STREAM_CREATE),
                        IOCTL_INFO(POST_TERM),
                        IOCTL_INFO(MEM_EXEC_INIT),
			IOCTL_INFO(MEM_JIT_INIT),
			IOCTL_INFO(MEM_ALLOC),
			IOCTL_INFO(MEM_IMPORT),
			IOCTL_INFO(MEM_COMMIT),
			IOCTL_INFO(MEM_QUERY),
			IOCTL_INFO(MEM_FREE),
			IOCTL_INFO(MEM_FLAGS_CHANGE),
			IOCTL_INFO(MEM_ALIAS),
			IOCTL_INFO(SYNC),
                        IOCTL_INFO(DEBUGFS_MEM_PROFILE_ADD),
#ifndef dvalin
			IOCTL_INFO(GPU_PROPS_REG_DUMP),
			IOCTL_INFO(GET_VERSION_NEW),
#endif
			IOCTL_INFO(JOB_SUBMIT),

			IOCTL_INFO(CS_QUEUE_REGISTER),
			IOCTL_INFO(CS_QUEUE_BIND),
			IOCTL_INFO(CS_QUEUE_GROUP_CREATE_1_6),
			IOCTL_INFO(CS_TILER_HEAP_INIT),
			IOCTL_INFO(CS_GET_GLB_IFACE),
			IOCTL_INFO(KCPU_QUEUE_CREATE),
                        IOCTL_INFO(KCPU_QUEUE_ENQUEUE),
			IOCTL_INFO(CS_QUEUE_KICK),
			IOCTL_INFO(CS_EVENT_SIGNAL),
		},
	},
};
#undef IOCTL_INFO
#undef IOCTL_TYPE

static inline const struct ioctl_info *
ioctl_get_info(unsigned long int request)
{
	return &mali_info.info[_IOC_TYPE(request) - MALI_IOCTL_TYPE_BASE]
	                      [_IOC_NR(request)];
}

static int mali_fd = 0;
static int mali_fd_write = 0;
static LIST_HEAD(allocations);
static LIST_HEAD(mmaps);

#define LOCK()   pthread_mutex_lock(&l);
#define UNLOCK() pthread_mutex_unlock(&l)

int ioctl(int fd, int _request, ...);

#include "decode.h"

__attribute__((noreturn)) void
panwrap_at_exit(void)
{
        LOCK();
        printf("start code dump\n");

#if 0 && defined(PANWRAP_CSF)
	PROLOG(mmap);

        void *addr = orig_mmap(NULL, 4096 * 146, PROT_READ | PROT_WRITE,
                               MAP_SHARED, mali_fd, 0x2e000);
        if (addr == MAP_FAILED) {
            perror("mmap(csf_interface)");
        } else {
            hexdump(stderr, addr, 4096 * 146, NULL, 0, 0);
        }
#endif

        pandecode_reset_frame();

        pandecode_dump_mappings(NULL);
        printf("end core dump\n");
        fflush(NULL);
        abort();
}


// Haaaaack!
pthread_t main_thread;

int
my_sync_object_wait(int a, int b, int c, int d)
{
        usleep(1000);
        LOCK();
        UNLOCK();
        printf("sync_object %x %x %x %x\n", a, b, c, d);
        return 1;
}

static void
panwrap_after_open(void)
{
        const char *fake_env = getenv("FAKE");
        if (fake_env)
                fake = strtol(fake_env, NULL, 10);

        // TODO: Don't do this if running with real hw
        if (true) {
                const char *fns[] = {
                        "osup_sync_object_wait",
                        "osup_sync_object_timedwait",
                };
                for (int i = 0; i < (sizeof(fns) / sizeof(*fns)); ++i) {
                        uint32_t *function = (uint32_t*)((uintptr_t)dlsym(RTLD_DEFAULT, fns[i]) & ~1);
                        printf("fn: %s %p\n", fns[i], function);
                        if (mprotect((void*)((uintptr_t)function & ~4095), 4096, PROT_READ | PROT_WRITE | PROT_EXEC)) {
                                perror("mprotect");
                                break;
                        }
#ifdef __aarch64__
                        function[0] = 0xd65f03c0; // ret
#else
                        // ldr.w   ip, [pc, #4]
                        // bx      ip
                        function[0] = 0xf8df | (0xc004 << 16);
                        function[1] = 0x4760;
                        function[2] = (uintptr_t)my_sync_object_wait;
#endif
                }
        }

        if (!IS_CSF && fake > 1) {
                int pipefd[2];
                pipe(pipefd);
                mali_fd_write = pipefd[1];
                dup2(pipefd[0], mali_fd);

                /* Avoid symbol versioning problems */
                int (*fcntl_ptr)(int, int, int) = dlsym(RTLD_DEFAULT, "fcntl");

                fcntl_ptr(mali_fd, F_SETFL, fcntl_ptr(mali_fd, F_GETFL, 0) | O_NONBLOCK);
        }

        if (fake >= 3) {
//                pan_dev = panblob_create_device();

//                if (!pan_dev)
//                        fake = 1;
        }
}

/**
 * Overriden libc functions start here
 */
static inline int
panwrap_open_wrap(open_func *func, const char *path, int flags, va_list args)
{
	mode_t mode = 0;
	int ret;

	if (flags & O_CREAT) {
		mode = (mode_t) va_arg(args, int);
		ret = func(path, flags, mode);
	} else {
		ret = func(path, flags);
	}

	LOCK();
	if (strcmp(path, "/dev/mali0") == 0) {
            printf("open dev/mali0\n");
            // todo; base this on 'fake'..
//                ret = func("/dev/null", O_RDWR);
		mali_fd = ret;
                panwrap_after_open();
        }
	UNLOCK();

	return ret;
}

int
__open_2(const char *path, int flags)
{
	PROLOG(__open_2);

        printf("open2\n");

        if (strcmp(path, "/dev/mali0") == 0) {
            // TODO: What if fake is not set?
            int ret = orig___open_2("/dev/null", O_RDWR);
            mali_fd = ret;

            panwrap_after_open();
            return mali_fd;
        }

	return orig___open_2(path, flags);
}

int
open(const char *path, int flags, ...)
{
	PROLOG(open);
	va_list args;
	va_start(args, flags);
	int o = panwrap_open_wrap(orig_open, path, flags, args);
	va_end(args);
	return o;
}

#define PAN_BO_EXECUTE            (1 << 0)
#define PAN_BO_GROWABLE           (1 << 1)
#define PAN_BO_INVISIBLE          (1 << 2)
#define PAN_BO_SAME_VA            (1 << 5)

#define PANFROST_JD_REQ_FS        (1 << 0)

#ifdef PANWRAP_JM
int
__fxstat (int vers, int fd, struct stat *buf)
{
        PROLOG(__fxstat);

        if (fd != mali_fd)
                return orig___fxstat(vers, fd, buf);

        if (fake) {
                printf("faking fstat\n");
                memset(buf, 0, sizeof(struct stat));
                buf->st_mode = 0x21b6;
                return 0;
        }

        int o = orig___fxstat(vers, fd, buf);
        uint32_t *ret = (uint32_t *)buf;
        for (int i = 0; i < 16; ++i)
                printf(": 0x%x\n", ret[i]);
        return o;
}
#endif

struct mali_jd_event_v2 {
        u32 event_code;
        mali_atom_id atom_number;
        u32 :24;
        struct mali_jd_udata udata;
} __attribute__((packed));

static pthread_mutex_t events_lock;
#define MAX_PENDING_EVENTS 256
static struct mali_jd_event_v2 pending_events[MAX_PENDING_EVENTS];
static int pending_events_head; // Write to head
#ifdef PANWRAP_JM
static int pending_events_tail; // Read from tail
#endif

bool done_post_term;

#if 1
int ppoll(struct pollfd *fds, nfds_t nfds,
          const struct timespec *tmo_p, const sigset_t *sigmask)
{
        PROLOG(ppoll);
        if (!mali_fd)
                return orig_ppoll(fds, nfds, tmo_p, sigmask);

        printf("X(%i) poll", mali_fd);
        for (int i = 0; i < nfds; ++i)
                printf(" %i", fds[i].fd);
        printf("\n");

        panwrap_at_exit();
}
#endif

int panwrap_quit_next_poll;

int poll(struct pollfd *__fds, nfds_t __nfds, int __timeout)
{
        PROLOG(poll);

        if (panwrap_quit_next_poll)
                panwrap_at_exit();

        return orig_poll(__fds, __nfds, __timeout);
}

#if 0
int poll(struct pollfd *__fds, nfds_t __nfds, int __timeout)
{
        PROLOG(poll);

        printf("X(%i) poll", mali_fd);
        for (int i = 0; i < __nfds; ++i)
                printf(" %i", __fds[i].fd);
        printf("\n");

        int ret = -1;

        for (int i = 0; i < __nfds; ++i) {
                if (__fds[i].fd != mali_fd)
                        continue;

                pthread_mutex_lock(&events_lock);

                //ugh, this doesn't work with csf, does it?
                if (pending_events_head != pending_events_tail) {
                        __fds[i].events = 0;
                        // ?
//                        __timeout = 1;

                        int atom = pending_events[pending_events_tail].atom_number;

//                        if (mali_fd_write)
//                                write(mali_fd_write, &pending_events[pending_events_tail], sizeof(struct mali_jd_event_v2));
                        ++pending_events_tail;
                        pending_events_tail %= MAX_PENDING_EVENTS;
                        ret = i;
                        printf("poll returning early with atom %i (%i:%i)\n", atom, pending_events_head, pending_events_tail);

#ifndef __aarch64__
                        if (done_post_term) {
                                /* Open /dev/null writeonly so that trying to
                                 * read it will give EBADF and cause the
                                 * driver thread to exit */
                                int fd = open("/dev/null", O_WRONLY);
                                dup2(fd, mali_fd);
                        }
#endif
                } else {
                        printf("still no event\n");
                        // ISTR this not working as expected..
                        //uh, what?
                        //__fds[i].events = 0;
                }

                pthread_mutex_unlock(&events_lock);
        }

        int poll_ret = __poll(__fds, __nfds, __timeout);


        if (ret != -1) {
                __fds[ret].events = POLLIN;
                __fds[ret].revents = POLLIN;
                ++poll_ret;
        }

        for (int i = 0; i < __nfds; ++i) {
                if (__fds[i].events)
                        printf("event %i: %x\n", __fds[i].fd, __fds[i].revents);
        }

        if (false) {
                struct mali_jd_event_v2 event = {0};
                printf("%i\n", read(mali_fd, &event, sizeof(event)));
                perror("read");
                printf("event code: %x\n", event.event_code);
                printf("atom_number: %x\n", event.atom_number);
                printf("udata: %"PRIx64" %"PRIx64"\n", event.udata.blob[0], event.udata.blob[1]);
        }

        return poll_ret;
}
#endif

#ifdef PANWRAP_JM
int
__poll_chk(struct pollfd *__fds, nfds_t __nfds, int __timeout, __SIZE_TYPE__ __fdslen)
{
        if (panwrap_quit_next_poll)
                panwrap_at_exit();

        printf("X(%i) poll", mali_fd);
        for (int i = 0; i < __nfds; ++i)
                printf(" %i", __fds[i].fd);
        printf("\n");

        int ret = -1;

        if (fake >= 1)
        for (int i = 0; i < __nfds; ++i) {
                //haack!
                if (__fds[i].fd != 8)
                        continue;

                // maybe we could use.. poll.. for this? ;)
//                while (pending_events_head == pending_events_tail) {
//                        printf("waiting for event\n");
//                        usleep(50000);
//                }

                pthread_mutex_lock(&events_lock);

                //ugh, this doesn't work with csf, does it?
                if (pending_events_head != pending_events_tail) {
                        __fds[i].events = 0;
                        // ?
//                        __timeout = 1;

                        int atom = pending_events[pending_events_tail].atom_number;

//                        if (mali_fd_write)
//                                write(mali_fd_write, &pending_events[pending_events_tail], sizeof(struct mali_jd_event_v2));
                        ++pending_events_tail;
                        pending_events_tail %= MAX_PENDING_EVENTS;
                        ret = i;
                        printf("poll returning early with atom %i (%i:%i)\n", atom, pending_events_head, pending_events_tail);

                        if (done_post_term) {
                                /* Open /dev/null writeonly so that trying to
                                 * read it will give EBADF and cause the
                                 * driver thread to exit */
                                int fd = open("/dev/null", O_WRONLY);
                                dup2(fd, mali_fd);
                        }
                } else {
                        printf("still no event\n");
                        // ISTR this not working as expected..
                        //uh, what?
                        //__fds[i].events = 0;
                }

                pthread_mutex_unlock(&events_lock);
        }

        printf("poll returning normally\n");
        PROLOG(__poll_chk);
        int poll_ret = orig___poll_chk(__fds, __nfds, __timeout, __fdslen);

        if (ret != -1) {
                __fds[ret].events = POLLIN;
                __fds[ret].revents = POLLIN;
                ++poll_ret;
        }

        for (int i = 0; i < __nfds; ++i) {
                if (__fds[i].events)
                        printf("event %i: %x\n", __fds[i].fd, __fds[i].revents);
        }

        if (false) {
                struct mali_jd_event_v2 event = {0};
                printf("%i\n", read(mali_fd, &event, sizeof(event)));
                perror("read");
                printf("event code: %x\n", event.event_code);
                printf("atom_number: %x\n", event.atom_number);
                printf("udata: %"PRIx64" %"PRIx64"\n", event.udata.blob[0], event.udata.blob[1]);
        }

        return poll_ret;
}
#endif

#ifdef PANWRAP_JM
int
sem_wait(void *sem)
{
        // We can't use PROLOG because these functions are in libpthread
        int (*orig_sem_wait)(void *sem) = dlsym(RTLD_NEXT, "sem_wait");
        int (*orig_sem_wait_t)(void *sem, const struct timespec *restrict abs_timeout) = dlsym(RTLD_NEXT, "sem_timedwait");
        struct timespec ts;
        if (mali_fd) {
//                printf("sem_wait %i %p!\n", gettid(), sem);
                clock_gettime(CLOCK_REALTIME, &ts);
                ts.tv_sec += 2;
                int ret = orig_sem_wait_t(sem, &ts);
//                printf("wait ret %i %i\n", gettid(), ret);
                return ret & 0;
        }

        return orig_sem_wait(sem);
}
#endif

int
axaxsem_post(void *sem)
{
        int (*orig_sem_post)(void *sem) = dlsym(RTLD_NEXT, "sem_post");
//        if (mali_fd)
//                printf("sem_post %i %p!\n", gettid(), sem);
        return orig_sem_post(sem);
}

int
close(int fd)
{
	PROLOG(close);

        /* Intentionally racy: prevents us from trying to hold the global mutex
         * in calls from system libraries */
        if (fd <= 0 || !mali_fd || fd != mali_fd)
                return orig_close(fd);

	LOCK();
	if (!fd || fd != mali_fd) {
		mali_fd = 0;
	}
	UNLOCK();

	return orig_close(fd);
}

/* HW version */
static bool bifrost = false;
static unsigned product_id;
int allocation_number = 0;

struct pandecode_flag_info {
        u64 flag;
        const char *name;
};

static void
pandecode_log_decoded_flags(const struct pandecode_flag_info *flag_info,
                            u64 flags)
{
        bool decodable_flags_found = false;

        for (int i = 0; flag_info[i].name; i++) {
                if ((flags & flag_info[i].flag) != flag_info[i].flag)
                        continue;

                if (!decodable_flags_found) {
                        decodable_flags_found = true;
                } else {
                        printf(" | ");
                }

                printf("%s", flag_info[i].name);

                flags &= ~flag_info[i].flag;
        }

        if (decodable_flags_found) {
                if (flags)
                        printf(" | 0x%" PRIx64, flags);
        } else {
                printf("0x%" PRIx64, flags);
        }
}

#include "alloc.h"

#define FLAG_INFO(flag) { BASE_MEM_##flag, #flag }
static const struct pandecode_flag_info alloc_flag_info[] = {
        { 0xf, "RW" },
        { 0xc, "INV" },
        { 0x17, "EX" },
        FLAG_INFO(PROT_CPU_RD),
        FLAG_INFO(PROT_CPU_WR),
        FLAG_INFO(PROT_GPU_RD),
        FLAG_INFO(PROT_GPU_WR),
        FLAG_INFO(PROT_GPU_EX),
        FLAG_INFO(GROW_ON_GPF),
        FLAG_INFO(COHERENT_SYSTEM),
        FLAG_INFO(COHERENT_LOCAL),
        FLAG_INFO(CACHED_CPU),
        FLAG_INFO(SAME_VA),
        FLAG_INFO(NEED_MMAP),
        FLAG_INFO(COHERENT_SYSTEM_REQUIRED),
        FLAG_INFO(SECURE),
        FLAG_INFO(DONT_NEED),
        FLAG_INFO(IMPORT_SHARED),
        {}
};
#undef FLAG_INFO

static const char *kcpu_commands[] = {
       "fence_signal",
         "fence_wait",
           "cqs_wait",
            "cqs_set",
 "cqs_wait_operation",
  "cqs_set_operation",
         "map_import",
       "unmap_import",
 "unmap_import_force",
          "jit_alloc",
           "jit_free",
      "group_suspend",
      "error_barrier",
};

struct base_jit_alloc_info {
	u64 gpu_alloc_addr;
	u64 va_pages;
	u64 commit_pages;
	u64 extension;
	u8 id;
	u8 bin_id;
	u8 max_allocations;
	u8 flags;
	u8 padding[2];
	u16 usage_id;
	u64 heap_info_gpu_addr;
};

static void
dump_kcpu_commands(FILE *fp, struct base_kcpu_command *commands,
                   unsigned num)
{
    for (unsigned i = 0; i < num; ++i) {
        struct base_kcpu_command c = commands[i];

        if (c.type > (sizeof(kcpu_commands) / sizeof(kcpu_commands[0]))) {
            fprintf(fp, "kcpu command type %i out of range\n", c.type);
            continue;
        }

        fprintf(fp, "KCPU %i: %s:: 0x%08"PRIx64" 0x%08"PRIx64"\n", c.type, kcpu_commands[c.type], c.info.padding[0], c.info.padding[1]);

        if (c.type == 9) {
            struct base_jit_alloc_info *info = (void *)c.info.padding[0];
            printf("%"PRIu64" pages (%"PRIu64" commit, ext %"PRIu64") at 0x%"PRIx64"\n",
                   info->va_pages, info->commit_pages, info->extension,
                   info->gpu_alloc_addr);
        }

        /* Is this the best time to do this? */
//        if (c.type == 2) /* cqs_wait */
//                panwrap_at_exit();
    }

    panwrap_at_exit();
}

static int atexit_countdown;

static unsigned mem_mod;

/* XXX: Android has a messed up ioctl signature */
int ioctl(int fd, int _request, ...)
{
	PROLOG(ioctl);
	unsigned long int request = _request;
	int ioc_size = _IOC_SIZE(request);
	int ret = 0;
	void *ptr;
        bool track_alloc = true;

#ifdef dvalin
	if (request & 0x0F00)
		ioc_size = 1;
#endif

	if (ioc_size) {
		va_list args;

		va_start(args, _request);
		ptr = va_arg(args, void *);
		va_end(args);
	} else {
		ptr = NULL;
	}

        if (!fd || fd != mali_fd)
		return orig_ioctl(fd, request, ptr);

        printf("ioctl %x %s\n", _request, ioctl_get_info(_request)->name ?: "UNK");

	LOCK();

	mali_ptr alloc_va_pages = 0;

	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_MEM_ALLOC)) {
		union mali_ioctl_mem_alloc *args = ptr;

#ifdef dvalin
		args->in.flags |= MALI_MEM_PROT_CPU_RD;
		args->in.flags |= MALI_MEM_PROT_CPU_WR;
		//args->in.flags &= ~(MALI_MEM_DONT_NEED | MALI_MEM_COHERENT_LOCAL | MALI_MEM_CACHED_CPU);
		//args->in.flags |= MALI_MEM_COHERENT_SYSTEM | MALI_MEM_COHERENT_SYSTEM_REQUIRED;

		alloc_va_pages = args->in.va_pages;
#else
		alloc_va_pages = args->inout.va_pages;
#endif

                if (atexit_countdown && !--atexit_countdown) {
                        printf("set atexit\n");
                        atexit(panwrap_at_exit);
                }
	}

        bool skip = false;

        if (fake) {
                switch (IOCTL_CASE(request)) {

                case IOCTL_CASE(MALI_IOCTL_CS_QUEUE_BIND): {
                    union kbase_ioctl_cs_queue_bind *args = ptr;
                    printf("bind %"PRIx64" handle %i csi %i\n", args->in.buffer_gpu_addr, args->in.group_handle, args->in.csi_index);
                    memset((void *)(uintptr_t)args->in.buffer_gpu_addr, 0xfa, 65536);
                    pandecode_cs_queue_bind(args->in.buffer_gpu_addr, args->in.group_handle, args->in.csi_index);
//                    args->out.mmap_handle = 0x123456;
                    break;
                }

                case IOCTL_CASE(MALI_IOCTL_CS_QUEUE_REGISTER): {
                        const struct mali_ioctl_cs_queue_register *reg = ptr;
                        printf("register %"PRIx64" size %i\n",
                               reg->buffer_gpu_addr, reg->buffer_size);
                        break;
                }

                case IOCTL_CASE(MALI_IOCTL_CS_EVENT_SIGNAL):
                        printf("atexit_countdown = 3\n");
                        atexit_countdown = 3;
                        break;

                case IOCTL_CASE(MALI_IOCTL_KCPU_QUEUE_ENQUEUE): {
                        struct kbase_ioctl_kcpu_queue_enqueue *e = ptr;
                        struct base_kcpu_command *commands = (void *)(uintptr_t)e->addr;
                        dump_kcpu_commands(stdout, commands, e->nr_commands);
                        break;
                }

                case IOCTL_CASE(MALI_IOCTL_GET_VERSION): {
                        struct mali_ioctl_get_version *args = ptr;
                        fprintf(stderr, "maj %i min %i\n", args->major, args->minor);
                        if (fake > 1) skip = true;
                        break;
                }
                case IOCTL_CASE(MALI_IOCTL_POST_TERM): {
                    printf("DONE_POST_TERM!\n");
                        done_post_term = true;
                        if (fake > 1) {
                                /* Add another event to trigger the poll early
                                 * return path */
                                pending_events_head = (pending_events_head + 1) % MAX_PENDING_EVENTS;
                        }
                        break;
                }
                case IOCTL_CASE(MALI_IOCTL_MEM_EXEC_INIT):
                    printf("pages: %"PRIx64"\n", ((uint64_t*)ptr)[0]);
                    /* fallthrough */
                case IOCTL_CASE(MALI_IOCTL_SET_FLAGS):
                case IOCTL_CASE(MALI_IOCTL_SYNC): /* Getting sync stuff needs to be handled properly for things to work! I think... */
                case IOCTL_CASE(MALI_IOCTL_GET_CONTEXT_ID): /* Probably fine, maybe we need to make it consistent */
                case IOCTL_CASE(MALI_IOCTL_GET_GPUPROPS): /* handled later */
                case IOCTL_CASE(MALI_IOCTL_DEBUGFS_MEM_PROFILE_ADD): /* What does this do? Maybe it's important? */
                        if (fake > 1) skip = true;
                        break;
                case IOCTL_CASE(MALI_IOCTL_MEM_ALLOC): {
                        union mali_ioctl_mem_alloc *args = ptr;
                        printf("alloc %i bytes, %i virt, %i growth, ",
                               (int)args->in.commit_pages * 4096,
                               (int)(args->in.va_pages - args->in.commit_pages) * 4096,
                               (int)args->in.extent * 4096);

                        int flags = args->in.flags;
                        pandecode_log_decoded_flags(alloc_flag_info, flags);
                        if (fake != 2) printf("\n");

                        if (mem_mod > args->in.va_pages)
                                args->in.va_pages = mem_mod;

                        args->in.commit_pages = args->in.va_pages;
                        args->in.extent = 0;
                        args->in.flags &= ~BASE_MEM_GROW_ON_GPF;

                        unsigned size = args->in.va_pages * 4096;

                        if (fake == 2) {
                                void *out = aligned_alloc(4096, size);
                                memset(out, 0, size);
                                args->out.gpu_va = (uintptr_t)out;
                                args->out.flags = flags;
                                skip = true;
                                printf(", out=%p\n", out);
                        } else if (fake >= 3) {
                                unsigned pan_flags = 0;
                                if (flags & BASE_MEM_PROT_GPU_EX)
                                        pan_flags |= PAN_BO_EXECUTE;
                                if (flags & BASE_MEM_GROW_ON_GPF) {
                                        /* The blob sometimes tries to map these BOs, just
                                         * set a constant size. */
                                        if (0)
                                                pan_flags |= PAN_BO_GROWABLE | PAN_BO_INVISIBLE;
                                        else
                                                size = 1024 * 1024 * 16;
                                }
                                if (flags & BASE_MEM_SAME_VA)
                                        pan_flags |= PAN_BO_SAME_VA;

                                track_alloc = false;
//                                args->out.gpu_va = panblob_create_bo(pan_dev, size, pan_flags);
                                args->out.flags = 0;
                                skip = true;
                        }
                        break;
                }
                case IOCTL_CASE(MALI_IOCTL_MEM_ALIAS): {
                        struct mali_ioctl_mem_alias *alias = ptr;
                        const struct mali_mem_aliasing_info *info = (void *)(uintptr_t)alias->ai;

                        printf("alias %"PRIx64" %"PRIx64" %"PRIx64" %"PRIx64" ",
                               alias->flags, alias->stride, alias->nents, alias->ai);
                        pandecode_log_decoded_flags(alloc_flag_info, alias->flags);
                        printf("\n");

                        for (unsigned i = 0; i < alias->nents; ++i) {
                                printf("  - %"PRIx64" %x %x\n", info[i].handle, (int)info[i].offset, (int)info[i].length);
                        }

                        break;
                }
                case IOCTL_CASE(MALI_IOCTL_MEM_FREE):
                        printf("free %"PRIx64"\n", *(uint64_t *)ptr);
                        if (fake > 1) skip = true;
                        break;

                case IOCTL_CASE(MALI_IOCTL_MEM_COMMIT): {
                        // I don't know we can do much about this one
                        const struct mali_ioctl_mem_commit *c = ptr;
                        printf("commit %"PRIx64" %"PRIu64"\n", c->gpu_addr, c->pages * 4096);

                        // otherwise we run into bus errors when dumping
                        skip = true;
                        break;
                }

                case IOCTL_CASE(MALI_IOCTL_MEM_QUERY): {
                        const struct mali_ioctl_mem_query *q = ptr;
                        // only see commit size here
                        // maybe we need to store this locally and guess
                        printf("query %"PRIx64" %i\n", q->gpu_addr, q->query);
                        break;
                }

                case IOCTL_CASE(MALI_IOCTL_JOB_SUBMIT): {
                        // TODO: Dependency stuff
                        const struct mali_ioctl_job_submit *js = ptr;
                        struct mali_jd_atom_v2 *atoms = js->addr;

                        for (unsigned i = 0; i < js->nr_atoms; ++i) {
                                printf("submit %"PRIx64", atom %i, udata %"PRIx64" %"PRIx64" flags %x\n",
                                       atoms[i].jc, atoms[i].atom_number, atoms[i].udata.blob[0],
                                       atoms[i].udata.blob[1], atoms[i].core_req);

                                unsigned reqs = 0;
                                if (atoms[i].core_req & (1 << 0))
                                        reqs |= PANFROST_JD_REQ_FS;

                                pthread_mutex_lock(&events_lock);

                                uint64_t jc = atoms[i].jc;
                                if (!jc) jc = atoms[i].udata.blob[0];

//                                if (fake >= 4)
//                                        panblob_submit(pan_dev, jc, reqs);

                                /* Don't add an event for soft jobs */
                                if (atoms[i].jc) {
                                        struct mali_jd_event_v2 event = {
                                                .event_code = 1,
                                                .atom_number = atoms[i].atom_number,
                                                .udata = atoms[i].udata,
                                        };
                                        pending_events[pending_events_head++] = event;
                                        pending_events_head %= MAX_PENDING_EVENTS;
                                }

                                pthread_mutex_unlock(&events_lock);
                        }
                        if (fake > 1) skip = true;
                        break;
                }

                        /* This does not seem to be essential */
                case IOCTL_CASE(MALI_IOCTL_STREAM_CREATE):
//                        ret = -1;
                        if (fake > 1) skip = true;
                        break;

                default:
                        printf("ioctl %x %s\n", _request, ioctl_get_info(_request)->name ?: "UNK");
                        break;
                }
        }

        if (!skip) {
                if (fake > 1)
                        printf("skipping ioctl\n");
                else
                        ret = orig_ioctl(fd, request, ptr);
        }

        /* TODO: Do something about this bit */
#if 0
	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_JOB_SUBMIT)) {
		const struct mali_ioctl_job_submit *js = ptr;
		if (sizeof(struct mali_jd_atom) == js->stride) {
			struct mali_jd_atom *atoms = js->addr;

			for (unsigned i = 0; i < js->nr_atoms; ++i) {
				printf("Atom %u\n", i);
				printf("seq_nr = %" PRIx64 "\n", atoms[i].seq_nr);
				printf("jc = %" PRIx64 "\n", atoms[i].jc);
				printf("udata[0] = %" PRIx64 "\n", atoms[i].udata.blob[0]);
				printf("udata[1] = %" PRIx64 "\n", atoms[i].udata.blob[1]);
				printf("nr_ext_res = %" PRIx16 "\n", atoms[i].nr_ext_res);
				printf("jit_id[0] = %" PRIx16 "\n", atoms[i].jit_id[0]);
				printf("jit_id[1] = %" PRIx16 "\n", atoms[i].jit_id[1]);
				for (unsigned j = 0; j < 2; ++j) {
					printf("pre_dep[%u].atom = %" PRIx8 "\n", j, atoms[i].pre_dep[j].atom_id);
					printf("pre_dep[%u].type = %" PRIx8 "\n", j, atoms[i].pre_dep[j].dependency_type);
				}
				printf("atom = %" PRIx8 "\n", atoms[i].atom_number);
				printf("prio = %" PRIx8 "\n", atoms[i].prio);
				printf("device_nr = %" PRIx8 "\n", atoms[i].device_nr);
				printf("jobslot = %" PRIx8 "\n", atoms[i].jobslot);
				printf("core_req = %" PRIx32 "\n", atoms[i].core_req);
				printf("renderpass_id = %" PRIx8 "\n", atoms[i].renderpass_id);
				printf("\n");

				printf("core req %X\n", atoms[i].core_req);
				if (atoms[i].core_req == 0x209) 
					pandecode_jit_alloc(atoms[i].jc, atoms[i].nr_ext_res);
				else if (atoms[i].core_req & MALI_JD_REQ_SOFT_JOB)
					printf("soft job %X\n", atoms[i].core_req);
				else
                                        pandecode_jc_v9(atoms[i].jc, product_id, NULL);
			}
		} else if (sizeof(struct mali_jd_atom_v2) == js->stride) {
			struct mali_jd_atom_v2 *atoms = js->addr;

			for (unsigned i = 0; i < js->nr_atoms; ++i) {
				printf("Atom %u\n", i);
				printf("jc = %" PRIx64 "\n", atoms[i].jc);
				printf("udata[0] = %" PRIx64 "\n", atoms[i].udata.blob[0]);
				printf("udata[1] = %" PRIx64 "\n", atoms[i].udata.blob[1]);
				printf("nr_ext_res = %" PRIx16 "\n", atoms[i].nr_ext_res);
				printf("jit_id[0] = %" PRIx16 "\n", atoms[i].jit_id[0]);
				printf("jit_id[1] = %" PRIx16 "\n", atoms[i].jit_id[1]);
				for (unsigned j = 0; j < 2; ++j) {
					printf("pre_dep[%u].atom = %" PRIx8 "\n", j, atoms[i].pre_dep[j].atom_id);
					printf("pre_dep[%u].type = %" PRIx8 "\n", j, atoms[i].pre_dep[j].dependency_type);
				}
				printf("atom = %" PRIx8 "\n", atoms[i].atom_number);
				printf("prio = %" PRIx8 "\n", atoms[i].prio);
				printf("device_nr = %" PRIx8 "\n", atoms[i].device_nr);
				printf("jobslot = %" PRIx8 "\n", atoms[i].jobslot);
				printf("core_req = %" PRIx32 "\n", atoms[i].core_req);
				printf("renderpass_id = %" PRIx8 "\n", atoms[i].renderpass_id);
				printf("\n");

				if (atoms[i].core_req == 0x209) 
					pandecode_jit_alloc(atoms[i].jc, atoms[i].nr_ext_res);
				else if (atoms[i].core_req & MALI_JD_REQ_SOFT_JOB)
					printf("soft job %X\n", atoms[i].core_req);
				else
					pandecode_jc_v9(atoms[i].jc, product_id, NULL);
			}
		} else {
			printf("[WRAP] bad atom stride, got %u\n", js->stride);
		}
	}
#endif

#if PAN_ARCH <= 9
	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_JOB_SUBMIT)) {
		usleep(1000);
		const struct mali_ioctl_job_submit *js = ptr;
		if (sizeof(struct mali_jd_atom) == js->stride) {
			struct mali_jd_atom *atoms = js->addr;

			for (unsigned i = 0; i < js->nr_atoms; ++i) {
				printf("core req %X\n", atoms[i].core_req);
				if (atoms[i].core_req & MALI_JD_REQ_SOFT_JOB)
					printf("soft job %X\n", atoms[i].core_req);
				else
                                    pandecode_jc_v9(atoms[i].jc, product_id, NULL);
			}

			if (getenv("VALHALL_DUMP"))
				pandecode_dump_mappings(NULL);
		} else if (sizeof(struct mali_jd_atom_v2) == js->stride) {
			struct mali_jd_atom_v2 *atoms = js->addr;

                        struct job_shader_info *si = job_shader_info_create();

			for (unsigned i = 0; i < js->nr_atoms; ++i) {
				printf("core req %X\n", atoms[i].core_req);
				if (atoms[i].core_req & MALI_JD_REQ_SOFT_JOB || !atoms[i].jc)
					printf("soft job %X\n", atoms[i].core_req);
				else
                                    pandecode_jc_v9(atoms[i].jc, product_id, si);
			}

//			if (getenv("VALHALL_DUMP"))
				pandecode_dump_mappings(si);
                        job_shader_info_free(si);
                } else {
			printf("[WRAP] bad atom stride, got %u\n", js->stride);
		}
	}
#endif

	/* Track memory allocation if needed  */
	if ((IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_MEM_ALLOC)) && track_alloc) {
		const union mali_ioctl_mem_alloc *args = ptr;

#ifdef dvalin
		mali_ptr va = args->out.gpu_va;
		u64 flags = args->out.flags;
#else
		mali_ptr va = args->inout.gpu_va;
		u64 flags = args->inout.flags;
#endif

//                if (va != 0x41000)
                        panwrap_track_allocation(va, flags, allocation_number++, alloc_va_pages * 4096);
	}

#ifndef dvalin
	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_GPU_PROPS_REG_DUMP)) {
		const struct mali_ioctl_gpu_props_reg_dump *dump = ptr;
		product_id = dump->core.product_id;
		/* based on mali_kbase_gpu_id.h and mali_kbase_hw.c */
		if (product_id >> 12 == 6 && product_id != 0x6956 /* T60x */)
			bifrost = true;
	}
#else
       if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_GET_GPUPROPS)) {
               const struct mali_ioctl_get_gpuprops *dump = ptr;

               bool data = dump && dump->size;

               u32 *buf = dump ? (u32 *)(uintptr_t)dump->buffer : NULL;
               u8 *buf8 = (u8 *)buf;

               if (fake > 1) {
                       ret = sizeof(props_g72);

                       if (data) {
                               assert(dump->size >= ret);

                               memcpy(buf, props_g72, ret);

#ifdef PANWRAP_CSF
                               buf8[4] = 0x02;
                               buf8[5] = 0xa0;
#else
                               buf8[4] = 0x91;
                               buf8[5] = 0x90;
#endif
                       }
               }

               if (false && data) {
                       for (int i = 0; i < dump->size; ++i)
                               printf("0x%x, ", buf8[i]);
                       printf("\n");

                       int i = 0;
                       u64 x = 0;
                       while (i < dump->size) {
                               //endian assumptions...
                               x = 0;
                               memcpy(&x, buf8 + i, 4);
                               i += 4;
                               printf("%"PRIx64" ", x >> 2);
                               int size;
                               switch (x & 3) {
                               case 0: printf("u8 "); size = 1; break;
                               case 1: printf("u16 "); size = 2; break;
                               case 2: printf("u32 "); size = 4; break;
                               case 3: printf("u64 "); size = 8; break;
                               }
                               x = 0;
                               memcpy(&x, buf8 + i, size);
                               i += size;
                               printf("%"PRIx64"\n", x);
                       }
                       printf("\n");
               }

               if (data) {
                       product_id = buf[1];
                       fprintf(stderr, "panwrap: Detected GPU ID %x\n", product_id);
                       /* based on mali_kbase_gpu_id.h and mali_kbase_hw.c */
		       if (product_id >> 12 == 6 && product_id != 0x6956 /* T60x */)
                               bifrost = true;
                       if (product_id >> 12 == 7)
                               bifrost = true;
                }
       }
#endif

       if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_GET_VERSION)) {
	       const struct mali_ioctl_get_version *dump = ptr;
	       fprintf(stderr, "panwrap: Detected kbase version %d.%d\n", dump->major, dump->minor);
       }


       if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_JOB_SUBMIT)) {
           if (getenv("VALHALL_POST_DUMP"))
               pandecode_dump_mappings(NULL);
       }
	

	UNLOCK();
	return ret;
}

static inline void *panwrap_mmap_wrap(mmap_func *func,
				      void *addr, size_t length, int prot,
				      int flags, int fd, loff_t offset)
{
	void *ret = NULL;

	if (!mali_fd || fd != mali_fd)
		return func(addr, length, prot, flags, fd, offset);

        static int mmap_count = 0;
        printf("mmap %i: %"PRIxPTR" | %i %i\n", ++mmap_count, length, fd, mali_fd);

        if (mem_mod) {
                length = mem_mod * 4096;
                mem_mod = 0;
        }

	LOCK();
        if (fake >= 3) {
//                ret = panblob_cpu_addr(pan_dev, offset);
                if (!ret) {
                        printf("could not find BO for %"PRIx64"\n", offset);
                        ret = (void*)(uintptr_t)offset;
                }
        } else if (fake == 2) {
                ret = (void*)(uintptr_t)offset;
        } else {
                ret = func(addr, length, prot, flags, fd, offset);
        }

	switch (offset) { /* offset == gpu_va */
	case MALI_MEM_MAP_TRACKING_HANDLE:
                printf("MALI_MEM_MAP_TRACKING_HANDLE\n");
		break;
	default:
		panwrap_track_mmap(offset, ret, length, prot, flags);
		break;
	}


        printf("mmap returning %p\n", ret);

	UNLOCK();
	return ret;
}

void *mmap64(void *addr, size_t length, int prot, int flags, int fd,
	     loff_t offset)
{
	PROLOG(mmap64);

	return panwrap_mmap_wrap(orig_mmap64, addr, length, prot, flags, fd,
				 offset);
}

//#ifdef IS_MMAP64_SEPERATE_SYMBOL
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
{
#ifdef __LP64__
	PROLOG(mmap);

	return panwrap_mmap_wrap(orig_mmap, addr, length, prot, flags, fd,
				 offset);
#else
	return mmap64(addr, length, prot, flags, fd, (loff_t) offset);
#endif
}

int munmap(void *addr, size_t len)
{
	/* Stub */
        return 0;
}

void
panwrap_alloc_mem(unsigned num_pages)
{
        if (!mali_fd)
                return;

        union mali_ioctl_mem_alloc alloc = {
                .in = {
                        .va_pages = num_pages,
                        .commit_pages = num_pages,
                        .flags = MALI_MEM_PROT_CPU_RD | MALI_MEM_PROT_CPU_WR
                               | MALI_MEM_PROT_GPU_RD | MALI_MEM_PROT_GPU_WR
                               | MALI_MEM_SAME_VA,
                },
        };
        ioctl(mali_fd, MALI_IOCTL_MEM_ALLOC, &alloc);
        mmap(NULL, num_pages * 4096, PROT_READ | PROT_WRITE, MAP_SHARED, mali_fd, 0x41000);
}


void
panwrap_alloc_mod(unsigned num_pages)
{
        mem_mod = num_pages;
}
//#endif
