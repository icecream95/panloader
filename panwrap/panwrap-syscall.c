/*
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <linux/ioctl.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
#include <linux/limits.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>

#include <mali-ioctl.h>
#include <mali-props.h>
#include <list.h>
#include "panwrap.h"
#include "wrap.h"

static pthread_mutex_t l;
static void __attribute__((constructor)) panloader_constructor(void) {
	pthread_mutexattr_t mattr;

	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&l, &mattr);
	pthread_mutexattr_destroy(&mattr);
}

#define IOCTL_CASE(request) (_IO(_IOC_TYPE(request), _IOC_NR(request)))

struct ioctl_info {
	const char *name;
};

struct device_info {
	const char *name;
	const struct ioctl_info info[MALI_IOCTL_TYPE_COUNT][_IOC_NR(0xffffffff)];
};

typedef void* (mmap_func)(void *, size_t, int, int, int, loff_t);
typedef int (open_func)(const char *, int flags, ...);

#define IOCTL_TYPE(type) [type - MALI_IOCTL_TYPE_BASE] =
#define IOCTL_INFO(n) [_IOC_NR(MALI_IOCTL_##n)] = { .name = #n }
static struct device_info mali_info = {
	.name = "mali",
	.info = {
		IOCTL_TYPE(0x80) {
			IOCTL_INFO(GET_VERSION),
#ifndef dvalin
		},
		IOCTL_TYPE(0x82) {
#endif
			IOCTL_INFO(MEM_ALLOC),
			IOCTL_INFO(MEM_IMPORT),
			IOCTL_INFO(MEM_COMMIT),
			IOCTL_INFO(MEM_QUERY),
			IOCTL_INFO(MEM_FREE),
			IOCTL_INFO(MEM_FLAGS_CHANGE),
			IOCTL_INFO(MEM_ALIAS),
			IOCTL_INFO(SYNC),
#ifndef dvalin
			IOCTL_INFO(GPU_PROPS_REG_DUMP),
			IOCTL_INFO(GET_VERSION_NEW),
#endif
			IOCTL_INFO(JOB_SUBMIT),
		},
	},
};
#undef IOCTL_INFO
#undef IOCTL_TYPE

static inline const struct ioctl_info *
ioctl_get_info(unsigned long int request)
{
	return &mali_info.info[_IOC_TYPE(request) - MALI_IOCTL_TYPE_BASE]
	                      [_IOC_NR(request)];
}

static int mali_fd = 0;

extern pthread_mutex_t user_list_lock;

#define LOCK()   pthread_mutex_lock(&l);
#define UNLOCK() pthread_mutex_unlock(&l)

/**
 * Overriden libc functions start here
 */
static inline int
panwrap_open_wrap(open_func *func, const char *path, int flags, va_list args)
{
	mode_t mode = 0;
	int ret;

	if (flags & O_CREAT) {
		mode = (mode_t) va_arg(args, int);
		ret = func(path, flags, mode);
	} else {
		ret = func(path, flags);
	}

	LOCK();
	if (ret != -1 && strcmp(path, "/dev/mali0") == 0)
		mali_fd = ret;
	UNLOCK();

	return ret;
}

//#ifdef IS_OPEN64_SEPERATE_SYMBOL
int
open(const char *path, int flags, ...)
{
	PROLOG(open);
	va_list args;
	va_start(args, flags);
	int o = panwrap_open_wrap(orig_open, path, flags, args);
	va_end(args);
	return o;
}
//#endif

int
open64(const char *path, int flags, ...)
{
	PROLOG(open64);
	va_list args;
	va_start(args, flags);
	int o = panwrap_open_wrap(orig_open64, path, flags, args);
	va_end(args);
	return o;
}

int
close(int fd)
{
	PROLOG(close);

        /* Intentionally racy: prevents us from trying to hold the global mutex
         * in calls from system libraries */
        if (fd <= 0 || !mali_fd || fd != mali_fd)
                return orig_close(fd);

	LOCK();
	if (!fd || fd != mali_fd) {
		mali_fd = 0;
	}
	UNLOCK();

	return orig_close(fd);
}

/* HW version */
static unsigned product_id;
int allocation_number = 0;

static mali_ptr registered_cs_va;
static unsigned registered_cs_size;

/* XXX: Android has a messed up ioctl signature */
int ioctl(int fd, int _request, ...)
{
	PROLOG(ioctl);
	unsigned long int request = _request;
	int ioc_size = _IOC_SIZE(request);
	int ret;
	void *ptr;

#ifdef dvalin
	if (request & 0x0F00)
		ioc_size = 1;
#endif

	if (ioc_size) {
		va_list args;

		va_start(args, _request);
		ptr = va_arg(args, void *);
		va_end(args);
	} else {
		ptr = NULL;
	}

	if (!fd || fd != mali_fd)
		return orig_ioctl(fd, request, ptr);

	LOCK();

	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_JOB_SUBMIT)) {
		const struct mali_ioctl_job_submit *js = ptr;
		struct mali_jd_atom_v2 *atoms = js->addr;

		for (unsigned i = 0; i < js->nr_atoms; ++i)
                        pandecode_jc(atoms[i].jc, product_id);
	}

	mali_ptr alloc_va_pages = 0;

	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_MEM_ALLOC)) {
		const union mali_ioctl_mem_alloc *args = ptr;

#ifdef dvalin
		alloc_va_pages = args->in.va_pages;
#else
		alloc_va_pages = args->inout.va_pages;
#endif
	}

#ifdef dvalin
        if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_CS_QUEUE_REGISTER)) {
                const struct mali_ioctl_cs_queue_register *args = ptr;

                /* Record CS information for a future user IO mapping */
                registered_cs_va = args->buffer_gpu_addr;
                registered_cs_size = args->buffer_size;
        }
#endif

        union mali_ioctl_cs_tiler_heap_init heap_init;

        if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_CS_TILER_HEAP_INIT)) {
                const union mali_ioctl_cs_tiler_heap_init *args = ptr;

                heap_init = *args;
        }

        ret = orig_ioctl(fd, request, ptr);

	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_CS_TILER_HEAP_INIT)) {
		const union mali_ioctl_cs_tiler_heap_init *args = ptr;

		fprintf(stderr, "va %lx chunk %lx size %i\n",
			args->out.gpu_heap_va,
			args->out.first_chunk_va,
                        heap_init.in.chunk_size);

                PROLOG(mmap);
                void *cpu = orig_mmap(NULL, 4096, PROT_WRITE,
                                      MAP_SHARED, fd, args->out.gpu_heap_va);

                pandecode_inject_mmap(args->out.gpu_heap_va, cpu,
                                      heap_init.in.chunk_size, "tiler_heap");

                cpu = orig_mmap(NULL, heap_init.in.chunk_size, PROT_READ | PROT_WRITE,
                                MAP_SHARED, fd, args->out.first_chunk_va);

                pandecode_inject_mmap(args->out.first_chunk_va, cpu,
                                      heap_init.in.chunk_size, "heap_chunk");
	}


	/* Track memory allocation if needed  */
	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_MEM_ALLOC)) {
		const union mali_ioctl_mem_alloc *args = ptr;

#ifdef dvalin
		mali_ptr va = args->out.gpu_va;
		u64 flags = args->out.flags;
#else
		mali_ptr va = args->inout.gpu_va;
		u64 flags = args->inout.flags;
#endif

		panwrap_track_allocation(va, flags, allocation_number++, alloc_va_pages * 4096);
	}

#ifndef dvalin
	if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_GPU_PROPS_REG_DUMP)) {
		const struct mali_ioctl_gpu_props_reg_dump *dump = ptr;
		product_id = dump->core.product_id;
	}
#else
       if (IOCTL_CASE(request) == IOCTL_CASE(MALI_IOCTL_GET_GPUPROPS)) {
               const struct mali_ioctl_get_gpuprops *dump = ptr;
               if (dump && dump->size) {
                       u32 *buf = (u32 *)dump->buffer;
                       product_id = buf[1];
                       fprintf(stderr, "panwrap: Detected GPU ID %x\n", product_id);

                       /* watch user pages for CSF GPUs, there are no submit
                        * ioctls */
                       if (product_id >> 12 >= 10)
                               panwrap_user_page_init();
                }
       }
#endif

	UNLOCK();
	return ret;
}

void panwrap_kick_ioctl(mali_ptr va)
{
        struct mali_ioctl_cs_queue_kick kick = {
                .buffer_gpu_addr = registered_cs_va,
        };

	PROLOG(ioctl);
        orig_ioctl(mali_fd, MALI_IOCTL_CS_QUEUE_KICK, &kick);
}

void panwrap_event_ioctl(void)
{
	PROLOG(ioctl);
        orig_ioctl(mali_fd, MALI_IOCTL_CS_EVENT_SIGNAL);
}

static inline void *panwrap_mmap_wrap(mmap_func *func,
				      void *addr, size_t length, int prot,
				      int flags, int fd, loff_t offset)
{
	void *ret;

	if (!mali_fd || fd != mali_fd)
		return func(addr, length, prot, flags, fd, offset);

	LOCK();
	ret = func(addr, length, prot, flags, fd, offset);

	switch (offset) { /* offset == gpu_va */
	case MALI_MEM_MAP_TRACKING_HANDLE:
		break;
        case MALI_MEM_CSF_USER_IO_PAGES_HANDLE ...
             MALI_MEM_COOKIE_BASE - 1: {
                if (length != 4096 * 3)
                        fprintf(stderr, "Incorrect length 0x%zx for "
                                "CSF user io pages\n", length);

                ret = panwrap_proxy_user_page(ret,
                                              registered_cs_va,
                                              registered_cs_size,
                                              product_id);

                panwrap_disable_unmap(ret);

                /* The blob does a kick ioctl itself but for some
                 * reason this one is needed as well. */
/*                struct mali_ioctl_cs_queue_kick kick = {
                    .buffer_gpu_addr = registered_cs_va,
                };

                ioctl(mali_fd, MALI_IOCTL_CS_QUEUE_KICK, &kick);
*/
                break;
        }
	default:
		panwrap_track_mmap(offset, ret, length, prot, flags);
		break;
	}

	UNLOCK();
	return ret;
}

void *mmap64(void *addr, size_t length, int prot, int flags, int fd,
	     loff_t offset)
{
	PROLOG(mmap64);

	return panwrap_mmap_wrap(orig_mmap64, addr, length, prot, flags, fd,
				 offset);
}

//#ifdef IS_MMAP64_SEPERATE_SYMBOL
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
{
#ifdef __LP64__
	PROLOG(mmap);

	return panwrap_mmap_wrap(orig_mmap, addr, length, prot, flags, fd,
				 offset);
#else
	return mmap64(addr, length, prot, flags, fd, (loff_t) offset);
#endif
}

int munmap(void *addr, size_t length)
{
        PROLOG(munmap);

	pthread_mutex_lock(&user_list_lock);
	pthread_mutex_unlock(&user_list_lock);

        if (panwrap_allow_unmap(addr))
                return orig_munmap(addr, length);
        else
                return 0;
}
//#endif
