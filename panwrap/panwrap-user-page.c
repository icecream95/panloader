/*
 * © Copyright 2022 Icecream95
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <list.h>

#include "panwrap.h"
#include "wrap.h"

extern bool force_stderr;
extern FILE *pandecode_dump_stream;

/* Proxy memory accesses every 10 millisecnods */

struct user_page {
        struct list list;

        volatile uint32_t *proxy_addr;
        volatile uint32_t *real_addr;
        unsigned decode_offset;
        unsigned old_extract;

        mali_ptr cs_va;
        unsigned cs_size;

        unsigned gpu_id;

        unsigned cs_id;
};

static LIST_HEAD(user_pages);
static unsigned cs_id;
pthread_mutex_t user_list_lock;

/* Offsets are in 32-bit units */
#define DOORBELL_OFFSET 0
#define INPUT_OFFSET 1024
#define OUTPUT_OFFSET 1024 * 2

static void
panwrap_dump_mappings(unsigned queue, bool after)
{
        if (!getenv("PAN_DUMP"))
                return;

        mkdir("/tmp/d", 0777);
        char buf[128] = {0};
        sprintf(buf, "/tmp/d/dump-%i.%i", queue, after);
        setenv("PANDECODE_DUMP_FILE", buf, 1);

        force_stderr = false;
        pandecode_dump_stream = NULL;
        pandecode_dump_mappings();
        force_stderr = true;
        pandecode_dump_file_close();
}

void panwrap_user_page_iter(void)
{
        pthread_mutex_lock(&user_list_lock);

#ifdef __ARM_ARCH
        /* Make sure that we see the latest writes to the proxy
         * pages... I'm not convinced this is necessary because we use
         * atomics loads, but this is here just in case. */
        asm volatile ("dmb ish" ::: "memory");
#endif

        bool decoded = false;

        struct user_page *u;
        list_for_each_entry_rev(u, &user_pages, list) {

                /* Start by proxying the output page back. Don't worry
                 * about high words, who would need a >4GB ring buffer
                 * anyway? */

                volatile uint32_t *output_read = u->real_addr + OUTPUT_OFFSET;
                volatile uint32_t *output_write = u->proxy_addr + OUTPUT_OFFSET;

                uint32_t extract = output_read[0];

                /* CS_ACTIVE */
                atomic_store(output_write + 2, output_read[2]);
                /* CS_EXTRACT_LO */
                atomic_store(output_write, extract);

                if (extract != u->old_extract) {
                        //panwrap_event_ioctl();
                        u->old_extract = extract;
                }

                volatile uint32_t *doorbell_write = u->real_addr + DOORBELL_OFFSET;

                /* CS_EXTRACT_INIT seems to be touched by the kernel,
                 * so only CS_INSERT is copied to the real page.
                 * But first we decode command streams. */

                volatile uint32_t *input_read = u->proxy_addr + INPUT_OFFSET;
                volatile uint32_t *input_write = u->real_addr + INPUT_OFFSET;

                /* CS_EXTRACT_INIT_LO */
                unsigned extract_init = atomic_load(input_read + 2);
                if (extract_init)
                        fprintf(stderr, "Extract offset is %i, expected zero\n", extract_init);

                /* CS_INSERT_LO */
                unsigned insert = atomic_load(input_read);

                unsigned doorbell = (insert == u->decode_offset) ? 0 : 1;

                //printf("%p insert %i doorbell %i extract %i\n", u, insert, doorbell, extract);

                /* Assumes that the doorbell is always used instead of the
                 * kick ioctl */
                if (!doorbell)
                        continue;

#ifdef __ARM_ARCH
                /* Make sure that we see all writes from userspace. */
                asm volatile ("dmb ish" ::: "memory");
#endif

                unsigned old_offset = u->decode_offset;

                if (!decoded)
                        pandecode_next_frame();

                panwrap_dump_mappings(u->cs_id, false);

                /* If there was wrapping, we first have to decode up
                 * to the end of the ring buffer. Note that register
                 * contents are lost... I don't know if it's okay to
                 * do that or not */

                if (insert != u->decode_offset)
                        fprintf(stderr, "\nQUEUE %i\n", u->cs_id);

                if (insert < u->decode_offset) {
                        pandecode_cs(u->cs_va + u->decode_offset,
                                     u->cs_size - u->decode_offset,
                                     u->gpu_id);
                        u->decode_offset = 0;
                }

                if (insert != u->decode_offset) {
                        pandecode_cs(u->cs_va + u->decode_offset,
                                     insert - u->decode_offset,
                                     u->gpu_id);
                        u->decode_offset = insert;
                }

                /* Decoding has finished, so it's safe to let the GPU
                 * have a go at the command stream */
                input_write[0] = insert;

#ifdef __ARM_ARCH
                asm volatile ("dmb sy" ::: "memory");
#endif

                doorbell_write[0] = doorbell;

#ifdef __ARM_ARCH
                asm volatile ("dmb sy" ::: "memory");
#endif

                panwrap_kick_ioctl(u->cs_va);

                if (!getenv("PAN_WAIT"))
                        continue;

                sleep(1);

#ifdef __ARM_ARCH
                asm volatile ("dmb sy" ::: "memory");
#endif

                panwrap_dump_mappings(u->cs_id, true);

                /* TODO: Fix this for if wrapping occurs */
                fprintf(stderr, "\x1b[90m");
                pandecode_cs(u->cs_va + old_offset,
                             insert - old_offset,
                             u->gpu_id);
                fprintf(stderr, "\x1b[39m");
        }

        pthread_mutex_unlock(&user_list_lock);
}

static void *
panwrap_user_page_thread(void *data)
{
        for (;;) {
                usleep(100 * 1000);
                panwrap_user_page_iter();
        }

        return NULL;
}

void
panwrap_user_page_init(void)
{
        pthread_mutex_init(&user_list_lock, NULL);

        pthread_t thr;
        pthread_create(&thr, NULL,
                       panwrap_user_page_thread,
                       NULL);
}

void *
panwrap_proxy_user_page(void *ptr, mali_ptr cs_va, unsigned cs_size,
                        unsigned gpu_id)
{
        if (ptr == MAP_FAILED)
                return MAP_FAILED;

        void *proxy = mmap(NULL, 4096 * 3, PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

        if (proxy == MAP_FAILED)
                return MAP_FAILED;

        struct user_page *u = calloc(1, sizeof(*u));

        u->real_addr = ptr;
        u->proxy_addr = proxy;
        u->decode_offset = 0;
        u->cs_va = cs_va;
        u->cs_size = cs_size;
        u->gpu_id = gpu_id;

        pthread_mutex_lock(&user_list_lock);
        u->cs_id = cs_id++;
        list_add(&u->list, &user_pages);
        pthread_mutex_unlock(&user_list_lock);

        return proxy;
}
