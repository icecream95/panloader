/*
 * © Copyright 2022 Icecream95
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef PANWRAP_USER_PAGE_H
#define PANWRAP_USER_PAGE_H

void
panwrap_user_page_init(void);

void
panwrap_user_page_iter(void);

void *
panwrap_proxy_user_page(void *ptr, mali_ptr cs_va, unsigned cs_size,
                        unsigned gpu_id);

#endif
