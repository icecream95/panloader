/*
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdarg.h>
#include <list.h>
#include <memory.h>

#include <mali-ioctl.h>
#include <panloader-util.h>
#include "panwrap.h"
#include "panwrap-mmap.h"
#ifdef HAVE_LINUX_MMAN_H
#include <linux/mman.h>
#endif

#include "wrap.h"

static LIST_HEAD(allocations);
static LIST_HEAD(no_unmap);

void panwrap_track_allocation(mali_ptr addr, int flags, int number, size_t length)
{
	struct panwrap_allocated_memory *mem = malloc(sizeof(*mem));

	list_init(&mem->node);
	mem->gpu_va = addr;
	mem->flags = flags;
	mem->allocation_number = number;
	mem->length = length;

	list_add(&mem->node, &allocations);

	/* XXX: Hacky workaround for cz's board */
	if (mem->gpu_va >> 28 == 0xb)
		panwrap_track_mmap(addr, (void *) (uintptr_t) addr, length, PROT_READ | PROT_WRITE, MAP_SHARED);
        else
                pandecode_inject_mmap(addr, NULL, length, NULL);
}

void panwrap_track_mmap(mali_ptr gpu_va, void *addr, size_t length,
			int prot, int flags)
{
	struct panwrap_allocated_memory *pos, *mem = NULL;

	/* Find the pending unmapped allocation for the memory */
	list_for_each_entry(pos, &allocations, node) {
		if (pos->gpu_va == gpu_va) {
			mem = pos;
			break;
		}
	}
	if (!mem) {
		printf("// Untracked...\n");
		return;
	}

        if (mem->flags & MALI_MEM_PROT_GPU_EX)
                panwrap_disable_unmap(addr);

	/* Try not to break other systems... there are so many configurations
	 * of userspaces/kernels/architectures and none of them are compatible,
	 * ugh. */

#define MEM_COOKIE_VA 0x41000

	if (mem->flags & MALI_MEM_SAME_VA && gpu_va == MEM_COOKIE_VA)
		gpu_va = (mali_ptr) (uintptr_t) addr;

	//printf("allocate va %lx - %lx flags 0x%x\n", gpu_va, gpu_va + length, mem->flags);

	/* Generate somewhat semantic name for the region */
	char name[512];

	snprintf(name, sizeof(name) -1,
			"%s_%d",
			mem->flags & MALI_MEM_PROT_GPU_EX ? "shader" : "memory",
			mem->allocation_number);

	pandecode_inject_mmap(gpu_va, addr, length, name);

	list_del(&mem->node);
	free(mem);
}

void panwrap_disable_unmap(void *addr)
{
        struct panwrap_addr_entry *a = malloc(sizeof(*a));
        a->addr = addr;
        list_init(&a->node);

        list_add(&a->node, &no_unmap);
}

bool panwrap_allow_unmap(void *addr)
{
        struct panwrap_addr_entry *pos;

        list_for_each_entry(pos, &no_unmap, node) {
                if (pos->addr == addr)
                        return false;
        }

        return true;
}
