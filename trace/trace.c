#define _GNU_SOURCE /* strchrnul, memrchr */

#include <fcntl.h>
#include <inttypes.h>
#include <poll.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

void
write_file(const char *filename, const char *text)
{
        int fd = open(filename, O_WRONLY | O_TRUNC);
        if (fd == -1) {
                fprintf(stderr, "open(\"%s\"): %m\n", filename);
                return;
        }

        size_t len = strlen(text);
        ssize_t ret = write(fd, text, len);
        if (ret == -1) {
                fprintf(stderr, "write(\"%s\"): %m\n", filename);
                return;
        }

        ret = close(fd);
        if (ret == -1) {
                fprintf(stderr, "close(\"%s\"): %m\n", filename);
                return;
        }
}

int
xopen(const char *filename, int flags)
{
        int ret = open(filename, flags);
        if (ret == -1) {
                fprintf(stderr, "open(\"%s\"): %m\n", filename);
                abort();
        }

        return ret;
}

void
setup_mali_fd(int mali_fd)
{
        int ret;

        uint32_t ver = 0;
        ret = ioctl(mali_fd, _IOWR(0x80, 52, uint32_t), &ver);
        if (ret == -1) {
                fprintf(stderr, "ioctl(VERSION_CHECK): %m\n");
                abort();
        }

        uint32_t create_flags = 0;
        ret = ioctl(mali_fd, _IOW(0x80, 1, uint32_t), &create_flags);
        if (ret == -1) {
                fprintf(stderr, "ioctl(SET_FLAGS): %m\n");
                abort();
        }
}

/* Enable additional tracepoints for latency measurements (TL_ATOM_READY,
 * TL_ATOM_DONE, TL_ATOM_PRIO_CHANGE, TL_ATOM_EVENT_POST)
 */
#define BASE_TLSTREAM_ENABLE_LATENCY_TRACEPOINTS (1 << 0)

/* Indicate that job dumping is enabled. This could affect certain timers
 * to account for the performance impact.
 */
#define BASE_TLSTREAM_JOB_DUMPING_ENABLED (1 << 1)

/* Enable KBase tracepoints for CSF builds */
#define BASE_TLSTREAM_ENABLE_CSF_TRACEPOINTS (1 << 2)

/* Enable additional CSF Firmware side tracepoints */
#define BASE_TLSTREAM_ENABLE_CSFFW_TRACEPOINTS (1 << 3)

int
get_trace_fd(int mali_fd)
{
        uint32_t flags = BASE_TLSTREAM_ENABLE_CSFFW_TRACEPOINTS;

        int ret = ioctl(mali_fd, _IOW(0x80, 18, uint32_t), &flags);
        if (ret == -1) {
                fprintf(stderr, "ioctl(TLSTREAM_ACQUIRE): %m\n");
                abort();
        }
        return ret;
}


struct reader {
        void *data;
        void *end;
};

static uint64_t
read_u64(struct reader *r)
{
        uint64_t result;
        memcpy(&result, r->data, 8);
        r->data += 8;
        return result;
}

static uint32_t
read_u32(struct reader *r)
{
        uint32_t result;
        memcpy(&result, r->data, 4);
        r->data += 4;
        return result;
}

static uint32_t
read_u8(struct reader *r)
{
        uint8_t result;
        memcpy(&result, r->data, 1);
        r->data += 1;
        return result;
}

static struct reader
read_sub(struct reader *r, uint32_t len)
{
        struct reader ret = {
                .data = r->data,
                .end = r->data + len,
        };
        r->data += len;
        return ret;
}

static char *
read_str(struct reader *r, uint32_t len)
{
        char *ret = r->data;
        r->data += len;
        return ret;
}

static char *
read_sized_str(struct reader *r)
{
        uint32_t len = read_u32(r);
        return read_str(r, len);
}

static bool
read_eof(struct reader *r)
{
        if (r->data > r->end)
                fprintf(stderr, "reader overflow!\n");
        return r->data >= r->end;
}

struct enumeration {
        unsigned num_values;
        const char **value_names;
};

struct tracepoint {
        const char *name;
        const char *desc;
        bool timestamp;
        unsigned num_args;
        char *arg_types;
        const char **arg_names;
        struct enumeration **enums;
};

struct tp_set {
        unsigned tp_count;
        struct tracepoint *tracepoints;
};

static void
add_enum(struct tracepoint *tracepoints, unsigned tp_count,
         const char *enum_name, struct enumeration *e)
{
        for (unsigned i = 0; i < tp_count; ++i) {
                struct tracepoint *tp = &tracepoints[i];

                for (unsigned arg = 0; arg < tp->num_args; ++arg) {
                        const char *name = tp->arg_names[arg];
                        if (name && !strcmp(name, enum_name))
                                tp->enums[arg] = e;
                }
        }
}

static void
set_tracepoint_name(struct tracepoint *tp,
                    unsigned arg,
                    char *name)
{
        tp->arg_names[arg] = name;

        const char *no_hex[] = {
                "as_nr",
                "atom_nr",
                "bin_id",
                "chunk_count",
                "core_count",
                "cs",
                "csffw_cycle",
                "csffw_timestamp",
                "csg",
                "cshwif",
                "ctx_nr",
                "cycle_count",
                "dummy",
                "gpu_cmdq_grp_handle",
                "heap_id",
                "heap_id",
                "j_id",
                "jit_alloc_bin_id",
                "jit_alloc_jit_id",
                "jit_alloc_max_allocations",
                "jit_alloc_usage_id",
                "kbase_device_as_index",
                "kbase_device_csg_slot_index",
                "kcpu_queue_id",
                "kcpuq_num_pending_cmds",
                "kernel_ctx_id",
                "lpu_nr",
                "max_allocs",
                "max_chunks",
                "mmu_cmd_id",
                "nr_in_flight",
                "prio",
                "slot_nr",
                "state",
                "target_in_flight",
                "tgid",
                "usg_id",
        };

        unsigned num_no_hex = sizeof(no_hex) / sizeof(*no_hex);

        /* Make lowercase to not print as hex */
        for (unsigned i = 0; i < num_no_hex; ++i)
                if (!strcmp(no_hex[i], name))
                        tp->arg_types[arg] |= 32;
}

static void
dump_timeline_header(struct reader *r, unsigned length,
                     struct tp_set *tps, bool verbose)
{
        void *end = r->data + length;

        uint8_t protocol_version = read_u8(r);
        uint8_t pointer_size = read_u8(r);
        uint32_t tp_count = read_u32(r);

        if (verbose)
                printf("tracepoints ver %i, pointers are %i bytes:\n",
                       protocol_version, pointer_size);

        struct tracepoint *tracepoints =
                calloc(tp_count, sizeof(*tracepoints));

        if (tps) {
                *tps = (struct tp_set) {
                        .tp_count = tp_count,
                        .tracepoints = tracepoints,
                };
        }

        for (unsigned i = 0; i < tp_count; ++i) {
                uint32_t name = read_u32(r);
                char *string_name = read_sized_str(r);
                char *desc = read_sized_str(r);
                char *arg_types = read_sized_str(r);
                char *arg_names = read_sized_str(r);

                if (verbose)
                        printf("  name %i s '%s' d '%s' a '%s' n '%s'\n",
                               name, string_name, desc, arg_types, arg_names);

                bool timestamp = (arg_types[0] == '@');
                if (timestamp)
                        ++arg_types;

                unsigned num_args = strlen(arg_types);

                tracepoints[i] = (struct tracepoint) {
                        .name = strdup(string_name),
                        .desc = strdup(desc),
                        .timestamp = timestamp,
                        .num_args = num_args,
                        .arg_types = strdup(arg_types),
                        .arg_names = calloc(num_args, sizeof(const char *)),
                        .enums = calloc(num_args, sizeof(struct enumeration *)),
                };

                char *names = arg_names;
                for (unsigned arg = 0; arg < num_args; ++arg) {
                        char *next = strchrnul(names, ',');
                        if (next != names)
                                set_tracepoint_name(&tracepoints[i],
                                            arg, strndup(names, next - names));
                        if (*next == ',')
                                names = next + 1;
                        else
                                break;
                }
        }

        struct {
                const char *from;
                const char *to;
                struct enumeration *e;
        } enums[] = {
                { .from = "iter_type", .to = "iterator_type" },
                { .from = "tl_fw_state", .to = "fw_state" },
                { .from = "csi_state", .to = "cs_state" },
                { .from = "tl_csg_internal_state", .to = "csg_state" },
                { .from = "cshwif_state", .to = "cshwif_state" },
                { .from = "iter_status", .to = "iterator_state" },
        };

        unsigned num_enums = sizeof(enums) / sizeof(enums[0]);

        while (r->data < end) {
                uint32_t msg_id = read_u32(r);
                const char *arg_name = read_sized_str(r);
                uint32_t value = read_u32(r);
                char *value_str = read_sized_str(r);

                unsigned r;
                for (r = 0; r < num_enums; ++r)
                        if (!strcmp(arg_name, enums[r].from))
                                break;

                if (r < num_enums)
                        arg_name = enums[r].to;

                if (verbose)
                        printf("  enum %i s '%s' v %i n '%s'\n",
                               msg_id, arg_name, value, value_str);

                if (r == num_enums) {
                        fprintf(stderr, "unknown enum, skipping\n");
                        continue;
                }

                if (!enums[r].e) {
                        enums[r].e = calloc(1, sizeof(struct enumeration));

                        add_enum(tracepoints, tp_count, arg_name, enums[r].e);
                }

                struct enumeration *e = enums[r].e;

                if (value >= e->num_values) {
                        e->value_names =
                                reallocarray(e->value_names, value + 1,
                                             sizeof(const char *));
                        memset(&e->value_names[e->num_values],
                               0, sizeof(const char *) *
                               (value + 1 - e->num_values));
                        e->num_values = value + 1;
                }

                e->value_names[value] = strdup(value_str);
        }

        if (r->data != end)
                fprintf(stderr, "overflow reading timeline header\n");
}

/* Packet header - first word.
 * These values must be defined according to MIPE documentation.
 */
#define PACKET_STREAMID_POS  0
#define PACKET_STREAMID_LEN  8
#define PACKET_RSVD1_POS     (PACKET_STREAMID_POS + PACKET_STREAMID_LEN)
#define PACKET_RSVD1_LEN     8
#define PACKET_TYPE_POS      (PACKET_RSVD1_POS + PACKET_RSVD1_LEN)
#define PACKET_TYPE_LEN      3
#define PACKET_CLASS_POS     (PACKET_TYPE_POS + PACKET_TYPE_LEN)
#define PACKET_CLASS_LEN     7
#define PACKET_FAMILY_POS    (PACKET_CLASS_POS + PACKET_CLASS_LEN)
#define PACKET_FAMILY_LEN    6

/* Packet header - second word
 * These values must be defined according to MIPE documentation.
 */
#define PACKET_LENGTH_POS    0
#define PACKET_LENGTH_LEN    24
#define PACKET_SEQBIT_POS    (PACKET_LENGTH_POS + PACKET_LENGTH_LEN)
#define PACKET_SEQBIT_LEN    1
#define PACKET_RSVD2_POS     (PACKET_SEQBIT_POS + PACKET_SEQBIT_LEN)
#define PACKET_RSVD2_LEN     7

#define FLD2VAL(field, value) ((((uint32_t)(value)) >> field ## _POS) & ((1 << field ## _LEN) - 1))

/* Timeline packet family ids.
 * Values are significant! Check MIPE documentation.
 */
enum tl_packet_family {
	TL_PACKET_FAMILY_CTRL = 0, /* control packets */
	TL_PACKET_FAMILY_TL = 1,   /* timeline packets */
	TL_PACKET_FAMILY_COUNT
};

/* Packet classes used in timeline streams.
 * Values are significant! Check MIPE documentation.
 */
enum tl_packet_class {
	TL_PACKET_CLASS_OBJ = 0, /* timeline objects packet */
	TL_PACKET_CLASS_AUX = 1, /* auxiliary events packet */
};

/* Packet types used in timeline streams.
 * Values are significant! Check MIPE documentation.
 */
enum tl_packet_type {
	TL_PACKET_TYPE_HEADER = 0,  /* stream's header/directory */
	TL_PACKET_TYPE_BODY = 1,    /* stream's body */
	TL_PACKET_TYPE_SUMMARY = 2, /* stream's summary */
};

/* Stream ID types (timeline family). */
enum tl_stream_id {
	TL_STREAM_ID_USER = 0, /* User-space driver Timeline stream. */
	TL_STREAM_ID_KERNEL = 1, /* Kernel-space driver Timeline stream. */
	TL_STREAM_ID_CSFFW = 2, /* CSF firmware driver Timeline stream. */
};

struct stream {
        struct tp_set tps;
};

struct timeline_ctx {
        struct stream normal, aux, csf;
        FILE *fp;
        bool verbose;
};

static void
dump_timeline_data(struct timeline_ctx *ctx, struct stream *stream,
                   struct reader *r)
{
        FILE *fp = ctx->fp;

        while (!read_eof(r)) {
                uint32_t msg_id = read_u32(r);

                if (msg_id > stream->tps.tp_count) {
                        fprintf(fp, "unknown tracepoint %u\n", msg_id);
                        return;
                }

                struct tracepoint *tp = &stream->tps.tracepoints[msg_id];

                uint64_t timestamp = tp->timestamp ? read_u64(r) : 0;

                fprintf(fp, "%"PRIu64"\t%s(", timestamp, tp->name);

                for (unsigned arg = 0; arg < tp->num_args; ++arg) {
                        fprintf(fp, "%s%s=", arg ? ", " : "", tp->arg_names[arg]);

                        char type = tp->arg_types[arg];

                        struct enumeration *e = tp->enums[arg];

                        if (e) {
                                uint64_t value = ~0ULL;

                                switch (type) {
                                case 'I':
                                        value = read_u32(r);
                                        break;
                                default:
                                        fprintf(fp, "unk enum '%c'", type);
                                }

                                if (value < e->num_values)
                                        fprintf(fp, "%s", e->value_names[value]);
                                else
                                        fprintf(fp, "unk <0x%"PRIx64">", value);
                                continue;
                        }

                        uint64_t u64;
                        uint32_t u32;

                        switch (type) {
                        case 'i':
                                u32 = read_u32(r);
                                fprintf(fp, "%i", u32);
                                break;
                        case 'l':
                                u64 = read_u64(r);
                                fprintf(fp, "%"PRIi64, u64);
                                break;
                        case 'I':
                                u32 = read_u32(r);
                                fprintf(fp, "0x%x", u32);
                                break;
                        case 'L':
                        case 'p':
                                u64 = read_u64(r);
                                fprintf(fp, "0x%"PRIx64, u64);
                                break;
                        default:
                                fprintf(fp, "unk '%c'", type);
                        }
                }

                fprintf(fp, ") -- %s\n", tp->desc);
        }
}

static void
dump_timeline_packet(struct timeline_ctx *ctx, struct reader *r)
{
        uint32_t w0 = read_u32(r);
        uint32_t w1 = read_u32(r);

        enum tl_stream_id streamid = FLD2VAL(PACKET_STREAMID, w0);
        enum tl_packet_type type = FLD2VAL(PACKET_TYPE, w0);
        enum tl_packet_class class = FLD2VAL(PACKET_CLASS, w0);
        enum tl_packet_family family = FLD2VAL(PACKET_FAMILY, w0);
        unsigned rsvd1 = FLD2VAL(PACKET_RSVD1, w0);

        bool numbered = FLD2VAL(PACKET_SEQBIT, w1);
        int packet_number = numbered ? read_u32(r) : -1;

        unsigned length = FLD2VAL(PACKET_LENGTH, w1) - (numbered ? 4 : 0);
        unsigned rsvd2 = FLD2VAL(PACKET_RSVD2, w1);

        if (ctx->verbose)
                printf("P(%zi) %i %i %i %i %i | %i %i num %i\n",
                       r->end - r->data,
                       streamid, rsvd1, type, class, family,
                       length, rsvd2, packet_number);

        struct stream null_stream = {0}, *stream;
        switch (streamid) {
        case TL_STREAM_ID_KERNEL:
                switch (class) {
                case TL_PACKET_CLASS_AUX:
                        stream = &ctx->aux;
                        break;
                default:
                        stream = &ctx->normal;
                }
                break;
        case TL_STREAM_ID_CSFFW:
                stream = &ctx->csf;
                break;
        default:
                stream = &null_stream;
        }

        void *data = r->data;

        if (type == TL_PACKET_TYPE_HEADER) {
                dump_timeline_header(r, length, &stream->tps, ctx->verbose);
        } else {
                struct reader sub = read_sub(r, length);
                dump_timeline_data(ctx, stream, &sub);
        }

        if (r->data != data + length) {
                fprintf(stderr, "read %zi bytes, expected %i\n",
                        r->data - data, length);
        }
}

static bool
fd_ready(int fd)
{
        struct pollfd pollfd = {
                .fd = fd,
                .events = POLLIN,
        };

        return poll(&pollfd, 1, 0) == 1;
}

#define DEVNAME "mali0"
#define DEVICE "/sys/class/misc/"DEVNAME"/device"
#define DEBUGFS "/sys/kernel/debug/"DEVNAME
#define DEVFILE "/dev/"DEVNAME

int
main(void)
{
        /* This tool must be run as root, but other users may want to
         * move its log files about. */
        umask(0);

        mkdir("/tmp/pantrace-log", 0777);

        write_file(DEVICE"/firmware_config/Log verbosity/cur", "3");
        write_file(DEBUGFS"/fw_trace_enable_mask", "0xffffffffffffffff");

        int fw_traces = xopen(DEBUGFS"/fw_traces", O_RDONLY);
        int mali_fd = xopen(DEVFILE, O_RDWR);
        setup_mali_fd(mali_fd);
        int trace_fd = get_trace_fd(mali_fd);

        unsigned size = 32 * 1024 * 1024;
        char *buf = malloc(size);

        struct timeline_ctx ctx = {0};

        char *last_trace = NULL;

        for (;;) {
                int sz, read_sz;

                usleep(1000);

                FILE *out_file = fopen("/tmp/pantrace-log/current", "a");

                /* Dump mali_trace */
                int mali_trace = xopen(DEBUGFS"/mali_trace", O_RDONLY);
                sz = 0;
                while ((read_sz = read(mali_trace, buf + sz, size - sz)))
                        sz += read_sz;
                close(mali_trace);
                buf[sz] = '\0';

                char *match = last_trace ? strstr(buf, last_trace) : NULL;
                char *start = match ? strchr(match + 1, '\n') : NULL;
                char *trace_buf = start ? start + 1 : buf;

                if (*trace_buf) {
                        struct timespec raw, real;
                        clock_gettime(CLOCK_MONOTONIC_RAW, &raw);
                        clock_gettime(CLOCK_REALTIME, &real);

                        long sec = real.tv_sec - raw.tv_sec;
                        int ns = real.tv_nsec - raw.tv_nsec;
                        if (ns < 0) {
                                ns += 1000000000;
                                sec -= 1;
                        }

                        fprintf(out_file, "time delta: %li.%09i\n", sec, ns);
                }

                fputs(trace_buf, out_file);

                free(last_trace);
                last_trace = strdup(memrchr(buf, '\n', sz - 1));

                /* Dump firmware trace */
                sz = read(fw_traces, buf, size);
                /* TODO: Handle partial messages? */
                fwrite(buf, sz, 1, out_file);

                if (fd_ready(trace_fd)) {
                        /* Dump timelines */
                        ctx.fp = out_file;
                        sz = read(trace_fd, buf, size);
                        struct reader r = {
                                .data = buf,
                                .end = buf + sz,
                        };

                        while (!read_eof(&r))
                                dump_timeline_packet(&ctx, &r);
                }

                ctx.fp = NULL;
                fclose(out_file);
        }
}
